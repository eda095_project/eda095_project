package sparky.gui.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingWorker;

import shared.game.block.Block;
import shared.game.block.SimpleBlock;
import sparky.ImageLoader;
import sparky.SparkyControlls;
import sparky.gui.SparkyGUI;
import sparky.model.Game;

@SuppressWarnings("serial")
public class SparkyGame extends JPanel implements KeyListener {

	private static final int PADDING = 10;

	private SparkyGUI frame;
	private Game game;
	private Boolean repaint;

	private Image logo;

	public SparkyGame(SparkyGUI frame, Game game) {
		this.frame = frame;
		this.game = game;

		logo = ImageLoader.logo;
		if (logo == null) {
			System.err.println("Omitted image on game screen");
		}

		addKeyListener(this);
		setFocusable(true);
		game.start();
		setRepainting(true);
		new Repainter().execute();
	}

	private synchronized boolean isRepainting() {
		return repaint;
	}

	private synchronized void setRepainting(boolean b) {
		repaint = b;
	}

	@Override
	public void paint(Graphics g) {
		int rows = game.getRows();
		int cols = game.getCols();

		g.setColor(SparkyGUI.BACKGROUND);
		g.fillRect(0, 0, getWidth(), getHeight());

		int imageH = 0;
		if (logo != null) {
			imageH = logo.getHeight(null) + PADDING;
			g.drawImage(logo, PADDING, PADDING, null);
		}
		int boardW = getWidth() - PADDING * 2;
		int boardH = getHeight() - PADDING * 3 - imageH;
		int boardY = PADDING + imageH;

		g.setColor(SparkyGUI.BOARD);
		g.fillRect(PADDING, boardY, boardW, boardH);

		int wSize = boardW / cols;
		int hSize = boardH / rows;

		int size = Math.min(wSize, hSize);

		int gridX = PADDING + boardW / 2 - (size * cols) / 2;
		int gridY = boardY + boardH / 2 - (size * rows) / 2;

		g.setColor(Color.BLACK);
		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows; y++) {
				g.drawRect(x * size + gridX, y * size + gridY, size - 1,
						size - 1);
			}
		}

		for (Block b : game.getBlocks()) {
			g.setColor(getColor(b));

			for (SimpleBlock simple : b) {
				if (simple.getY() < 0) {
					continue;
				}
				g.fillRect(simple.getX() * size + gridX, simple.getY() * size
						+ gridY, size, size);
			}
		}

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		int key = SparkyControlls.translate(arg0);
		switch (key) {
		case SparkyControlls.QUIT:
			setRepainting(false);
			frame.setGameSelector();
			break;
		case SparkyControlls.P1_LEFT:
			game.doLeft();
			break;
		case SparkyControlls.P1_RIGHT:
			game.doRight();
			break;
		case SparkyControlls.P1_DOWN:
			game.doDown();
			break;
		case SparkyControlls.P1_UP:
			game.doRotate();
			break;
		case SparkyControlls.P1_A:
			game.doFoceDown();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

	public static Color getColor(Block b) {
		switch (b.getType()) {
		case O:
			return new Color(0xF280A1);
		case I:
			return new Color(0xf7941e);
		case S:
			return new Color(0xCC2127);
		case L:
			return new Color(0x3853A4);
		case J:
			return new Color(0x9932CC);
		case Z:
			return new Color(0x00FFFF);
		case T:
			return new Color(0x008000);
		default:
			return new Color(0x000000);
		}
	}

	private class Repainter extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {

			while (isRepainting()) {
				Thread.sleep(100);
				publish();
			}
			return null;
		}

		@Override
		protected void process(List<Void> chunks) {
			repaint();
		}

	}
}

package sparky.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingWorker;

import sparky.ImageLoader;
import sparky.SparkyControlls;
import sparky.gui.game.SparkyGame;
import sparky.model.Game;
import sparky.model.OfflineGame;

@SuppressWarnings("serial")
public class SparkyGameSelector extends JPanel implements KeyListener {

	public static final int LEFT = 1;
	public static final int RIGHT = -1;

	private static final String ONE = "[1 Player]";
	private static final String TWO = "[2 Player]";
	private static final String MUL = "[Net Split]";
	private static final String ANA = "[Net Anarchy]";
	private static final String HIG = "[Highscores]";

	private static final int STEP = 25;

	private SparkyGUI frame;
	private HashMap<String, Integer> options;
	private boolean moving = false;
	private int steps;
	private Image logo;

	public SparkyGameSelector(SparkyGUI frame) {
		this.frame = frame;

		logo = ImageLoader.logo;
		if (logo == null) {
			System.err.println("Omitted image on game selector screen");
		}

		ArrayList<String> temp = new ArrayList<>();
		temp.add(ONE);
		temp.add(TWO);
		temp.add(MUL);
		temp.add(ANA);
		temp.add(HIG);

		steps = temp.size() * STEP;

		options = new HashMap<>();
		for (int i = 0; i < temp.size(); i++) {
			options.put(temp.get(i), STEP * i);
		}
		addKeyListener(this);
		setFocusable(true);
		requestFocus();
	}

	@Override
	public void paint(Graphics g) {

		g.setColor(SparkyGUI.BACKGROUND);
		g.fillRect(0, 0, getWidth(), getHeight());

		int midX = 320;
		int midY = 240;
		int r = 200;

		int r1 = 280;
		int r2 = 120;

		double redBase = Math.abs(SparkyGUI.BACKGROUND.getRed()
				- SparkyGUI.CIRCLE.getRed());
		double greenBase = Math.abs(SparkyGUI.BACKGROUND.getGreen()
				- SparkyGUI.CIRCLE.getGreen());
		double blueBase = Math.abs(SparkyGUI.BACKGROUND.getBlue()
				- SparkyGUI.CIRCLE.getBlue());

		for (int i = 0; i < 10; i++) {
			int red = (int) ((i * redBase) / 10)
					+ SparkyGUI.BACKGROUND.getRed();
			int green = (int) ((i * greenBase) / 10)
					+ SparkyGUI.BACKGROUND.getGreen();
			int blue = (int) ((i * blueBase) / 10)
					+ SparkyGUI.BACKGROUND.getBlue();
			g.setColor(new Color(red, green, blue));
			g.fillOval(midX - (r1 - i), midY - (r1 - i), (r1 - i) * 2,
					(r1 - i) * 2);
		}
		g.setColor(SparkyGUI.CIRCLE);
		g.fillOval(midX - r1 + 10, midY - r1 + 10, (r1 - 10) * 2, (r1 - 10) * 2);

		g.setColor(SparkyGUI.BACKGROUND);
		g.fillOval(midX - r2, midY - r2, r2 * 2, r2 * 2);

		g.setColor(SparkyGUI.TEXT);
		g.setFont(SparkyGUI.TEXT_FONT);
		FontMetrics fm = g.getFontMetrics();

		int h = fm.getHeight();
		for (String s : options.keySet()) {
			double proc = options.get(s) / ((double) steps);
			double angle = 2 * Math.PI * proc - Math.PI / 2;

			int x = (int) (Math.cos(angle) * r);
			int y = (int) (Math.sin(angle) * r);

			int w = fm.stringWidth(s);

			int realX = midX + x - w / 2;
			int realY = midY + y + h / 2;
			g.drawString(s, realX, realY);
		}

		Image i = ImageLoader.logo;
		if (i != null) {
			int imageX = midX - i.getWidth(null) / 2;
			int imageY = midY - i.getHeight(null) / 2;

			g.drawImage(ImageLoader.logo, imageX, imageY, null);
		}
	}

	private synchronized boolean isMoving() {
		return moving;
	}

	private synchronized void setMoving(boolean moving) {
		this.moving = moving;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		int key = SparkyControlls.translate(arg0);
		switch (key) {
		case SparkyControlls.P1_LEFT:
			new Move(LEFT);
			break;
		case SparkyControlls.P1_RIGHT:
			new Move(RIGHT);
			break;
		case SparkyControlls.P1_A:
			if (isMoving())
				break;
			String goal = null;
			for (String s : options.keySet()) {
				if (options.get(s) == 0) {
					goal = s;
					break;
				}
			}
			if (goal != null) {
				switch (goal) {
				case ONE:
					new StartGame(new OfflineGame(1));
					break;
				case TWO:
					new StartGame(new OfflineGame(2));
					break;
				}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {

	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}

	private class Move extends SwingWorker<Void, Integer> {

		private int direction;

		public Move(int direction) {
			this.direction = direction;
			if (!isMoving()) {
				setMoving(true);
				execute();
			}
		}

		@Override
		protected Void doInBackground() throws Exception {
			for (int i = 0; i < STEP; i++) {
				publish(i);
				Thread.sleep(15);
			}
			setMoving(false);
			return null;
		}

		@Override
		protected void process(List<Integer> chunks) {
			int steps = chunks.size();

			for (String s : options.keySet()) {
				int i = options.get(s);
				i += steps * direction;
				i = (i + SparkyGameSelector.this.steps)
						% SparkyGameSelector.this.steps;
				options.put(s, i);
			}
			repaint();
		}
	}

	public class StartGame extends SwingWorker<Void, Void> {

		private Game game;

		public StartGame(Game game) {
			this.game = game;
			execute();
		}

		@Override
		protected Void doInBackground() throws Exception {
			return null;
		}

		@Override
		protected void done() {
			frame.setGame(new SparkyGame(frame, game));
		}
	}

}

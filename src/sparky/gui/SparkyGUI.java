package sparky.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import sparky.gui.game.SparkyGame;

@SuppressWarnings("serial")
public class SparkyGUI extends JFrame {

	public static final Color BACKGROUND = new Color(39, 35, 35);
	public static final Color CIRCLE = new Color(5, 17, 28);
	public static final Color TEXT = Color.WHITE;
	public static final Color BOARD = Color.WHITE;

	public static final Font TEXT_FONT = new Font("Verdana", Font.PLAIN, 20);

	private SparkyGameSelector gameSelector;
	private SparkyGame game;
	private JPanel panel;

	public SparkyGUI() {
		super("Tetris anarchy for Sparky");
		setSize(640, 480);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		setLocationRelativeTo(null);
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
		setCursor(blankCursor);

		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		add(panel);
		gameSelector = new SparkyGameSelector(this);
		panel.add(gameSelector, BorderLayout.CENTER);

		setVisible(true);
	}

	public void setGame(SparkyGame game) {
		this.game = game;
		panel.remove(gameSelector);
		panel.add(game, BorderLayout.CENTER);
		panel.updateUI();
		game.requestFocus();
	}

	public void setGameSelector() {
		panel.remove(game);
		panel.add(gameSelector);
		gameSelector.requestFocus();
		panel.updateUI();
	}
}

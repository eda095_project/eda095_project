package sparky;

import java.io.IOException;

import sparky.gui.SparkyGUI;

public class SparkyTetris {

	public static void main(String[] args) {

		SparkyControlls.pcMode();

		try {
			ImageLoader.loadImages();
		} catch (IOException e) {
			System.err.println("Could not load images");
		}

		new SparkyGUI();

	}

}

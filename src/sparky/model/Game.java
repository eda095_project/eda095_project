package sparky.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import shared.game.block.Block;
import shared.game.board.ClientBoard;

public abstract class Game implements ActionListener {

	protected ClientBoard board;
	protected int score = 0;
	protected int level = 0;
	protected int lines = 0;
	protected String gameState = "";

	public abstract void start();

	public int getRows() {
		return board.getHeight();
	}

	public int getCols() {
		return board.getWidth();
	}

	public List<Block> getBlocks() {
		List<Block> blocks = new ArrayList<>();
		blocks.addAll(board.getStaticBlocks());
		blocks.addAll(board.getOther());
		
		Block current = board.getCurrent();
		if (current != null) {
			blocks.add(current);
		}
		return blocks;
	}

	public abstract void tick();

	@Override
	public void actionPerformed(ActionEvent e) {
		tick();
	}

	public abstract void doLeft();

	public abstract void doRight();

	public abstract void doDown();

	public abstract void doPause();

	public abstract void doFoceDown();

	public abstract void doRotate();
}

package sparky.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.Timer;

import shared.Constants;
import shared.game.Direction;
import shared.game.block.Block;
import shared.game.board.Player;
import sparky.model.board.SparkyBoard;

public class OfflineGame extends Game {

	private static final int[] pointModifiers = { 0, 40, 100, 300, 1200 };

	private SparkyBoard board;
	private Timer timer;

	public OfflineGame(int players) {
		super.board = board = new SparkyBoard(players);
		timer = new Timer(1000, this);
		gameState = Constants.STARTED;
	}

	@Override
	public void start() {
		timer.start();
	}

	@Override
	public void tick() {
		boolean[] movedIndices = board.moveCurrentsDownIfLegal();
		for (int i = 0; i < movedIndices.length; i++) {
			if (!movedIndices[i]) {
				int tetris = updateBoardWithNewCurrent(board.getPlayer(0));
				lines += tetris;
				score += (pointModifiers[tetris] * level);
				updateLevel();
			}
		}
	}

	private synchronized void updateLevel() {
		int tempLevel = 1 + lines / 10;
		if (tempLevel > level) {
			level = tempLevel;
			timer.setDelay((int) (1000 * 1.17 * Math.pow(Math.E, -0.4
					* (level - 1))));
		}
	}

	private synchronized int updateBoardWithNewCurrent(Player player) {
		int rowsRemoved = 0;
		Block current = board.getCurrent(player);
		if (current != null) {
			board.addStaticBlock(current);
			rowsRemoved = checkRemoveRows(current);
		}
		if (!board.setCurrent(player, board.generateRandomBlock())) {
			gameState = Constants.ENDED;
		}
		return rowsRemoved;
	}

	protected synchronized int checkRemoveRows(Block current) {
		int rowsRemoved = 0;
		Set<Integer> possibleRowsToRemove = current.getYVals();
		List<Integer> rowsToRemove = new ArrayList<>();
		synchronized (board) {
			for (int row : possibleRowsToRemove) {
				if (board.hasRow(row)) {
					rowsToRemove.add(row);
					rowsRemoved++;
				}
			}
			Collections.sort(rowsToRemove);
			board.removeRows(rowsToRemove);
		}
		return rowsRemoved;
	}

	@Override
	public synchronized void doLeft() {
		if (gameState == Constants.STARTED) {
			board.moveCurrentIfLegal(board.getPlayer(0), Direction.LEFT);
		}

	}

	@Override
	public synchronized void doRight() {
		if (gameState == Constants.STARTED) {
			board.moveCurrentIfLegal(board.getPlayer(0), Direction.RIGHT);
		}
	}

	@Override
	public synchronized void doDown() {
		if (gameState == Constants.STARTED) {
			if (board.moveCurrentIfLegal(board.getPlayer(0), Direction.DOWN)) {
				score++;
			}
		}
	}

	@Override
	public synchronized void doPause() {
		if (gameState == Constants.STARTED) {
			gameState = Constants.PAUSED;
			timer.stop();
		} else if (gameState == Constants.PAUSED) {
			gameState = Constants.STARTED;
			timer.start();
		}
	}

	@Override
	public synchronized void doFoceDown() {
		if (gameState == Constants.STARTED) {
			int spaces = board.forceDown(board.getPlayer(0));
			score += spaces;
		}
	}

	@Override
	public synchronized void doRotate() {
		if (gameState == Constants.STARTED) {
			board.rotateCurrentIfLegal(board.getPlayer(0));
		}
	}

}
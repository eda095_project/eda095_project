package sparky.model.board;

import java.util.ArrayList;
import java.util.List;

import shared.game.block.Block;
import shared.game.block.generator.StandardGenerator;
import shared.game.board.ClientBoard;
import shared.game.board.TetrisBoard;

public class SparkyBoard extends TetrisBoard implements ClientBoard {

	private static final int ROWS = 20;
	private static final int COLS = 12;

	public SparkyBoard(int players) {
		super(ROWS, COLS * players, new StandardGenerator(), players);
	}

	@Override
	public synchronized int getWidth() {
		return getCols();
	}

	@Override
	public synchronized int getHeight() {
		return getRows();
	}

	@Override
	public synchronized List<Block> getOther() {
		return new ArrayList<Block>();
	}

	@Override
	public synchronized Block getGhost() {
		return playerList[0].getGhost();
	}

	@Override
	public synchronized Block getCurrent() {
		return playerList[0].getCurrent();
	}

	@Override
	public List<Block> getStaticBlocks() {
		return staticBlocks;
	}
}
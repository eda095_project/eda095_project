package sparky;

import java.awt.event.KeyEvent;

public class SparkyControlls {

	public static final int P1_UP = 1;
	public static final int P1_DOWN = 2;
	public static final int P1_LEFT = 3;
	public static final int P1_RIGHT = 4;
	public static final int P1_A = 5;
	public static final int P1_B = 6;
	public static final int P1_C = 7;

	public static final int CRED = 8;
	public static final int QUIT = 9;
	public static final int START1 = 10;
	public static final int START2 = 11;

	public static final int P2_UP = 12;
	public static final int P2_DOWN = 13;
	public static final int P2_LEFT = 14;
	public static final int P2_RIGHT = 15;
	public static final int P2_A = 16;
	public static final int P2_B = 17;
	public static final int P2_C = 18;

	public static final int UNKNWON = -1;

	private static boolean PC = true;

	public static void pcMode() {
		PC = true;
	}

	public static void sparkyMode() {
		PC = false;
	}

	public static int translate(KeyEvent e) {
		int key = e.getKeyCode();
		if (PC)
			return translatePC(key);
		return translateSparky(key);
	}

	private static int translatePC(int key) {
		switch (key) {
		case KeyEvent.VK_LEFT:
			return P1_LEFT;
		case KeyEvent.VK_RIGHT:
			return P1_RIGHT;
		case KeyEvent.VK_UP:
			return P1_UP;
		case KeyEvent.VK_DOWN:
			return P1_DOWN;
		case KeyEvent.VK_SPACE:
			return P1_A;
		case KeyEvent.VK_Q:
		case KeyEvent.VK_ESCAPE:
			return QUIT;
		}
		return UNKNWON;
	}

	private static int translateSparky(int key) {
		return UNKNWON;
	}

	// Singleton
	private SparkyControlls() {

	}

}

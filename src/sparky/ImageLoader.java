package sparky;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader {

	public static Image logo;

	public static void loadImages() throws IOException {
		logo = ImageIO.read(new File("title_small.png"));
	}

}

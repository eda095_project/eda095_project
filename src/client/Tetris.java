package client;

import java.io.IOException;

import client.gui.TetrisGUI;
import client.model.ClientModel;
import client.model.Data;

public class Tetris {
	public static void main(String[] args) {
		ClientModel model;

		try {
			Data d = new Data();
			model = new ClientModel(d);
			new TetrisGUI(model, d);
		} catch (IOException e) {
			System.err.println("Could not connect to server [" + e.getMessage() + "]");
		}
	}
}
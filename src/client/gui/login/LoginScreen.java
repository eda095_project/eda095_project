package client.gui.login;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import client.gui.ImagePanel;
import client.model.Data;
import client.model.ClientModel;

@SuppressWarnings("serial")
public class LoginScreen extends JLayeredPane {

	private ClientModel model;
	private Data d;

	private JLabel contributors;

	private JLabel info;
	private JTextField text;
	private JButton connect;
	private JLabel connecting;
	private JLabel offlineInfo;
	private JButton session;
	private JButton offlineMode;
	private JButton reconnect;
	private JTextField hostSelection;
	private JButton connectNewHost;
	private JLabel connectedTo;
	private JButton disconnect;

	public LoginScreen(final ClientModel model, Data d) {
		this.model = model;
		this.d = d;

		text = new JTextField();
		text.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = text.getText().trim();
				if (!name.isEmpty()) {
					model.login(name, false);
				}
			}
		});
		text.setBounds(300, 300, 200, 25);

		add(text, PALETTE_LAYER);

		JPanel img = new ImagePanel("bg.png");
		img.setBounds(0, 0, 800, 600);
		add(img, DEFAULT_LAYER);

		contributors = new ContributorPanel(this);
		add(contributors, PALETTE_LAYER);

		info = new JLabel("Please select screen name");
		info.setBounds(300, 275, 200, 25);
		info.setForeground(Color.WHITE);
		add(info, PALETTE_LAYER);

		connect = new JButton("Login");
		connect.setBounds(300, 330, 200, 17);
		connect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = text.getText().trim();
				if (!name.isEmpty()) {
					model.login(name, false);
				}
			}
		});
		add(connect, PALETTE_LAYER);

		session = new JButton("Use previous session");
		session.setBounds(300, 385, 200, 25);
		session.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				model.login(null, true);
			}
		});
		session.setVisible(false);
		add(session, PALETTE_LAYER);

		connectedTo = new JLabel();
		connectedTo.setForeground(Color.WHITE);
		add(connectedTo, PALETTE_LAYER);

		disconnect = new JButton("Disconnect");
		disconnect.setBounds(300, 350, 200, 17);
		disconnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new Disconnect();
			}
		});
		add(disconnect, PALETTE_LAYER);

		connecting = new JLabel("<html>Connecting to host...");
		connecting.setBounds(300, 275, 200, 25);
		connecting.setBorder(BorderFactory.createLineBorder(Color.WHITE));
		connecting.setForeground(Color.WHITE);

		add(connecting, PALETTE_LAYER);

		offlineInfo = new JLabel("Could not connect to known hosts");
		offlineInfo.setBounds(300, 275, 200, 25);
		offlineInfo.setForeground(Color.WHITE);
		add(offlineInfo, PALETTE_LAYER);

		hostSelection = new JTextField();
		hostSelection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new Connect();
			}
		});
		hostSelection.setBounds(300, 300, 200, 25);
		add(hostSelection, PALETTE_LAYER);

		connectNewHost = new JButton("Connect to new host");
		connectNewHost.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new Connect();
			}
		});
		connectNewHost.setBounds(300, 330, 200, 25);
		add(connectNewHost, PALETTE_LAYER);

		offlineMode = new JButton("Offline");
		offlineMode.setBounds(300, 380, 95, 25);
		offlineMode.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				model.login("", false);
			}
		});
		add(offlineMode, PALETTE_LAYER);

		reconnect = new JButton("Retry");
		reconnect.setBounds(405, 380, 95, 25);
		reconnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ConnectAll();
			}
		});
		add(reconnect, PALETTE_LAYER);

		new ConnectAll();
	}

	@Override
	public void repaint() {
		if (contributors != null) {
			Dimension d = contributors.getPreferredSize();
			int y = getHeight() - d.height - 20;
			int x = getWidth() - d.width - 20;
			contributors.setBounds(x, y, d.width, d.height);
		}
		if (connectedTo != null) {
			int y = getHeight() - 45;
			connectedTo.setBounds(20, y, 200, 45);
		}
		super.repaint();
	}

	private void showConnection(boolean visible) {
		connecting.setVisible(visible);
	}

	private void showLogin(boolean visible) {
		info.setVisible(visible);
		text.setVisible(visible);
		connect.setVisible(visible);
		connectedTo.setVisible(visible);
		disconnect.setVisible(visible);
	}

	private void showHostSelect(boolean visible) {
		offlineInfo.setVisible(visible);
		offlineMode.setVisible(visible);
		reconnect.setVisible(visible);
		hostSelection.setVisible(visible);
		connectNewHost.setVisible(visible);
	}

	private class ConnectAll extends SwingWorker<String, String> {

		public ConnectAll() {
			showLogin(false);
			showHostSelect(false);
			showConnection(true);
			execute();
		}

		@Override
		protected String doInBackground() throws Exception {

			for (String host : d.getHosts()) {
				try {
					publish("<br>");
					publish(host);
					model.connect(host);
					return host;
				} catch (IOException e) {
					publish("...failed!");
				}
			}
			Thread.sleep(500);
			return null;
		}

		@Override
		protected void done() {
			String host = null;
			try {
				host = get();
			} catch (Exception e) {

			}
			showConnection(false);
			if (host != null) {
				showLogin(true);
				if (model.hasActiveSession()) {
					session.setVisible(true);
				}
				connectedTo.setText(host);
			} else {
				showHostSelect(true);
			}
		}

		@Override
		protected void process(List<String> chunks) {
			for (String s : chunks) {
				connecting.setText(connecting.getText() + s);
			}
			Rectangle r = connecting.getBounds();
			r.height = connecting.getPreferredSize().height;
			connecting.setBounds(r);
		}
	}

	private class Connect extends SwingWorker<String, String> {

		private String host;

		public Connect() {
			host = hostSelection.getText().trim();
			if (host.length() > 1) {
				showLogin(false);
				showHostSelect(false);
				showConnection(true);
				execute();
			}
		}

		@Override
		protected String doInBackground() throws Exception {
			try {
				publish("<br>");
				publish(host);
				model.connect(host);
				return host;
			} catch (IOException e) {
				publish("...failed!");
			}
			return null;
		}

		@Override
		protected void done() {
			String host = null;
			try {
				host = get();
			} catch (Exception e) {

			}
			showConnection(false);
			if (host != null) {
				showLogin(true);
				if (model.hasActiveSession()) {
					session.setVisible(true);
				}
				connectedTo.setText(host);
			} else {
				showHostSelect(true);
			}
			System.out.println("Connected to " + host);
		}

		@Override
		protected void process(List<String> chunks) {
			for (String s : chunks) {
				connecting.setText(connecting.getText() + s);
			}
			Rectangle r = connecting.getBounds();
			r.height = connecting.getPreferredSize().height;
			connecting.setBounds(r);
		}

	}

	private class Disconnect extends SwingWorker<Void, Void> {

		public Disconnect() {
			showLogin(false);
			showHostSelect(false);
			showConnection(false);
			session.setVisible(false);
			execute();
		}

		@Override
		protected Void doInBackground() throws Exception {
			model.disconnect();
			return null;
		}

		@Override
		protected void done() {
			showHostSelect(true);
		}

	}
}

package client.gui.login;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingWorker;

@SuppressWarnings("serial")
public class ContributorPanel extends JLabel implements MouseListener {

	private String contributors;
	private JComponent parent;

	public ContributorPanel(JComponent parent) {
		this.parent = parent;
		StringBuilder sb = new StringBuilder();
		try {
			Scanner s = new Scanner(new File("contributors.txt"));
			sb.append("<html>");
			sb.append("Contributors:");
			while (s.hasNext()) {
				sb.append("<br>").append(s.nextLine());
			}
			sb.append("</html>");
			s.close();
		} catch (FileNotFoundException e) {
			System.err.println("Could not load contributors.txt");
		}

		addMouseListener(this);

		contributors = sb.toString();

		setText("MezzMud");
		setForeground(Color.WHITE);

	}

	private class Grower extends SwingWorker<Void, Integer> {

		@Override
		protected Void doInBackground() throws Exception {
			return null;
		}

		@Override
		protected void done() {
			setText(contributors);
			parent.repaint();
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (!contributors.isEmpty())
			new Grower().execute();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

}

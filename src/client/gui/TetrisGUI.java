package client.gui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import client.gui.game.GamePanel;
import client.gui.game.background.ColoredBackground;
import client.gui.game.board.CatBoard;
import client.gui.game.board.TetrisBoard;
import client.gui.game.status.InfoStatusPanel;
import client.gui.game.status.LevelStatusPanel;
import client.gui.game.status.LinesStatusPanel;
import client.gui.game.status.NextBlockStatusPanel;
import client.gui.game.status.PlayerStatusPanel;
import client.gui.game.status.ScoreStatusPanel;
import client.gui.lobby.LobbyScreen;
import client.gui.login.LoginScreen;
import client.model.ClientModel;
import client.model.Data;
import client.model.game.game.Game;

@SuppressWarnings("serial")
public class TetrisGUI extends JFrame implements Observer {

	private static final int STATUS_AREA_WIDTH = 200;
	private static final int NEXT_AREA_HEIGHT = 150;
	private static final int INFO_AREA_HEIGHT = 150;

	private static final int ALERT_WIDTH = 200;
	private static final int ALERT_HEIGHT = 85;

	private ClientModel model;
	private Data d;
	private JPanel basePanel;
	private JPanel componentPanel;
	private JPanel alertPanel;

	public TetrisGUI(ClientModel model, Data d) {
		super("Tetris anarchy");
		setSize(800, 600);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.model = model;
		this.d = d;
		model.addObserver(this);

		basePanel = new BasePanel();
		add(basePanel);

		JLayeredPane layeredPane = new JLayeredPane();
		basePanel.add(layeredPane);

		componentPanel = new JPanel(new BorderLayout());
		layeredPane.add(componentPanel, JLayeredPane.DEFAULT_LAYER);

		componentPanel.setBounds(0, 0, getWidth(), getHeight());

		alertPanel = new AlertPanel(model);
		layeredPane.add(alertPanel, JLayeredPane.POPUP_LAYER);

		setVisible(true);
		basePanel.repaint();
		update(null, null);
	}

	@Override
	public void update(Observable o, Object arg) {
		componentPanel.removeAll();
		switch (model.getState()) {
		case 0:
			componentPanel.add(new LoginScreen(model, d));
			break;
		case 1:
			componentPanel.add(new LobbyScreen(model));
			break;
		case 2:
			GamePanel gp = getGame();
			componentPanel.add(gp);
			gp.focus();
			Rectangle r = new Rectangle(gp.getPreferredSize());
			componentPanel.setBounds(r);
			basePanel.setPreferredSize(gp.getPreferredSize());
			pack();
			break;
		}
		componentPanel.updateUI();
	}

	private GamePanel getGame() {
		Game game = model.getGame();
		game.waitForGame();

		TetrisBoard tb = new CatBoard(game);
		tb.addKeyListener(game);

		GamePanel gp = new GamePanel(game, new ColoredBackground(39, 35, 35),
				tb);
		gp.addStatusPanel(new NextBlockStatusPanel(game, STATUS_AREA_WIDTH,
				NEXT_AREA_HEIGHT));
		gp.addStatusPanel(new ScoreStatusPanel(game, STATUS_AREA_WIDTH, 30));
		gp.addStatusPanel(new LevelStatusPanel(game, STATUS_AREA_WIDTH, 30));
		gp.addStatusPanel(new LinesStatusPanel(game, STATUS_AREA_WIDTH, 30));
		gp.addStatusPanel(new InfoStatusPanel(game, STATUS_AREA_WIDTH,
				INFO_AREA_HEIGHT));
		gp.addStatusPanel(new PlayerStatusPanel(game, STATUS_AREA_WIDTH,
				INFO_AREA_HEIGHT));
		return gp;
	}

	private class BasePanel extends JPanel {

		public BasePanel() {
			super(new BorderLayout());
		}

		@Override
		public void repaint() {
			if (alertPanel != null) {
				int x = (getWidth() - ALERT_WIDTH) / 2;
				int y = (getHeight() - ALERT_HEIGHT) / 2;
				int w = ALERT_WIDTH;
				int h = ALERT_HEIGHT;

				alertPanel.setBounds(x, y, w, h);
			}
			super.repaint();
		}
	}
}
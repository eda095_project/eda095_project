package client.gui;

import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import client.model.ClientModel;

@SuppressWarnings("serial")
public class AlertPanel extends JPanel {

	private static final int DELAY_TIME = 2000;

	private JLabel text;
	private ClientModel model;

	public AlertPanel(ClientModel model) {
		this.model = model;
		text = new JLabel();
		add(text);
		setBorder(BorderFactory.createTitledBorder("Announcement"));
		setVisible(false);
		new AnnouncementFetcher().start();
	}

	@Override
	public void repaint() {
		super.repaint();
	}

	private class AnnouncementFetcher extends Thread {

		@Override
		public void run() {
			while (true) {
				String message = model.getAnnouncement();
				Updater u = new Updater(message);
				u.execute();
				u.waitForDone();
			}
		}
	}

	private class Updater extends SwingWorker<Void, Void> {

		private String message;

		public Updater(String message) {
			this.message = message;
		}

		@Override
		protected Void doInBackground() throws Exception {
			publish();
			Thread.sleep(DELAY_TIME);
			return null;
		}

		@Override
		protected void process(List<Void> chunks) {
			text.setText(message);
			setVisible(true);
		}

		@Override
		protected synchronized void done() {
			setVisible(false);
			notifyAll();
		}

		public synchronized void waitForDone() {
			while (!isDone()) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
		}
	}
}

package client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

import client.model.Data;

@SuppressWarnings("serial")
public class HostSelector extends JFrame implements WindowListener {

	private String host = null;
	private Data d;
	private JComboBox<String> box;
	private JTextField text;
	private JButton b2;

	public HostSelector(Data d) {
		this.d = d;
		setSize(400, 200);
		setLayout(null);
		addWindowListener(this);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		List<String> data = d.getHosts();
		String[] hosts = new String[data.size()];
		for (int i = 0; i < data.size(); i++)
			hosts[i] = data.get(i);

		box = new JComboBox<>(hosts);
		box.addActionListener(new A());
		box.setBounds(75, 10, 250, 20);
		add(box);
		JButton b = new JButton("Use selected");
		b.addActionListener(new A());
		b.setBounds(75, 40, 250, 20);
		add(b);
		text = new JTextField(20);
		text.addActionListener(new B());
		text.setBounds(75, 100, 250, 20);
		add(text);
		b2 = new JButton("Use input");
		b2.addActionListener(new B());
		b2.setBounds(75, 125, 250, 20);
		add(b2);
		setVisible(true);
	}

	public String getHost() {
		waitHost();
		return host;
	}

	private synchronized void waitHost() {
		while (host == null) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
	}

	private synchronized void setHost(String host) {
		this.host = host;
		notifyAll();
		setVisible(false);
		dispose();
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {
		setHost(d.getHosts().get(0));
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	private class A implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			setHost(box.getSelectedItem().toString());
		}
	}

	private class B implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			setHost(text.getText());
		}

	}
}

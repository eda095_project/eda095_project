package client.gui.lobby;

import java.util.List;

import javax.swing.JLayeredPane;
import javax.swing.SwingWorker;

import client.gui.ImagePanel;
import client.gui.lobby.chat.ChatPanel;
import client.model.ClientModel;
import client.model.game.gamemode.GameMode;

@SuppressWarnings("serial")
public class LobbyScreen extends JLayeredPane {

	private static final int PADDING = 20;
	private static final int TOP_PADDING = 80;
	private static final int LIST_WIDTH = 200;
	private static final int CHAT_HEIGHT_MAX = 170;
	private static final int CHAT_HEIGHT_MIN = 20;

	private static final int CHAT_HIDE_SPEED = 15;

	private ClientModel model;

	private LobbyList list;
	private GameInfo info;
	private ChatPanel chat;

	private int chatHeight;
	private boolean moving;

	public LobbyScreen(ClientModel model) {
		this.model = model;

		ImagePanel bgPanel = new ImagePanel("lobby.png");
		bgPanel.setBounds(0, 0, 800, 600);
		add(bgPanel, DEFAULT_LAYER);

		list = new LobbyList();
		info = new GameInfo(model, list);
		chat = new ChatPanel(this, model);

		chatHeight = CHAT_HEIGHT_MAX;

		add(list, PALETTE_LAYER);

		for (GameMode g : model.getGamemodes()) {
			GameButton b = new GameButton(info, g);
			list.add(b);
		}
		add(info, PALETTE_LAYER);
		add(chat, PALETTE_LAYER);
	}

	@Override
	public void repaint() {
		boolean showChat = model.isOnline();
		if (list != null) {
			int x = PADDING;
			int y = TOP_PADDING;
			int w = LIST_WIDTH;
			int h = getHeight() - PADDING - TOP_PADDING;
			if (showChat) {
				h -= chatHeight + PADDING;
			}
			list.setBounds(x, y, w, h);
		}
		if (info != null) {
			int x = PADDING * 2 + LIST_WIDTH;
			int y = TOP_PADDING;
			int w = getWidth() - PADDING * 3 - LIST_WIDTH;
			int h = getHeight() - PADDING - TOP_PADDING;
			if (showChat) {
				h -= chatHeight + PADDING;
			}
			info.setBounds(x, y, w, h);
		}
		if (chat != null) {
			if (showChat) {
				int x = PADDING;
				int y = getHeight() - PADDING - chatHeight;
				int w = getWidth() - PADDING * 2;
				int h = chatHeight;
				chat.setBounds(x, y, w, h);
			} else {
				chat.setBounds(0, 0, 0, 0);
			}
		}
		super.repaint();
	}

	private synchronized boolean canMove() {
		if (moving)
			return false;
		moving = true;
		return true;
	}

	private synchronized void resetMoved() {
		moving = false;
	}

	public void resizeChat() {
		if (canMove())
			new ResizeChat();
	}

	public class ResizeChat extends SwingWorker<Void, Integer> {

		int dir;
		int diff;

		public ResizeChat() {
			if (chatHeight == CHAT_HEIGHT_MAX) {
				dir = -1;
			} else {
				dir = 1;
			}
			diff = CHAT_HEIGHT_MAX - CHAT_HEIGHT_MIN;

			execute();
		}

		@Override
		protected Void doInBackground() throws Exception {
			for (int i = 0; i < diff; i += 5) {
				publish(i);
				Thread.sleep(CHAT_HIDE_SPEED);
			}
			return null;
		}

		@Override
		protected void process(List<Integer> chunks) {
			chatHeight = chatHeight + (chunks.size() * 5 * dir);
			repaint();
		}

		@Override
		protected void done() {
			resetMoved();
		}

	}

}

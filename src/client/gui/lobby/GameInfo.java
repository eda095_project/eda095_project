package client.gui.lobby;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import shared.Constants;

import client.model.ClientModel;
import client.model.game.game.Game;
import client.model.game.gamemode.GameMode;

@SuppressWarnings("serial")
public class GameInfo extends JLayeredPane implements ActionListener, Observer {

	private ClientModel model;
	private JLabel info;
	private JButton join;
	private JPanel bg;
	private GameMode active;
	private LobbyList ll;
	private LoadingScreen load;
	private JLabel stats;

	public GameInfo(ClientModel model, LobbyList ll) {
		this.model = model;
		this.ll = ll;
		info = new JLabel();
		info.setVerticalAlignment(JLabel.TOP);
		join = new JButton("Join game");
		join.addActionListener(this);
		bg = new JPanel();

		load = new LoadingScreen(model);

		stats = new JLabel();
		stats.setVerticalAlignment(JLabel.TOP);

		setup();
		add(bg, DEFAULT_LAYER);
		add(join, PALETTE_LAYER);
		add(info, PALETTE_LAYER);
		add(stats, PALETTE_LAYER);
		add(load, POPUP_LAYER);
	}

	private void setup() {
		info.setText("Please select what game you want to play, then press join game.");
		stats.setText("");
		join.setEnabled(false);
	}

	public void setActive(GameMode g) {
		if (active != null) {
			active.deleteObserver(this);
		}
		active = g;
		g.addObserver(this);
		info.setText(active.toHTML());
		updateStats();
		join.setEnabled(true);
	}

	private void updateStats() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append("Playing right now: ").append(active.getPlaying());
		if (active.getPlayers() > 1) {
			sb.append("<br>");
			sb.append("Queueing: ").append(active.getQueueing());
			sb.append("/").append(active.getPlayers());
		}
		sb.append("</html>");

		stats.setText(sb.toString());
		repaint();
	}

	@Override
	public void repaint() {
		if (join != null) {
			int w = 150;
			int h = 25;
			join.setBounds(getWidth() - w - 10, getHeight() - h - 10, w, h);
		}
		if (info != null) {
			// info.setBounds(10, 10, getWidth() - 20,
			// info.getPreferredSize().height);
			info.setBounds(10, 10, getWidth() - 20, 200);
		}
		if (bg != null) {
			bg.setBounds(0, 0, getWidth(), getHeight());
		}
		if (load != null) {
			int w = 300;
			int h = 100;

			int x = getWidth() / 2 - w / 2;
			int y = getHeight() / 2 - h / 2;
			load.setBounds(x, y, w, h);
		}
		if (stats != null) {
			int w = 200;
			int h = stats.getPreferredSize().height;
			int x = 10;
			int y = getHeight() - h - 10;
			stats.setBounds(x, y, w, h);
		}
		super.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		join.setEnabled(false);
		ll.setEnabled(false);

		boolean b = model.joinGame(active);

		if (!b) {
			JOptionPane.showMessageDialog(null, "Could not join game");
			ll.setEnabled(true);
			join.setEnabled(true);
		} else {
			load.setVisible(true);
			new GameWaiter();
		}
	}

	private class GameWaiter extends SwingWorker<Void, Void> {

		public GameWaiter() {
			execute();
		}

		@Override
		protected Void doInBackground() throws Exception {
			boolean done = false;
			Game g = model.getGame();
			while (!done) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				int status = g.getInfo();
				done = status != Constants.GAMEINFO_IN_QUEUE;

			}
			return null;
		}

		@Override
		protected void done() {
			load.setVisible(false);
			ll.setEnabled(true);
			join.setEnabled(true);
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		updateStats();
	}
}

package client.gui.lobby.chat;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import shared.chat.Message;
import client.model.ClientModel;

@SuppressWarnings("serial")
public class TextScreen extends JPanel implements ActionListener {

	private ClientModel model;

	private JTextArea text;
	private JTextField input;

	public TextScreen(ClientModel model) {
		this.model = model;
		setLayout(null);

		text = new JTextArea("Test");
		input = new JTextField();
		input.addActionListener(this);
		new ChatUpdated().execute();
		add(text, BorderLayout.CENTER);
		add(input, BorderLayout.SOUTH);
	}

	@Override
	public void repaint() {
		int inputH = 0;
		if (input != null) {
			Dimension d = input.getPreferredSize();
			inputH = d.height;
			int x = 0;
			int y = getHeight() - inputH;
			int w = getWidth();
			int h = inputH;
			input.setBounds(x, y, w, h);
		}
		if (text != null) {
			int x = 0;
			int y = 0;
			int w = getWidth();
			int h = getHeight() - inputH;
			text.setBounds(x, y, w, h);
		}
		super.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		model.sendChat(input.getText());
		input.setText("");
	}

	private class ChatUpdated extends SwingWorker<Void, Message> {

		@Override
		protected Void doInBackground() throws Exception {
			while (model.isOnline()) {
				Message message = model.getChat();
				publish(message);
			}
			return null;
		}

		@Override
		protected void process(List<Message> chunks) {
			for (Message m : chunks) {
				String message = "[" + m.getTime() + "] " + m.getFrom() + ": "
						+ m.getMessage();

				String temp = text.getText();
				temp += "\n" + message;
				text.setText(temp);
			}
		}

	}

}

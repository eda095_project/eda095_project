package client.gui.lobby.chat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLayeredPane;

import client.gui.lobby.LobbyScreen;
import client.model.ClientModel;

@SuppressWarnings("serial")
public class ChatPanel extends JLayeredPane {

	private static final int LIST_WIDTH = 140;
	private static final int MINIMIZE_SIZE = 20;

	private TextScreen text;
	private ChatList list;
	private JButton minimize;

	public ChatPanel(final LobbyScreen lobbyScreen, ClientModel model) {
		text = new TextScreen(model);
		list = new ChatList(model);
		minimize = new JButton("-");
		minimize.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				lobbyScreen.resizeChat();
			}
		});

		add(text, DEFAULT_LAYER);
		add(list, DEFAULT_LAYER);
		add(minimize, POPUP_LAYER);
	}

	@Override
	public void repaint() {
		if (text != null) {
			text.setBounds(0, 0, getWidth() - LIST_WIDTH, getHeight());
		}
		if (list != null) {
			list.setBounds(getWidth() - LIST_WIDTH, 0, LIST_WIDTH, getHeight());
		}
		if (minimize != null) {
			int x = getWidth() - MINIMIZE_SIZE;
			int y = 0;
			int w = MINIMIZE_SIZE;
			int h = MINIMIZE_SIZE;
			minimize.setBounds(x, y, w, h);
		}
	}
}

package client.gui.lobby.chat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;

import client.model.ClientModel;

@SuppressWarnings("serial")
public class ChatList extends JPanel implements ActionListener {

	private ClientModel model;
	private List<String> users;
	private JTextArea jta;

	public ChatList(ClientModel model) {
		this.model = model;
		users = new ArrayList<>();

		setLayout(null);
		jta = new JTextArea();
		jta.setEditable(false);

		add(jta);
		Timer t = new Timer(250, this);
		t.start();
	}

	@Override
	public void repaint() {
		if (jta != null) {
			jta.setBounds(0, 0, getWidth(), getHeight());

		}
		super.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int old = users.hashCode();
		users = model.getUsers();
		int newer = users.hashCode();
		if (old != newer) {
			StringBuilder sb = new StringBuilder();
			for (String user : users) {
				sb.append(user).append("\n");
			}
			jta.setText(sb.toString().trim());
		}
	}
}

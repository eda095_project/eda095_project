package client.gui.lobby;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class LobbyList extends JPanel {

	private ArrayList<JButton> buttons;

	public LobbyList() {
		setLayout(null);
		buttons = new ArrayList<>();
	}

	public synchronized Component add(JButton b) {
		buttons.add(b);
		return super.add(b);
	}

	@Override
	public void setEnabled(boolean enabled) {
		for (JButton b : buttons)
			b.setEnabled(enabled);
	}

	@Override
	public synchronized void repaint() {
		if (buttons != null) {
			int i = 0;
			for (JButton b : buttons) {
				b.setBounds(10, 30 * i + 10, getWidth() - 20, 25);
				i++;
			}
		}
	}
}

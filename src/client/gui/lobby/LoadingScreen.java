package client.gui.lobby;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.SwingWorker;

import client.model.ClientModel;

@SuppressWarnings("serial")
public class LoadingScreen extends JLayeredPane {

	private JLabel info;
	private JButton abort;
	private ClientModel model;

	public LoadingScreen(ClientModel model) {
		this.model = model;
		setBackground(Color.WHITE);
		setBorder(BorderFactory.createTitledBorder("Loading game..."));
		setBounds(100, 100, 250, 70);

		info = new JLabel(
				"<html>Please wait for all players to load and the game to start</html>");
		info.setVerticalAlignment(JLabel.TOP);
		add(info, DEFAULT_LAYER);

		abort = new JButton("Abort");
		abort.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new AbortQueue().execute();
			}
		});
		add(abort, POPUP_LAYER);

		setVisible(false);
	}

	@Override
	public void repaint() {
		int topPadding = 20;
		int botPadding = 5;
		int leftPadding = 5;
		int rightPadding = 5;

		if (info != null) {
			int h = getHeight() - topPadding - botPadding;
			int w = getWidth() - leftPadding - rightPadding;
			info.setBounds(leftPadding, topPadding, w, h);
		}
		if (abort != null) {
			int w = 120;
			int h = 25;
			abort.setBounds(getWidth() - w - botPadding, getHeight() - h
					- botPadding, w, h);
		}

		super.repaint();
	}

	private class AbortQueue extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			model.leaveQueue();
			return null;
		}

	}
}
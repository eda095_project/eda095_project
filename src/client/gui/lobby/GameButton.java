package client.gui.lobby;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import client.model.game.gamemode.GameMode;

@SuppressWarnings("serial")
public class GameButton extends JButton implements ActionListener {

	private GameInfo gi;
	private GameMode g;

	public GameButton(GameInfo gi, GameMode g) {
		super(g.getName());
		this.gi = gi;
		this.g = g;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		gi.setActive(g);
	}

}

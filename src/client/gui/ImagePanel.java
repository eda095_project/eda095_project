package client.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ImagePanel extends JPanel {

	private Image i;

	public ImagePanel(String file) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(file));
			setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
		} catch (IOException e) {
			System.err.println("Could not read image (" + file + ")");
		}
		i = img;
	}

	@Override
	public void paint(Graphics g) {
		if (i != null)
			g.drawImage(i, 0, 0, getWidth(), getHeight(), null);
	}

}

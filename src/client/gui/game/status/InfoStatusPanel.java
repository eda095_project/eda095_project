package client.gui.game.status;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;

import client.model.game.game.Game;

@SuppressWarnings("serial")
public class InfoStatusPanel extends TextStatusPanel {

	private String[] text = new String[] { "P - pause", "Q - quit", "G - ghost block", "U - I dare you", };

	public InfoStatusPanel(Game game, int width, int height) {
		super(game, width, height);
	}

	@Override
	protected void paintStatus(Graphics g) {
		g.setFont(DEFAULT_FONT);
		g.setColor(Color.BLACK);
		FontMetrics fm = g.getFontMetrics();
		int startY = fm.getHeight();
		for (String s : text) {
			g.drawString(s, 10, startY);
			startY += fm.getHeight();
		}
	}

}

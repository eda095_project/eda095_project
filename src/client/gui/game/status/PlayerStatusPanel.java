package client.gui.game.status;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.List;

import client.model.game.game.Game;

@SuppressWarnings("serial")
public class PlayerStatusPanel extends TextStatusPanel {

	public PlayerStatusPanel(Game game, int width, int height) {
		super(game, width, height);
	}

	@Override
	protected void paintStatus(Graphics g) {
		List<String> users = game.getPlayers();
		g.setFont(DEFAULT_FONT);
		FontMetrics fm = g.getFontMetrics();
		int startY = fm.getHeight();
		for (String s : users) {
			String[] data = s.split(",");
			if (data.length == 2) {
				g.setColor(Color.RED);
			} else {
				g.setColor(Color.BLACK);
			}
			g.drawString(data[0], 10, startY);
			startY += fm.getHeight();
		}
	}
}

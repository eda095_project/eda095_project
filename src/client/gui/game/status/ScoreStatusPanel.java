package client.gui.game.status;

import java.awt.Color;
import java.awt.Graphics;

import client.model.game.game.Game;

@SuppressWarnings("serial")
public class ScoreStatusPanel extends TextStatusPanel {

	public ScoreStatusPanel(Game game, int width, int height) {
		super(game, width, height);
	}

	@Override
	protected void paintStatus(Graphics g) {
		g.setColor(Color.BLACK);
		g.setFont(DEFAULT_FONT);
		g.drawString("Score: " + String.valueOf(game.getScore()), 10,
				getHeight() - 10);
	}
}

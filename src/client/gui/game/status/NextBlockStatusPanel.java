package client.gui.game.status;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;

import client.gui.game.board.SimpleTetrisBoard;
import client.gui.game.board.TetrisBoard;
import client.model.game.game.Game;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.block.SimpleBlock;

@SuppressWarnings("serial")
public class NextBlockStatusPanel extends TetrisStatusPanel {
	
	private BlockType next = BlockType.I;

	public NextBlockStatusPanel(Game game, int width, int height) {
		super(game, width, height);
	}

	@Override
	protected void paintStatus(Graphics g) {
		

		/* Draw next block */
		Block b = Block.generate(next, 0, 0);
		int size = TetrisBoard.BLOCK_SIZE - 1;
		int left = Integer.MAX_VALUE;
		int top = Integer.MAX_VALUE;
		int right = Integer.MIN_VALUE;
		int bottom = Integer.MIN_VALUE;
		for (SimpleBlock b2 : b) {
			int x = b2.getX();
			int y = b2.getY();
			left = (x < left) ? x : left;
			right = (x > right) ? x : right;
			top = (y < top) ? y : top;
			bottom = (y > bottom) ? y : bottom;
		}
		int blockWidth = (right + 1 - left);
		int blockHeight = (bottom + 1 - top);

		int globalX = getWidth() / 2 - (blockWidth * TetrisBoard.BLOCK_SIZE) / 2;
		int globalY = getHeight() / 2 - (blockHeight * TetrisBoard.BLOCK_SIZE) / 2;

		for (SimpleBlock b2 : b) {
			g.setColor(SimpleTetrisBoard.getColor(b));
			int x = (b2.getX() - left) * TetrisBoard.BLOCK_SIZE + globalX;
			int y = (b2.getY() - top) * TetrisBoard.BLOCK_SIZE + globalY;
			g.fillRect(x, y, size, size);
			g.setColor(Color.BLACK);
			g.drawRect(x, y, size, size);
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		next = game.getNextBlock();
		repaint();
	}

}

package client.gui.game.status;

import java.awt.Font;
import java.awt.event.ActionEvent;

import client.model.game.game.Game;

@SuppressWarnings("serial")
public abstract class TextStatusPanel extends TetrisStatusPanel {
	protected static final Font DEFAULT_FONT = new Font("Verdana", Font.BOLD, 15);
	
	public TextStatusPanel(Game game, int width, int height) {
		super(game, width, height);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
	}

}

package client.gui.game.status;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;

import client.model.game.game.Game;
import shared.Constants;

@SuppressWarnings("serial")
public class PopupPanel extends TextStatusPanel {

	public PopupPanel(Game game, int width, int height) {
		super(game, width, height);
	}

	@Override
	protected void paintStatus(Graphics g) {
		String state = game.getGameState();
		String text = "";
		String text2 = "";
		String text3 = "";
		if (state.equals(Constants.PAUSED)) {
			text = "Game is paused";
		} else if (state.equals(Constants.LOCKED)) {
			text = "Game is locked";
			text2 = "Waiting...";
		} else if (state.equals(Constants.ENDED)) {
			text = "GAME OVER";
			text2 = "" + game.getScore() + " points";
		}
		g.setColor(Color.BLACK);
		g.drawRect(1, 1, getWidth() - 2, getHeight() - 2);

		g.setFont(DEFAULT_FONT);
		FontMetrics fm = g.getFontMetrics();
		
		int x = getWidth() / 2 - fm.stringWidth(text) / 2;
		int y = getHeight() / 2 - fm.getHeight() / 2;

		g.drawString(text, x, y);

		int x2 = getWidth() / 2 - fm.stringWidth(text2) / 2;
		int y2 = y + fm.getHeight();
		g.drawString(text2, x2, y2);

		int x3 = getWidth() / 2 - fm.stringWidth(text3) / 2;
		int y3 = y2 + fm.getHeight();
		g.drawString(text3, x3, y3);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String state = game.getGameState();
		if (state.equals(Constants.PAUSED) || state.equals(Constants.LOCKED) || state.equals(Constants.ENDED)) {
			setVisible(true);
		} else {
			setVisible(false);
		}
		repaint();
	}
}

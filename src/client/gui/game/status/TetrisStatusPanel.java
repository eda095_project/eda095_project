package client.gui.game.status;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import client.model.game.game.Game;

@SuppressWarnings("serial")
public abstract class TetrisStatusPanel extends JPanel implements
		ActionListener {

	protected Game game;
	protected Timer t;

	public TetrisStatusPanel(Game game, int width, int height) {
		this.game = game;
		setPreferredSize(new Dimension(width, height));
		t = new Timer(200, this);
		t.start();
	}

	@Override
	public final void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		paintStatus(g);
	}

	protected abstract void paintStatus(Graphics g);

}

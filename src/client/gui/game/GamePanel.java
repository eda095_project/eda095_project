package client.gui.game;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLayeredPane;

import client.gui.ImagePanel;
import client.gui.game.background.TetrisBackground;
import client.gui.game.board.TetrisBoard;
import client.gui.game.status.PopupPanel;
import client.gui.game.status.TetrisStatusPanel;
import client.model.game.game.Game;

@SuppressWarnings("serial")
public class GamePanel extends JLayeredPane {

	private static final int PADDING = 20;

	private Game game;
	private TetrisBackground bg;
	private TetrisBoard board;
	private ImagePanel titleImage;
	private List<TetrisStatusPanel> statusPanels;
	private PopupPanel popup;

	private Rectangle bgBounds;

	public GamePanel(Game game, TetrisBackground bg, TetrisBoard board) {
		this.game = game;
		this.bg = bg;
		this.board = board;
		titleImage = new ImagePanel("title_small.png");
		statusPanels = new ArrayList<>();
		popup = new PopupPanel(game, 150, 100);
		popup.setBounds(10, 10, 150, 100);

		add(popup, POPUP_LAYER);
		add(titleImage, PALETTE_LAYER);
		add(board, PALETTE_LAYER);
		add(bg, DEFAULT_LAYER);
		setBoard(board);
	}

	private void updateBounds() {
		Rectangle titleBounds = new Rectangle(titleImage.getPreferredSize());
		titleBounds.x = PADDING;
		titleBounds.y = PADDING / 2;
		titleImage.setBounds(titleBounds);

		bgBounds = new Rectangle(board.getPreferredSize());
		Rectangle boardBounds = new Rectangle(board.getPreferredSize());
		boardBounds.x = PADDING;
		boardBounds.y = PADDING + titleBounds.height;
		board.setBounds(boardBounds);

		Rectangle popupBounds = new Rectangle(popup.getPreferredSize());
		int popupX = boardBounds.x + boardBounds.width / 2 - popupBounds.width / 2;
		int popupY = boardBounds.y + boardBounds.height / 3 - popupBounds.height / 2;
		popupBounds.x = popupX;
		popupBounds.y = popupY;
		popup.setBounds(popupBounds);

		int startX = boardBounds.width + 2 * PADDING;
		int startY = PADDING + titleBounds.height;
		int width = 0;
		for (TetrisStatusPanel status : statusPanels) {
			width = Math.max(width, status.getPreferredSize().width);
		}
		for (TetrisStatusPanel status : statusPanels) {
			Rectangle statusBounds = new Rectangle(status.getPreferredSize());
			statusBounds.x = startX;
			statusBounds.y = startY;
			startY += PADDING + statusBounds.height;
			status.setBounds(statusBounds);
		}

		bgBounds.width += 3 * PADDING + width;
		bgBounds.height += 2 * PADDING + titleBounds.height;
		bgBounds.height = Math.max(bgBounds.height, startY);
		bg.setBounds(bgBounds);
		setPreferredSize(new Dimension(bgBounds.width, bgBounds.height));
	}

	public void setBoard(TetrisBoard board) {
		remove(this.board);
		for (KeyListener kl : this.board.getKeyListeners()) {
			this.board.removeKeyListener(kl);
		}
		this.board = board;
		System.out.println(game);
		board.addKeyListener(game);
		add(board, PALETTE_LAYER);
		board.requestFocus();
		updateBounds();
		repaint();
	}

	public void setBackground(TetrisBackground bg) {
		remove(this.bg);
		this.bg = bg;
		add(bg, DEFAULT_LAYER);
		updateBounds();
		repaint();
	}

	public void addStatusPanel(TetrisStatusPanel tsp) {
		statusPanels.add(tsp);
		add(tsp, PALETTE_LAYER);
		updateBounds();
	}

	public void focus() {
		board.requestFocus();
	}

}

package client.gui.game.background;

import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class ColoredBackground extends TetrisBackground {

	private Color bg;
	
	public ColoredBackground(Color bg) {
		this.bg = bg;
	}
	
	public ColoredBackground(int r, int g , int b) {
		this(new Color(r,g,b));
	}
	
	@Override
	protected void paintBackground(Graphics g) {
		g.setColor(bg);
		g.fillRect(0, 0, getWidth(), getHeight());
	}

}

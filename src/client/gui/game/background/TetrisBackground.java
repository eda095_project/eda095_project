package client.gui.game.background;

import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class TetrisBackground extends JPanel {

	@Override
	public final void paint(Graphics g) {
		paintBackground(g);
	}

	protected abstract void paintBackground(Graphics g);
}

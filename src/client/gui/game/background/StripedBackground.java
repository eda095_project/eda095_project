package client.gui.game.background;

import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class StripedBackground extends TetrisBackground{

	@Override
	protected void paintBackground(Graphics g) {
		int x = Math.max(getHeight(), getWidth());
		g.setColor(Color.BLACK);
		for (int i = 0 ; i < x*2 ; i+=13) {
			g.drawLine(i, 0, 0, i);
		}
	}

}

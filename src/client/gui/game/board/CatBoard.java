package client.gui.game.board;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import client.model.game.game.Game;
import shared.game.Direction;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.block.SimpleBlock;

@SuppressWarnings("serial")
public class CatBoard extends SimpleTetrisBoard {

	private BufferedImage Icat;
	private BufferedImage Ocat;
	private BufferedImage Lcat;
	private BasicStroke widthTwo = new BasicStroke(2);
	private BasicStroke widthOne = new BasicStroke(1);

	public CatBoard(Game game) {
		super(game);

		Icat = null;
		Ocat = null;
		Lcat = null;

		try {
			Icat = ImageIO.read(new File("Icat.gif"));
			Lcat = ImageIO.read(new File("Lcat.jpg"));
			Ocat = ImageIO.read(new File("Ocat.jpg"));

		} catch (IOException e) {
		}
	}

	@Override
	protected void paintStaticBlocks(Graphics g, int size) {
		List<Block> staticBlocks = game.getStaticBlocks();
		for (Block b : staticBlocks) {
			for (SimpleBlock b2 : b) {
				paintSimpleBlock(g, size, b, b2);
			}
			paintBorder(g, b, size);
		}
	}

	@Override
	protected void paintCurrentBlock(Graphics g, int size) {
		Block current = game.getCurrent();
		if (current != null) {
			for (SimpleBlock b2 : current) {
				paintSimpleBlock(g, size, current, b2);
			}
			paintBorder(g, current, size);
		}
	}

	private void paintBorder(Graphics g, Block b, int size) {
		g.setColor(getColor(b));
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(widthTwo);
		for (SimpleBlock sb : b) {
			int x = sb.getX() * BLOCK_SIZE;
			int y = sb.getY() * BLOCK_SIZE;
			int x2 = x + size;
			int y2 = y + size;
			if (!b.hasPartInDirection(sb, Direction.LEFT)) {
				g.drawLine(x, y, x, y2);
			}
			if (!b.hasPartInDirection(sb, Direction.UP)) {
				g.drawLine(x, y, x2, y);
			}
			if (!b.hasPartInDirection(sb, Direction.RIGHT)) {
				g.drawLine(x2, y, x2, y2);
			}
			if (!b.hasPartInDirection(sb, Direction.DOWN)) {
				g.drawLine(x, y2, x2, y2);
			}
		}
		g2.setStroke(widthOne);

	}

	private void paintSimpleBlock(Graphics g, int size, Block b, SimpleBlock b2) {
		BlockType type = b.getType();
		Graphics2D g2 = (Graphics2D) g;
		int x = b2.getX() * BLOCK_SIZE;
		int y = b2.getY() * BLOCK_SIZE;
		Image i = null;
		switch (type) {
		case I:
			i = Icat;
			break;
		case O:
			i = Ocat;
			break;
		case L:
		default:
			i = Lcat;
			break;
		}
		int dx1 = x;
		int dy1 = y;
		int dx2 = x + size;
		int dy2 = y + size;
		int sx1 = 0;
		int sy1 = 0;
		int sx2 = i.getWidth(null);
		int sy2 = i.getHeight(null);

		if (type == BlockType.O) {
			switch (b2.getPart()) {
			case 0:
				sx1 = 0;
				sy1 = 0;
				sx2 = i.getWidth(null) / 2;
				sy2 = i.getHeight(null) / 2;
				break;
			case 1:
				sx1 = i.getWidth(null) / 2;
				sy1 = 0;
				sx2 = i.getWidth(null);
				sy2 = i.getHeight(null) / 2;
				break;
			case 2:
				sx1 = 0;
				sy1 = i.getHeight(null) / 2;
				sx2 = i.getWidth(null) / 2;
				sy2 = i.getHeight(null);
				break;
			case 3:
				sx1 = i.getWidth(null) / 2;
				sy1 = i.getHeight(null) / 2;
				sx2 = i.getWidth(null);
				sy2 = i.getHeight(null);
				break;
			}
		} else if (type == BlockType.I) {
			switch (b2.getPart()) {
			case 0:
				sx1 = 0;
				sy1 = 0;
				sx2 = i.getWidth(null);
				sy2 = i.getHeight(null) / 4;
				break;
			case 1:
				sx1 = 0;
				sy1 = i.getHeight(null) / 4;
				sx2 = i.getWidth(null);
				sy2 = i.getHeight(null) / 2;
				break;
			case 2:
				sx1 = 0;
				sy1 = i.getHeight(null) / 2;
				sx2 = i.getWidth(null);
				sy2 = 3 * i.getHeight(null) / 4;
				break;
			case 3:
				sx1 = 0;
				sy1 = 3 * i.getHeight(null) / 4;
				sx2 = i.getWidth(null);
				sy2 = i.getHeight(null);
				break;
			}
		}
		g2.drawImage(i, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, Color.WHITE, null);
		
	}

}

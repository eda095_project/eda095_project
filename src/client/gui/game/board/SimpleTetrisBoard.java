package client.gui.game.board;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import client.model.game.game.Game;
import shared.game.block.Block;
import shared.game.block.SimpleBlock;

@SuppressWarnings("serial")
public class SimpleTetrisBoard extends TetrisBoard {

	public SimpleTetrisBoard(Game game) {
		super(game);
	}

	public static Color getColor(Block b) {
		switch (b.getType()) {
		case O:
			return new Color(0xF280A1);
		case I:
			return new Color(0xf7941e);
		case S:
			return new Color(0xCC2127);
		case L:
			return new Color(0x3853A4);
		case J:
			return new Color(0x9932CC);
		case Z:
			return new Color(0x00FFFF);
		case T:
			return new Color(0x008000);
		default:
			return new Color(0x000000);
		}
	}

	@Override
	protected void paintStaticBlocks(Graphics g, int size) {
		/* Draw static blocks */
		List<Block> staticBlocks = game.getStaticBlocks();
		for (Block b : staticBlocks) {
			g.setColor(getColor(b));
			for (SimpleBlock b2 : b) {
				int x = b2.getX() * BLOCK_SIZE;
				int y = flipYIfApplicable(b2.getY()) * BLOCK_SIZE;
				g.fillRect(x, y, size, size);
			}
		}
	}

	@Override
	protected void paintGhostBlock(Graphics g, int size) {
		/* Draw ghost block */
		if (game.isGhostMode()) {
			int ghostSize = size - 1;
			Block ghost = game.getGhost();
			if (ghost != null) {
				for (SimpleBlock b2 : ghost) {
					int x = b2.getX() * BLOCK_SIZE;
					int y = flipYIfApplicable(b2.getY()) * BLOCK_SIZE;
					g.setColor(Color.LIGHT_GRAY);
					g.fillRect(x, y, ghostSize, ghostSize);
					g.setColor(Color.BLACK);
					g.drawRect(x, y, ghostSize, ghostSize);
					g.drawLine(x, y, x + ghostSize, y + ghostSize);
					g.drawLine(x + ghostSize, y, x, y + ghostSize);
				}
			}
		}
	}

	@Override
	protected void paintCurrentBlock(Graphics g, int size) {
		/* Draw current block */
		Block current = game.getCurrent();
		if (current != null) {
			g.setColor(getColor(current));
			for (SimpleBlock b2 : current) {
				int x = b2.getX() * BLOCK_SIZE;
				int y = flipYIfApplicable(b2.getY()) * BLOCK_SIZE;
				g.fillRect(x, y, size, size);
			}
		}
	}

	@Override
	protected void paintOtherBlocks(Graphics g, int size) {
		/* Draw other current blocks */
		for (Block other : game.getOther()) {
			g.setColor(getColor(other));
			for (SimpleBlock b2 : other) {
				int x = b2.getX() * BLOCK_SIZE;
				int y = flipYIfApplicable(b2.getY()) * BLOCK_SIZE;
				g.fillRect(x, y, size, size);
			}
		}
	}
}

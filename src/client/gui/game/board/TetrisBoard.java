package client.gui.game.board;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import client.model.game.game.Game;

@SuppressWarnings("serial")
public abstract class TetrisBoard extends JPanel implements ActionListener {

	public static final int BLOCK_SIZE = 28;

	protected int width;
	protected int height;
	protected Game game;
	private Timer t;

	public TetrisBoard(Game game) {
		this.game = game;
		this.width = game.getWidth();
		this.height = game.getHeight();
		setPreferredSize(new Dimension(width * BLOCK_SIZE, height * BLOCK_SIZE));
		setFocusable(true);
		t = new Timer(40, this);
		t.start();
	}
	
	protected int flipYIfApplicable(int y) {
		if (game.isFlipped()) {
			return height - y - 1;
		}
		return y;
	}

	@Override
	protected final void paintComponent(Graphics g) {
		paintBackground(g);
		paintBlocks(g);
	}

	private void paintBackground(Graphics g) {
		Color bg = Color.WHITE;
		g.setColor(bg);
		g.fillRect(0, 0, getWidth(), getHeight());
		Color grid = new Color(200, 200, 200);
		g.setColor(grid);
		int size = BLOCK_SIZE - 1;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				g.drawRect(x * BLOCK_SIZE, y * BLOCK_SIZE, size, size);
			}
		}
	}

	protected void paintBlocks(Graphics g) {
		int size = BLOCK_SIZE - 1;
		paintStaticBlocks(g, size);
		paintGhostBlock(g, size);
		paintCurrentBlock(g, size);
		paintOtherBlocks(g, size);
	}
	
	protected abstract void paintStaticBlocks(Graphics g, int size);
	protected abstract void paintGhostBlock(Graphics g, int size);
	protected abstract void paintCurrentBlock(Graphics g, int size);
	protected abstract void paintOtherBlocks(Graphics g, int size);
	
	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
	}
	
}

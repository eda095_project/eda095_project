package client.model.game.gamemode;

import client.model.game.game.Game;
import client.model.game.game.OfflineGame;
import connection.Client;

public class OfflineGamemode extends GameMode {

	private static final String DATA = "offline:Offline mode:1:Offline mode, if you have a bad internet connection";

	public OfflineGamemode() {
		super(DATA);
	}

	@Override
	public Game getGame(Client c) {
		return new OfflineGame(this);
	}

	
	@Override
	public boolean isOnlineMode() {
		return false;
	}
}

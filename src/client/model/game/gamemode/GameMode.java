package client.model.game.gamemode;

import java.util.Observable;

import client.model.game.game.Game;
import client.model.game.game.NetworkedGame;
import connection.Client;

public class GameMode extends Observable implements Comparable<GameMode> {

	private String type;
	private String name;
	private String info;
	private int players;
	private int playing;
	private int queueing;

	public GameMode(String data) {
		String[] d = data.split(":");
		type = d[0];
		name = d[1];
		players = Integer.parseInt(d[2]);
		info = d[3];
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public int getPlaying() {
		return playing;
	}

	public int getQueueing() {
		return queueing;
	}

	@Override
	public String toString() {
		return getName();
	}

	public int getPlayers() {
		return players;
	}

	public String toHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append(name);
		sb.append("<br>");
		sb.append(info);
		sb.append("<br>");
		sb.append("</html>");
		return sb.toString();
	}

	public void updateData(int p, int q) {
		if (p != playing) {
			playing = p;
			setChanged();
		}
		if (q != queueing) {
			queueing = q;
			setChanged();
		}
		notifyObservers();
	}

	@Override
	public int compareTo(GameMode o) {
		return name.compareTo(o.name);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GameMode)
			return ((GameMode) obj).compareTo(this) == 0;
		return false;
	}

	public Game getGame(Client c) {
		return new NetworkedGame(this, c);
	}

	public boolean isOnlineMode() {
		return true;
	}

}
 package client.model.game.game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.Timer;

import shared.Constants;
import shared.game.Direction;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.board.Player;
import client.model.game.board.ClientBoardOffline;
import client.model.game.gamemode.GameMode;

public class OfflineGame extends Game implements ActionListener {

	private static final int DEFAULT_DELAY = 1000;
	private static final int[] pointModifiers = { 0, 40, 100, 300, 1200 };

	protected ClientBoardOffline board;
	private Timer timer;

	public OfflineGame(GameMode gm) {
		super(gm);
		players.add("OFFLINE");
		timer = new Timer(DEFAULT_DELAY, this);
		gameState = Constants.STARTED;
	}

	public synchronized void gameTick() {
		boolean[] movedIndices = board.moveCurrentsDownIfLegal();
		for (int i = 0; i < movedIndices.length; i++) {
			if (!movedIndices[i]) {
				int tetris = updateBoardWithNewCurrent(board.getPlayer(i));
				lines += tetris;
				score += (pointModifiers[tetris] * level);
				updateLevel();
			}
		}
	}

	private synchronized void updateLevel() {
		int tempLevel = 1 + lines / 10;
		if (tempLevel > level) {
			level = tempLevel;
			timer.setDelay((int) (1000 * 1.17 * Math.pow(Math.E, -0.4 * (level - 1))));
		}
	}

	private synchronized int updateBoardWithNewCurrent(Player player) {
		int rowsRemoved = 0;
		Block current = board.getCurrent(player);
		if (current != null) {
			board.addStaticBlock(current);
			rowsRemoved = checkRemoveRows(current);
		}
		if (!board.setCurrent(player, board.generateRandomBlock())) {
			gameState = Constants.ENDED;
		}
		return rowsRemoved;
	}

	protected synchronized int checkRemoveRows(Block current) {
		int rowsRemoved = 0;
		Set<Integer> possibleRowsToRemove = current.getYVals();
		List<Integer> rowsToRemove = new ArrayList<>();
		synchronized (board) {
			for (int row : possibleRowsToRemove) {
				if (board.hasRow(row)) {
					rowsToRemove.add(row);
					rowsRemoved++;
				}
			}
			Collections.sort(rowsToRemove);
			board.removeRows(rowsToRemove);
		}
		return rowsRemoved;
	}

	@Override
	public synchronized int getInfo() {
		super.board = board = new ClientBoardOffline();
		notifyAll();
		return Constants.GAMEINFO_IN_GAME;
	}

	@Override
	public synchronized boolean join() {
		timer.start();
		return true;
	}

	@Override
	public synchronized void doLeft() {
		if (gameState == Constants.STARTED) {
			board.moveCurrentIfLegal(board.getPlayer(0), Direction.LEFT);
		}

	}

	@Override
	public synchronized void doRight() {
		if (gameState == Constants.STARTED) {
			board.moveCurrentIfLegal(board.getPlayer(0), Direction.RIGHT);
		}
	}

	@Override
	public synchronized void doDown() {
		if (gameState == Constants.STARTED) {
			if (board.moveCurrentIfLegal(board.getPlayer(0), Direction.DOWN)) {
				score++;
			}
		}
	}

	@Override
	public synchronized void doPause() {
		if (gameState == Constants.STARTED) {
			gameState = Constants.PAUSED;
			timer.stop();
		} else if (gameState == Constants.PAUSED) {
			gameState = Constants.STARTED;
			timer.start();
		}
	}

	@Override
	public synchronized void doFoceDown() {
		if (gameState == Constants.STARTED) {
			int spaces = board.forceDown(board.getPlayer(0));
			score += spaces;
		}
	}

	@Override
	public synchronized void doRotate() {
		if (gameState == Constants.STARTED) {
			board.rotateCurrentIfLegal(board.getPlayer(0));
		}
	}

	@Override
	public BlockType getNextBlock() {
		return board.getNext().getType();
	}

	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		gameTick();
	}

}

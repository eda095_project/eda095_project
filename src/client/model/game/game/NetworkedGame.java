package client.model.game.game;

import static shared.Constants.BOARD;
import static shared.Constants.BOARD_VERSION;
import static shared.Constants.BOARD_VERSION_ALL;
import static shared.Constants.COLS;
import static shared.Constants.COMMAND_DOWN;
import static shared.Constants.COMMAND_FORCE_DOWN;
import static shared.Constants.COMMAND_LEFT;
import static shared.Constants.COMMAND_PAUSE;
import static shared.Constants.COMMAND_RIGHT;
import static shared.Constants.COMMAND_ROTATE;
import static shared.Constants.CURRENT;
import static shared.Constants.GHOST;
import static shared.Constants.NEXT;
import static shared.Constants.ROWS;
import static shared.Constants.YOUR_CURRENT;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;
import shared.connection.packets.BoardPacket;
import shared.connection.packets.GameInfoPacket;
import shared.connection.packets.JoinGamePacket;
import shared.connection.packets.KeyPressPacket;
import shared.game.block.Block;
import shared.game.block.BlockType;
import client.model.game.board.ClientBoardNetwork;
import client.model.game.gamemode.GameMode;
import connection.Client;
import connection.packets.data.BooleanPacket;
import connection.packets.operands.JSONPacket;
import connection.packets.operands.OperatorPacket;
import connection.packets.operands.ResponsePacket;

public class NetworkedGame extends Game {

	private Client c;
	private BoardFetcher bf;
	protected ClientBoardNetwork board;

	public NetworkedGame(GameMode gm, Client c) {
		super(gm);
		this.c = c;
		bf = new BoardFetcher();
	}

	@Override
	public synchronized int getInfo() {
		byte id = c.send(new GameInfoPacket());

		OperatorPacket op = c.recieve(id);
		JSONPacket jp = (JSONPacket) op;

		int status = jp.getInt(Constants.GAMEINFO_STATUS);

		if (status == Constants.GAMEINFO_IN_GAME) {
			int cols = jp.getInt(COLS);
			int rows = jp.getInt(ROWS);
			super.board = board = new ClientBoardNetwork(cols, rows);
			bf.start();
			notifyAll();
		}
		return status;
	}

	@Override
	public synchronized boolean join() {
		String type = gm.getType();
		byte id = c.send(new JoinGamePacket(type));
		ResponsePacket rp = (ResponsePacket) c.recieve(id);
		BooleanPacket bp = (BooleanPacket) rp.getPacket();
		return bp.toBoolean();
	}

	@Override
	public synchronized void doLeft() {
		c.sendNoResponse(new KeyPressPacket(COMMAND_LEFT));
	}

	@Override
	public synchronized void doRight() {
		c.sendNoResponse(new KeyPressPacket(COMMAND_RIGHT));
	}

	@Override
	public synchronized void doDown() {
		c.sendNoResponse(new KeyPressPacket(COMMAND_DOWN));
	}

	@Override
	public synchronized void doPause() {
		c.sendNoResponse(new KeyPressPacket(COMMAND_PAUSE));
	}

	@Override
	public synchronized void doFoceDown() {
		c.sendNoResponse(new KeyPressPacket(COMMAND_FORCE_DOWN));
	}

	@Override
	public synchronized void doRotate() {
		c.sendNoResponse(new KeyPressPacket(COMMAND_ROTATE));
	}

	private class BoardFetcher implements ActionListener {

		private Timer t;
		private boolean first;

		public BoardFetcher() {
			first = true;
			t = new Timer(40, this);
		}

		public void start() {
			t.start();
		}

		@Override
		public synchronized void actionPerformed(ActionEvent e) {

			OperatorPacket op = c.sendRecieve(new BoardPacket(first));
			JSONPacket jsonPacket = (JSONPacket) op;
			JSONObject json = jsonPacket.getJSON();

			if (!json.getString(BOARD_VERSION).equals(BOARD_VERSION_ALL)) {
				System.err.println("Boardfetcher can't handle this version!!");
				System.exit(3);
			}

			JSONArray currents = json.getJSONArray(CURRENT);
			ArrayList<Block> others = new ArrayList<>();
			for (int i = 0; i < currents.length(); i++) {
				JSONObject current = currents.getJSONObject(i);
				Block b = Block.generate(current);
				if (current.getBoolean(YOUR_CURRENT)) {
					board.setCurrent(b);
				} else {
					others.add(b);
				}
			}
			board.setOther(others);

			if (json.has(NEXT)) {
				BlockType nextType = BlockType.generate(json.getInt(NEXT));
				Block b = Block.generate(nextType, 0, 0);
				board.setNext(b);
			}

			if (json.has(GHOST)) {
				Block b = Block.generate(json.getJSONObject(GHOST));
				board.setGhost(b);
			}

			if (json.has(BOARD)) {
				JSONArray staticBlocks = json.getJSONArray(BOARD);
				board.clearStaticBlocks();
				for (Object o : staticBlocks) {
					JSONObject jsonBlock = (JSONObject) o;
					Block b = Block.generate(jsonBlock);
					board.addStaticBlock(b);
				}
			}

			if (first)
				first = !first;
		}
	}

}

package client.model.game.game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import client.model.game.gamemode.GameMode;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.board.ClientBoard;

public abstract class Game implements KeyListener {

	protected GameMode gm;
	protected ClientBoard board;

	private boolean flipped;
	private boolean ghostMode;

	protected int score = 0;
	protected int level = 0;
	protected int lines = 0;

	protected String gameState ="";
	protected List<String> players = new ArrayList<>();

	public Game(GameMode gm) {
		this.gm = gm;
	}
	
	public synchronized GameMode getGameMode() {
		return gm;
	}

	public abstract int getInfo();

	public synchronized void waitForGame() {
		while (board == null) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
	}

	public abstract boolean join();

	public int getHeight() {
		return board.getHeight();
	}

	public int getWidth() {
		return board.getWidth();
	}

	private synchronized void toggleFlipped() {
		flipped = !flipped;
	}

	public synchronized boolean isFlipped() {
		return flipped;
	}

	private synchronized void toggleGhostMode() {
		ghostMode = !ghostMode;
	}

	public synchronized boolean isGhostMode() {
		return ghostMode;
	}

	@Override
	public void keyPressed(KeyEvent ke) {
		switch (ke.getKeyCode()) {
		case KeyEvent.VK_Q:
			System.exit(0);
			break;
		case KeyEvent.VK_U:
			toggleFlipped();
			break;
		case KeyEvent.VK_G:
			toggleGhostMode();
			break;
		case KeyEvent.VK_LEFT:
			doLeft();
			break;
		case KeyEvent.VK_RIGHT:
			doRight();
			break;
		case KeyEvent.VK_DOWN:
			doDown();
			break;
		case KeyEvent.VK_UP:
			doRotate();
			break;
		case KeyEvent.VK_SPACE:
			doFoceDown();
			break;
		case KeyEvent.VK_P:
			doPause();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {
	}

	@Override
	public void keyTyped(KeyEvent ke) {

	}

	public abstract void doLeft();

	public abstract void doRight();

	public abstract void doDown();

	public abstract void doPause();

	public abstract void doFoceDown();

	public abstract void doRotate();

	public List<Block> getStaticBlocks() {
		return board.getStaticBlocks();
	}

	public Block getGhost() {
		return board.getGhost();
	}

	public Block getCurrent() {
		return board.getCurrent();
	}

	public List<Block> getOther() {
		return board.getOther();
	}

	public BlockType getNextBlock() {
		return board.getNext().getType();
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public void setLines(int lines) {
		this.lines = lines;
	}

	public int getLines() {
		return lines;
	}

	public void setPlayers(List<String> players) {
		this.players = players;
	}

	public List<String> getPlayers() {
		return players;
	}

	public void setGameState(String gameState) {
		this.gameState = gameState;
	}

	public String getGameState() {
		return gameState;
	}
}

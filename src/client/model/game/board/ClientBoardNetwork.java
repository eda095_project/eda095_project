package client.model.game.board;

import java.util.ArrayList;
import java.util.List;

import shared.game.block.Block;
import shared.game.board.ClientBoard;

public class ClientBoardNetwork implements ClientBoard {
	
	private ArrayList<Block> staticBlocks, staticBlocksCopy;
	private Block next, current, ghost;
	private List<Block> other = new ArrayList<>();
	private boolean staticChanged;
	private int width, height;
	
	public ClientBoardNetwork(int width, int height) {
		this.width = width;
		this.height = height;
		staticBlocks = new ArrayList<Block>();
		staticBlocksCopy = new ArrayList<Block>();
	}
	
	public synchronized int getWidth() {
		return width;
	}
	
	public synchronized int getHeight() {
		return height;
	}

	public synchronized Block getNext() {
		return next;
	}
	
	public synchronized void setNext(Block b) {
		next = b;
	}
	
	public synchronized List<Block> getOther() {
		return other;
	}
	
	public synchronized void setOther(List<Block> others) {
		other = others;
	}

	public synchronized Block getGhost() {
		return ghost;
	}
	
	public synchronized void setGhost(Block b) {
		ghost = b;
	}

	public synchronized Block getCurrent() {
		return current;
	}
	
	public synchronized void setCurrent(Block b) {
		current = b;
	}

	public synchronized List<Block> getStaticBlocks() {
		if (staticChanged) {
			staticChanged = false;
			staticBlocksCopy.clear();
			for (Block b: staticBlocks) {
				staticBlocksCopy.add(b);
			}
		}
		return staticBlocksCopy;
	}
	
	public synchronized void addStaticBlock(Block b) {
		staticBlocks.add(b);
		staticChanged = true;
	}
	
	public synchronized void clearStaticBlocks() {
		staticBlocks.clear();
	}

}

package client.model.game.board;

import java.util.ArrayList;
import java.util.List;

import shared.game.block.Block;
import shared.game.block.generator.StandardGenerator;
import shared.game.board.ClientBoard;
import shared.game.board.TetrisBoard;

public class ClientBoardOffline extends TetrisBoard implements ClientBoard {

	public ClientBoardOffline() {
		this(DEFAULT_ROWS, DEFAULT_COLUMNS);
	}

	public ClientBoardOffline(int rows, int columns) {
		super(rows, columns, new StandardGenerator(), 1);
	}

	@Override
	public synchronized int getWidth() {
		return getCols();
	}

	@Override
	public synchronized int getHeight() {
		return getRows();
	}

	@Override
	public synchronized Block getGhost() {
		return playerList[0].getGhost();
	}

	@Override
	public synchronized Block getCurrent() {
		return playerList[0].getCurrent();
	}

	@Override
	public synchronized List<Block> getStaticBlocks() {
		return staticBlocks;
	}

	@Override
	public List<Block> getOther() {
		return new ArrayList<Block>();
	}
}
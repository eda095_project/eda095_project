package client.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

import javax.swing.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;
import shared.chat.Message;
import shared.connection.factories.ClientTetrisFactory;
import shared.connection.models.ClientTetrisModel;
import shared.connection.packets.ChatPacket;
import shared.connection.packets.HandshakePacket;
import shared.connection.packets.LeaveQueuePacket;
import shared.connection.packets.SessionPacket;
import shared.connection.packets.StatusPacket;
import shared.data.BlockingQueue;
import client.model.game.game.Game;
import client.model.game.gamemode.GameMode;
import client.model.game.gamemode.OfflineGamemode;
import connection.Client;
import connection.packets.operands.JSONPacket;

public class ClientModel extends Observable implements ClientTetrisModel {

	private static final String VERSION = "JAVA:1.1";
	private static final int DEFAULT_PORT = 30000;

	private Data d;
	private Client c;

	private Game game;

	private int connectionState;
	private HashMap<String, GameMode> gamemodes;
	private List<String> users;
	private BlockingQueue<Message> chatMessages;
	private BlockingQueue<String> announcements;

	public ClientModel(Data d) throws IOException {
		this.d = d;
		gamemodes = new HashMap<>();
		users = new ArrayList<>();
		chatMessages = new BlockingQueue<>();
		announcements = new BlockingQueue<>();
	}

	public boolean isOnline() {
		if (c != null && c.isAlive())
			return true;
		return false;
	}

	public boolean connect(String host) throws UnknownHostException,
			IOException {
		c = new Client(host, DEFAULT_PORT, new ClientTetrisFactory(this));
		d.addHost(host);
		d.save();
		new StatusFetcher();
		return true;
	}

	public void disconnect() throws IOException {
		c.close();
		c = null;
	}

	public void login(String name, boolean reconnect) {
		if (isOnline()) {
			HashMap<String, String> data = new HashMap<>();
			if (reconnect) {
				data.put("session", d.getSession());
			} else {
				data.put("name", name);
				data.put("os", System.getProperty("os.name"));
			}
			data.put("version", VERSION);

			HandshakePacket hsp = new HandshakePacket(data);
			JSONPacket ret = (JSONPacket) c.sendRecieve(hsp);
			JSONObject obj = ret.getJSON();

			JSONArray array = obj.getJSONArray(Constants.GAMEMODES);
			for (Object o : array) {
				GameMode g = new GameMode(o.toString());
				gamemodes.put(g.getType(), g);
			}

			if (reconnect) {
				String gametype = obj.getString(Constants.GAMEMODE);

				GameMode gm = gamemodes.get(gametype);
				game = gm.getGame(c);
				game.getInfo();
			} else {
				String s = obj.getString(Constants.SESSION);
				d.setSession(s);
				try {
					d.save();
				} catch (IOException e) {
				}
			}
		} else {
			setState(Constants.CONNECTION_STATE_LOBBY);
		}
	}

	public synchronized List<GameMode> getGamemodes() {
		ArrayList<GameMode> list = new ArrayList<>();
		for (String key : gamemodes.keySet()) {
			list.add(gamemodes.get(key));
		}
		Collections.sort(list);
		list.add(new OfflineGamemode());
		return list;
	}

	public boolean joinGame(GameMode gm) {
		Game game = gm.getGame(c);
		boolean accepted = game.join();
		if (accepted) {
			this.game = game;
			if (!gm.isOnlineMode())
				setState(Constants.CONNECTION_STATE_GAME);
		}
		return accepted;
	}

	public Game getGame() {
		return game;
	}

	private void setState(int state) {
		if (state != this.connectionState) {
			this.connectionState = state;
			setChanged();
			new Thread() {
				public void run() {
					notifyObservers();
				};
			}.start();
		}
	}

	public int getState() {
		return connectionState;
	}

	private class StatusFetcher implements ActionListener {

		private Timer t;

		public StatusFetcher() {
			t = new Timer(100, this);
			t.start();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			synchronized (ClientModel.this) {
				if (c == null) {
					t.stop();
					return;
				}
				if (connectionState == Constants.CONNECTION_STATE_GAME
						&& !game.getGameMode().isOnlineMode())
					return;

				JSONPacket json = (JSONPacket) c
						.sendRecieve(new StatusPacket());
				JSONObject obj = json.getJSON();
				int state = obj.getInt(Constants.CONNECTION_STATE);
				JSONObject data = obj.getJSONObject("data");

				if (state == Constants.CONNECTION_STATE_LOBBY) {
					JSONObject gms = data
							.getJSONObject(Constants.STATUS_DATA_GAMEMODES);
					for (String gameMode : gms.keySet()) {
						JSONObject list = gms.getJSONObject(gameMode);
						GameMode gm = gamemodes.get(gameMode);
						int playing = list.getInt("playing");
						int queue = list.getInt("queue");
						gm.updateData(playing, queue);
					}

					ArrayList<String> users = new ArrayList<>();
					for (Object o : data
							.getJSONArray(Constants.STATUS_DATA_CHAT)) {
						users.add(o.toString());
					}
					ClientModel.this.users = users;

					JSONArray chatMessages = data
							.getJSONArray(Constants.STATUS_DATA_CHATMESSAGES);
					for (Object o : chatMessages) {
						JSONObject chat = (JSONObject) o;
						ClientModel.this.chatMessages.offer(new Message(chat));
					}

					setState(state);
				} else if (state == Constants.CONNECTION_STATE_GAME
						&& game != null) {
					game.setScore(data.getInt(Constants.SCORE));
					game.setLevel(data.getInt(Constants.LEVEL));
					game.setLines(data.getInt(Constants.LINES));
					game.setGameState(data.getString(Constants.GAME_STATE));

					ArrayList<String> players = new ArrayList<>();
					for (Object o : data.getJSONArray(Constants.PLAYER_NAMES)) {
						players.add(o.toString());
					}
					game.setPlayers(players);
					setState(state);
				}
			}
		}
	}

	public boolean hasActiveSession() {
		String session = d.getSession();
		if (session == null)
			return false;
		byte id = c.send(new SessionPacket(session));
		JSONPacket rp = (JSONPacket) c.recieve(id);
		JSONObject obj = rp.getJSON();
		return obj.getBoolean("valid");
	}

	public void leaveQueue() {
		c.sendNoResponse(new LeaveQueuePacket());
	}

	public synchronized List<String> getUsers() {
		return users;
	}

	public void sendChat(String text) {
		c.sendNoResponse(new ChatPacket(text));
	}

	public Message getChat() {
		return chatMessages.poll();
	}

	@Override
	public void announce(String message) {
		announcements.offer(message);
	}

	public String getAnnouncement() {
		return announcements.poll();
	}
}
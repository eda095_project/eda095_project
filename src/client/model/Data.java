package client.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;

public class Data {
	private File file;
	private JSONObject data;

	public Data() throws IOException {
		File root = new File(System.getProperty("user.home"));
		File folder = new File(root, Constants.DATA_FOLDER);
		if (!folder.exists())
			folder.mkdir();

		file = new File(folder, Constants.DATA_FILE);
		if (file.exists()) {
			load();
		} else {
			data = new JSONObject();
		}
		save();
	}

	private void load() throws FileNotFoundException {
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(file);
		while (s.hasNextLine())
			sb.append(s.nextLine());
		s.close();
		data = new JSONObject(sb.toString());
	}

	public void save() throws IOException {
		FileWriter fw = new FileWriter(file);
		fw.write(data.toString(4));
		fw.flush();
		fw.close();
	}

	public List<String> getHosts() {
		ArrayList<String> hosts = new ArrayList<>();
		if (!data.has("hosts")) {
			JSONArray ja = new JSONArray();
			ja.put("localhost");
			data.put("hosts", ja);
		}
		JSONArray array = data.getJSONArray("hosts");
		for (Object o : array) {
			hosts.add(o.toString());
		}
		return hosts;
	}

	public void addHost(String host) {
		if (!getHosts().contains(host))
			data.getJSONArray("hosts").put(host);
	}

	public String getSession() {
		if (data.has("session"))
			return data.getString("session");
		return null;
	}
	
	public void setSession(String session) {
		data.put("session", session);
	}
	
	public int getHighScore() {
		if (data.has("highscore"))
			return data.getInt("highscore");
		return -1;
	}
	
	public void setHighScore(int score) {
		data.put("highscore", score);
		
	}
}

package client.singleplayergame;

import java.util.ArrayList;
import java.util.List;

import shared.game.block.Block;
import shared.game.block.generator.BlockGenerator;
import shared.game.board.TetrisBoard;

public class OfflineTetrisBoard extends TetrisBoard {
	private List<Block> staticBlocksCopy;

	public OfflineTetrisBoard(BlockGenerator generator) {
		this(DEFAULT_ROWS, DEFAULT_COLUMNS, generator);
	}

	public OfflineTetrisBoard(int rows, int columns, BlockGenerator generator) {
		super(rows, columns, generator, 1);
		staticBlocksCopy = new ArrayList<Block>();
	}

	public synchronized List<Block> getStaticBlocks() {
		if (playerList[0].export()) {
			staticBlocksCopy.clear();
			for (Block b : staticBlocks) {
				staticBlocksCopy.add(b);
			}
		}
		return staticBlocksCopy;
	}

	public int getWidth() {
		return getCols();
	}

	public int getHeight() {
		return getRows();
	}

}
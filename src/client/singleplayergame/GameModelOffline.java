//package client.singleplayergame;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.KeyEvent;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//import javax.swing.Timer;
//
//import client.model.Data;
//import client.model.GameModelImpl;
//import shared.Constants;
//import shared.game.block.Block;
//import shared.game.block.BlockType;
//
//public class GameModelOffline extends GameModelImpl implements ActionListener {
//	public static final int DEFAULT_DELAY = 1000;
//
//	protected enum State {
//		NOT_STARTED, STARTED, PAUSED, ENDED
//	};
//
//	private Data d;
//	protected Timer timer;
//	protected OfflineTetrisBoard board;
//	protected int delay;
//	protected State gameState = State.NOT_STARTED;
//	protected int score;
//	private int lines = 0;
//	private int level = 1;
//	private static final int[] pointModifiers = { 0, 40, 100, 300, 1200 };
//	private boolean ghostMode;
//	private boolean upSideDownMode;
//	private boolean isHighScore;
//
//	public GameModelOffline(Data d) {
//		this.d = d;
//		delay = DEFAULT_DELAY;
//		timer = new Timer(delay, this);
//		board = new OfflineTetrisBoard();
//	}
//
//	public void gameTick() {
//		boolean[] movedIndices = board.moveCurrentsDownIfLegal();
//		score += 1;
//		for (int i = 0; i < movedIndices.length; i++) {
//			if (!movedIndices[i]) {
//				int tetris = updateBoardWithNewCurrent(i);
//				lines += tetris;
//				score += (pointModifiers[tetris] * level);
//				updateLevel();
//			}
//		}
//	}
//
//	private void updateLevel() {
//		int tempLevel = 1 + lines / 10;
//		if (tempLevel > level) {
//			level = tempLevel;
//			timer.setDelay((int) (1000 * 1.17 * Math.pow(Math.E, -0.4 * (level - 1))));
//		}
//	}
//
//	public synchronized void startGame() {
//		gameState = State.STARTED;
//		timer.start();
//	}
//
//	public synchronized void stopGame() {
//		gameState = State.ENDED;
//		checkSaveHighScore();
//		timer.stop();
//	}
//	
//	private void checkSaveHighScore() {
//		int highscore = d.getHighScore();
//		if (score > highscore) {
//			d.setHighScore(score);
//			isHighScore = true;
//			try {
//				d.save();
//			} catch (IOException e) {
//			}
//		}
//	}
//
//	public synchronized void togglePauseGame() {
//		if (gameState == State.STARTED) {
//			pauseGame();
//		} else if (gameState == State.PAUSED) {
//			resumeGame();
//		}
//	}
//
//	public synchronized void forcePauseGame() {
//		pauseGame();
//	}
//
//	private synchronized void pauseGame() {
//		gameState = State.PAUSED;
//		timer.stop();
//	}
//
//	private synchronized void resumeGame() {
//		gameState = State.STARTED;
//		timer.start();
//	}
//
//	public synchronized void resetGame() {
//		gameState = State.NOT_STARTED;
//		board.reset();
//		timer.stop();
//	}
//
//	/**
//	 * The player pressed left
//	 */
//	public synchronized void left() {
//		if (gameState == State.STARTED) {
//			board.moveCurrentIfLegal(0, -1, 0);
//		}
//	}
//
//	/**
//	 * The player pressed right
//	 * 
//	 */
//	public synchronized void right() {
//		if (gameState == State.STARTED) {
//			board.moveCurrentIfLegal(0, 1, 0);
//		}
//	}
//
//	/**
//	 * The player pressed down
//	 */
//	public synchronized void down() {
//		if (gameState == State.STARTED) {
//			if (board.moveCurrentIfLegal(0, 0, 1)) {
//				score++;
//			}
//		}
//	}
//
//	/**
//	 * The player pressed rotate
//	 */
//	public synchronized void rotate() {
//		if (gameState == State.STARTED) {
//			board.rotateCurrentIfLegal(0);
//		}
//	}
//
//	/**
//	 * The player pressed forceDown
//	 */
//	public synchronized void forceDown() {
//		if (gameState == State.STARTED) {
//			int spaces = board.forceDown(0);
//			score += spaces;
//		}
//	}
//
//	@Override
//	public void actionPerformed(ActionEvent e) {
//		synchronized (this) {
//			gameTick();
//		}
//	}
//
//	protected synchronized int updateBoardWithNewCurrent(int pos) {
//		int rowsRemoved = 0;
//		Block current = board.getCurrent(pos);
//		if (current != null) {
//			board.addStaticBlock(current);
//			rowsRemoved = checkRemoveRows(current);
//		}
//		if (!board.setCurrent(pos, board.generateRandomBlock())) {
//			stopGame();
//		}
//		return rowsRemoved;
//	}
//
//	protected synchronized int checkRemoveRows(Block current) {
//		int rowsRemoved = 0;
//		Set<Integer> possibleRowsToRemove = current.getYVals();
//		for (int row : possibleRowsToRemove) {
//			if (board.hasRow(row)) {
//				board.removeRow(row);
//				rowsRemoved++;
//			}
//		}
//		return rowsRemoved;
//	}
//
//	protected static String translateState(State state) {
//		switch (state) {
//		case NOT_STARTED:
//			return Constants.NOT_STARTED;
//		case STARTED:
//			return Constants.STARTED;
//		case PAUSED:
//			return Constants.PAUSED;
//		case ENDED:
//			return Constants.ENDED;
//		default:
//			throw new IllegalArgumentException("Not supported type");
//		}
//	}
//
//	@Override
//	public Block getNext() {
//		return board.getNext();
//	}
//
//	@Override
//	public Block getGhost() {
//		return board.getGhost(0);
//	}
//
//	@Override
//	public Block getCurrent() {
//		return board.getCurrent(0);
//	}
//
//	@Override
//	public Block getOther() {
//		return null; // Not used
//	}
//
//	@Override
//	public List<Block> getStaticBlocks() {
//		return board.getStaticBlocks();
//	}
//
//	@Override
//	public int getWidth() {
//		return board.getCols();
//	}
//
//	@Override
//	public int getHeight() {
//		return board.getRows();
//	}
//
//	public void keyPress(int key) {
//		switch (key) {
//		case KeyEvent.VK_Q:
//			System.exit(0);
//			break;
//		case KeyEvent.VK_LEFT:
//			left();
//			break;
//		case KeyEvent.VK_RIGHT:
//			right();
//			break;
//		case KeyEvent.VK_DOWN:
//			down();
//			break;
//		case KeyEvent.VK_UP:
//			rotate();
//			break;
//		case KeyEvent.VK_SPACE:
//			forceDown();
//			break;
//		case KeyEvent.VK_P:
//			togglePauseGame();
//			break;
//		case KeyEvent.VK_U:
//			toggleUpSideDownMode();
//			break;
//		case KeyEvent.VK_G:
//			toggleGhostMode();
//			break;
//		}
//
//	}
//
//	@Override
//	public int getLevel() {
//		return level;
//	}
//
//	@Override
//	public int getLines() {
//		return lines;
//	}
//
//	@Override
//	public BlockType getNextBlock() {
//		return board.getNext().getType();
//	}
//
//	@Override
//	public List<String> getPlayers() {
//		return new ArrayList<String>(); // TODO: fix
//	}
//
//	@Override
//	public String getGameState() {
//		switch (gameState) {
//		case NOT_STARTED:
//			return Constants.NOT_STARTED;
//		case STARTED:
//			return Constants.STARTED;
//		case PAUSED:
//			return Constants.PAUSED;
//		case ENDED:
//			return Constants.ENDED;
//		default:
//			return gameState.name();
//		}
//	}
//
//	@Override
//	public int getScore() {
//		return score;
//	}
//
//	@Override
//	public synchronized void toggleUpSideDownMode() {
//		upSideDownMode = !upSideDownMode;
//	}
//
//	@Override
//	public synchronized void toggleGhostMode() {
//		ghostMode = !ghostMode;
//	}
//
//	@Override
//	public synchronized boolean ghostEnabled() {
//		return ghostMode;
//	}
//
//	@Override
//	public synchronized boolean upSideDownEnabled() {
//		return upSideDownMode;
//	}
//
//	@Override
//	public boolean getIsHighScore() {
//		return isHighScore;
//	}
//
//}
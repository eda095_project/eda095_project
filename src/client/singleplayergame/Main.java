//package client.singleplayergame;
//
//import java.io.IOException;
//
//import javax.swing.JFrame;
//
//import client.controller.TetrisController;
//import client.gui.game.GamePanel;
//import client.gui.game.background.ColoredBackground;
//import client.gui.game.board.SimpleTetrisBoard;
//import client.gui.game.board.TetrisBoard;
//import client.gui.game.status.InfoStatusPanel;
//import client.gui.game.status.LevelStatusPanel;
//import client.gui.game.status.LinesStatusPanel;
//import client.gui.game.status.NextBlockStatusPanel;
//import client.gui.game.status.PlayerStatusPanel;
//import client.gui.game.status.ScoreStatusPanel;
//import client.model.Data;
//
//public class Main {
//	private static final int STATUS_AREA_WIDTH = 200;
//	private static final int NEXT_AREA_HEIGHT = 150;
//	private static final int INFO_AREA_HEIGHT = 150;
//	
//	public static void main(String[] args) throws IOException {
//		
//		JFrame jf = new JFrame();
//		
//		Data d = new Data();
//		GameModelOffline game = new GameModelOffline(d);
//		TetrisBoard tb = new SimpleTetrisBoard(game);
//
//		TetrisController tc = new TetrisController(game);
//		tb.addKeyListener(tc);
//		GamePanel gp = new GamePanel(game, new ColoredBackground(39, 35, 35), tb, tc);
//		gp.addStatusPanel(new NextBlockStatusPanel(game, STATUS_AREA_WIDTH, NEXT_AREA_HEIGHT));
//		gp.addStatusPanel(new ScoreStatusPanel(game, STATUS_AREA_WIDTH, 30));
//		gp.addStatusPanel(new LevelStatusPanel(game, STATUS_AREA_WIDTH, 30));
//		gp.addStatusPanel(new LinesStatusPanel(game, STATUS_AREA_WIDTH, 30));
//		gp.addStatusPanel(new InfoStatusPanel(game, STATUS_AREA_WIDTH, INFO_AREA_HEIGHT));
//		gp.addStatusPanel(new PlayerStatusPanel(game, STATUS_AREA_WIDTH, INFO_AREA_HEIGHT));
//		jf.add(gp);
//		jf.pack();
//		jf.setVisible(true);
//		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		game.startGame();
//		
//		
//	}
//
//}

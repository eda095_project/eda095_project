package shared.chat;

import org.json.JSONObject;

import shared.Constants;
import shared.Functions;

public class Message {

	protected String from;
	protected String message;
	protected long timestamp;

	protected Message(String from, String message, long timestamp) {
		this.from = from;
		this.message = message;
		this.timestamp = timestamp;
	}

	public Message(JSONObject obj) {
		this.from = obj.getString(Constants.CHAT_FROM);
		this.message = obj.getString(Constants.CHAT_MESSAGE);
		this.timestamp = obj.getLong(Constants.CHAT_TIMESTAMP);
	}

	public String getFrom() {
		return from;
	}

	public String getMessage() {
		return message;
	}

	public String getTime() {
		return Functions.getTime(timestamp);
	}

}

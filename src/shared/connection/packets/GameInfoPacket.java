package shared.connection.packets;

import org.json.JSONObject;

import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;
import connection.packets.Packet;
import connection.packets.operands.JSONPacket;
import connection.packets.operands.NullPacket;
import connection.packets.operands.OperatorPacket;

public class GameInfoPacket extends OperatorPacket {

	private ServerTetrisModel model;

	public GameInfoPacket() {
	}

	public GameInfoPacket(byte[] data, Packet[] packets, ServerTetrisModel model) {
		this.model = model;
	}

	@Override
	public OperatorPacket perform() {
		JSONObject data = model.gameInfo();
		if (data == null)
			return new NullPacket();
		return new JSONPacket(data);
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[0];
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.GAME_INFO_PACKET;
	}

}

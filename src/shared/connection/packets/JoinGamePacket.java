package shared.connection.packets;

import connection.packets.Packet;
import connection.packets.data.BooleanPacket;
import connection.packets.data.StringPacket;
import connection.packets.operands.OperatorPacket;
import connection.packets.operands.ResponsePacket;
import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;

public class JoinGamePacket extends OperatorPacket {

	private ServerTetrisModel model;
	private StringPacket sp;

	public JoinGamePacket(String s) {
		sp = new StringPacket(s);
	}

	public JoinGamePacket(byte[] data, Packet[] packets, ServerTetrisModel model) {
		this.model = model;
		sp = (StringPacket) packets[0];
	}

	@Override
	public OperatorPacket perform() {
		boolean b = model.joinGame(sp.toString());
		return new ResponsePacket(new BooleanPacket(b));
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { sp };
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.JOIN_GAME_PACKET;
	}

}

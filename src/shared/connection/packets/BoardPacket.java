package shared.connection.packets;

import org.json.JSONObject;

import shared.connection.factories.TetrisFactory;
import shared.connection.models.ServerGameModel;
import connection.packets.Packet;
import connection.packets.data.BooleanPacket;
import connection.packets.operands.JSONPacket;
import connection.packets.operands.OperatorPacket;

public class BoardPacket extends OperatorPacket {

	private ServerGameModel model;
	private BooleanPacket forceEverything;

	public BoardPacket(boolean forceEverything) {
		this.forceEverything = new BooleanPacket(forceEverything);
	}

	public BoardPacket(byte[] data, Packet[] packets, ServerGameModel model) {
		this.model = model;
		forceEverything = (BooleanPacket) packets[0];
	}

	@Override
	public OperatorPacket perform() {
		JSONObject obj;
		if (forceEverything.toBoolean()) {
			obj = model.getEntireBoard();
		} else {
			obj = model.getBoard();
		}
		return new JSONPacket(obj);
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { forceEverything };
	}

	@Override
	protected byte getType() {
		return TetrisFactory.BOARD_PACKET;
	}

	@Override
	public String toString() {
		return "Board request";
	}

}

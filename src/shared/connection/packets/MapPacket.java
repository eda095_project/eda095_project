package shared.connection.packets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import connection.packets.Packet;
import connection.packets.data.ArrayPacket;
import connection.packets.data.DataPacket;
import connection.packets.data.StringPacket;
import shared.connection.factories.ServerTetrisFactory;

public class MapPacket<E> extends DataPacket {

	private ArrayPacket keys;
	private ArrayPacket values;

	@SuppressWarnings("unchecked")
	public MapPacket(Map<String, E> map) {
		keys = new ArrayPacket();
		values = new ArrayPacket();

		for (String s : map.keySet()) {
			E val = map.get(s);
			keys.addPacket(new StringPacket(s));

			if (val instanceof String) {
				values.addPacket(new StringPacket((String) val));
			} else if (val instanceof List) {
				ArrayPacket ap = new ArrayPacket();
				for (Object o : (List<Object>) val) {
					ap.addPacket(new StringPacket(o.toString()));
				}
				values.addPacket(ap);
			} else {
				throw new RuntimeException("Unsuported class for MapPacket (" + val.getClass().getSimpleName() + ")");
			}

		}
	}

	public MapPacket(byte[] data, Packet[] packets) {
		keys = (ArrayPacket) packets[0];
		values = (ArrayPacket) packets[1];

	}

	@Override
	protected byte[] getLoad() {
		return new byte[0];
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { keys, values };
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.MAP_PACKET;
	}

	@SuppressWarnings("unchecked")
	public Map<String, E> getMap() {
		HashMap<String, E> map = new HashMap<>();
		for (int i = 0; i < keys.size(); i++) {
			String key = ((StringPacket) keys.get(i)).toString();

			Packet p = values.get(i);
			E val = null;
			if (p instanceof StringPacket) {
				String s = ((StringPacket) p).toString();
				val = (E) s;
			} else if (p instanceof ArrayPacket) {
				ArrayList<String> list = new ArrayList<>();
				for (Packet tempP : (ArrayPacket) p) {
					list.add(tempP.toString());
				}
				val = (E) list;
			} else {
				throw new RuntimeException("Unsuported class for MapPacket (" + p.getClass().getSimpleName() + ")");
			}
			map.put(key, val);
		}
		return map;
	}

	@Override
	public String toString() {
		return getMap().toString();
	}

}

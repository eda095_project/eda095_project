package shared.connection.packets;

import connection.packets.Packet;
import connection.packets.data.StringPacket;
import connection.packets.operands.NullPacket;
import connection.packets.operands.OperatorPacket;
import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;

public class ChatPacket extends OperatorPacket {

	private StringPacket sp;
	private ServerTetrisModel model;

	public ChatPacket(String message) {
		sp = new StringPacket(message);
	}

	public ChatPacket(byte[] data, Packet[] packets, ServerTetrisModel model) {
		sp = (StringPacket) packets[0];
		this.model = model;
	}

	@Override
	public OperatorPacket perform() {
		model.chat(sp.toString());
		return new NullPacket();
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { sp };
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.CHAT_PACKET;
	}

}

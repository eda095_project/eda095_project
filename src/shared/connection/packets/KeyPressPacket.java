package shared.connection.packets;

import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerGameModel;
import connection.packets.Packet;
import connection.packets.data.IntPacket;
import connection.packets.operands.NullPacket;
import connection.packets.operands.OperatorPacket;

public class KeyPressPacket extends OperatorPacket {

	private IntPacket key;
	private ServerGameModel model;

	public KeyPressPacket(int i) {
		key = new IntPacket(i);
	}

	public KeyPressPacket(byte[] data, Packet[] packets, ServerGameModel model) {
		key = (IntPacket) packets[0];
		this.model = model;
	}

	@Override
	public OperatorPacket perform() {
		model.keyPress(key.toInt());
		return new NullPacket();
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { key };
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.KEY_PRESS_PACKET;
	}

	@Override
	public String toString() {
		return "Key press (" + key.toInt() + ")";
	}

}

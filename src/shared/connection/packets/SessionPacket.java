package shared.connection.packets;

import org.json.JSONObject;

import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;
import connection.packets.Packet;
import connection.packets.data.StringPacket;
import connection.packets.operands.JSONPacket;
import connection.packets.operands.OperatorPacket;

public class SessionPacket extends OperatorPacket {

	private ServerTetrisModel model;
	private StringPacket sp;

	public SessionPacket(String session) {
		sp = new StringPacket(session);
	}

	public SessionPacket(byte[] data, Packet[] packets, ServerTetrisModel model) {
		this.model = model;
		sp = (StringPacket) packets[0];
	}

	@Override
	public OperatorPacket perform() {
		JSONObject obj = model.sessionData(sp.toString());
		return new JSONPacket(obj);
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { sp };
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.SESSION_PACKET;
	}

}

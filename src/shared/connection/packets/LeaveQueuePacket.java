package shared.connection.packets;

import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;
import connection.packets.Packet;
import connection.packets.operands.NullPacket;
import connection.packets.operands.OperatorPacket;

public class LeaveQueuePacket extends OperatorPacket {

	private ServerTetrisModel model;

	public LeaveQueuePacket() {

	}

	public LeaveQueuePacket(byte[] data, Packet[] packets, ServerTetrisModel model) {
		this.model = model;
	}

	@Override
	public OperatorPacket perform() {
		model.leaveQueue();
		return new NullPacket();
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[0];
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.LEAVE_QUEUE_PACKET;
	}

}

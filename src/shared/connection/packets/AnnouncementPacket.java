package shared.connection.packets;

import shared.connection.factories.TetrisFactory;
import shared.connection.models.ClientTetrisModel;
import connection.packets.Packet;
import connection.packets.data.StringPacket;
import connection.packets.operands.NullPacket;
import connection.packets.operands.OperatorPacket;

public class AnnouncementPacket extends OperatorPacket {

	private ClientTetrisModel model;
	private StringPacket announcement;

	public AnnouncementPacket(String announcement) {
		this.announcement = new StringPacket(announcement);
	}

	public AnnouncementPacket(byte[] data, Packet[] packets,
			ClientTetrisModel model) {
		this.model = model;
		announcement = (StringPacket) packets[0];
	}

	@Override
	public OperatorPacket perform() {
		model.announce(announcement.toString());
		return new NullPacket();
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { announcement };
	}

	@Override
	protected byte getType() {
		return TetrisFactory.ANNOUNCEMENT_PACKET;
	}

}

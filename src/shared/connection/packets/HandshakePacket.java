package shared.connection.packets;

import java.util.Map;

import org.json.JSONObject;

import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;
import connection.packets.Packet;
import connection.packets.operands.JSONPacket;
import connection.packets.operands.OperatorPacket;

public class HandshakePacket extends OperatorPacket {

	private MapPacket<String> mp;
	private ServerTetrisModel model;

	public HandshakePacket(Map<String, String> map) {
		mp = new MapPacket<String>(map);
	}

	@SuppressWarnings("unchecked")
	public HandshakePacket(Packet[] packets, ServerTetrisModel model) {
		mp = (MapPacket<String>) packets[0];
		this.model = model;
	}

	@Override
	public OperatorPacket perform() {
		Map<String, String> map = mp.getMap();
		JSONObject o = model.handshake(map);
		return new JSONPacket(o);
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { mp };
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.HANDSHAKE_PACKET;
	}

	@Override
	public String toString() {
		return "Handshake " + mp;
	}

}

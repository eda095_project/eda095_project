package shared.connection.packets;

import org.json.JSONObject;

import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;
import connection.packets.Packet;
import connection.packets.operands.JSONPacket;
import connection.packets.operands.OperatorPacket;

public class StatusPacket extends OperatorPacket {

	private ServerTetrisModel model;

	public StatusPacket() {
	}

	public StatusPacket(byte[] data, Packet[] packets, ServerTetrisModel model) {
		this.model = model;
	}

	@Override
	public OperatorPacket perform() {
		JSONObject data = model.getStatus();
		return new JSONPacket(data);
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[0];
	}

	@Override
	protected byte getType() {
		return ServerTetrisFactory.STATUS_PACKET;
	}

	@Override
	public String toString() {
		return "Status request";
	}

}

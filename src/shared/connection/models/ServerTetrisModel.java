package shared.connection.models;

import java.util.Map;

import org.json.JSONObject;

public interface ServerTetrisModel {

	public boolean joinGame(String type);

	public JSONObject gameInfo();

	public JSONObject getStatus();

	public JSONObject handshake(Map<String, String> map);

	public JSONObject sessionData(String session);

	public void leaveQueue();

	public void chat(String message);
}

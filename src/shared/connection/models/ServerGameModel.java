package shared.connection.models;

import org.json.JSONObject;

public interface ServerGameModel {

	public void keyPress(int key);

	public JSONObject getBoard();

	public JSONObject getEntireBoard();

}

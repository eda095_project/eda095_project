package shared.connection.factories;

import shared.connection.models.ServerGameModel;
import shared.connection.models.ServerTetrisModel;
import shared.connection.packets.BoardPacket;
import shared.connection.packets.ChatPacket;
import shared.connection.packets.GameInfoPacket;
import shared.connection.packets.HandshakePacket;
import shared.connection.packets.JoinGamePacket;
import shared.connection.packets.KeyPressPacket;
import shared.connection.packets.LeaveQueuePacket;
import shared.connection.packets.SessionPacket;
import shared.connection.packets.StatusPacket;
import connection.packets.Packet;

public class ServerTetrisFactory extends TetrisFactory {

	private ServerTetrisModel model;
	private ServerGameModel game;

	public ServerTetrisFactory(ServerTetrisModel model) {
		this.model = model;
	}

	public void setGameModel(ServerGameModel model) {
		game = model;
	}

	@Override
	protected Packet create(byte type, byte[] data, Packet[] packets) {
		Packet p = null;
		switch (type) {
		case HANDSHAKE_PACKET:
			p = new HandshakePacket(packets, model);
			break;
		case STATUS_PACKET:
			p = new StatusPacket(data, packets, model);
			break;
		case JOIN_GAME_PACKET:
			p = new JoinGamePacket(data, packets, model);
			break;
		case GAME_INFO_PACKET:
			p = new GameInfoPacket(data, packets, model);
			break;
		case SESSION_PACKET:
			p = new SessionPacket(data, packets, model);
			break;
		case LEAVE_QUEUE_PACKET:
			p = new LeaveQueuePacket(data, packets, model);
			break;
		case CHAT_PACKET:
			p = new ChatPacket(data, packets, model);
		}

		if (game != null) {
			switch (type) {
			case KEY_PRESS_PACKET:
				p = new KeyPressPacket(data, packets, game);
				break;
			case BOARD_PACKET:
				p = new BoardPacket(data, packets, game);
				break;
			}
		}
		return p;
	}
}

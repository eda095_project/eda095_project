package shared.connection.factories;

import shared.connection.models.ClientTetrisModel;
import shared.connection.packets.AnnouncementPacket;
import connection.packets.Packet;

public class ClientTetrisFactory extends TetrisFactory {

	private ClientTetrisModel model;

	public ClientTetrisFactory(ClientTetrisModel model) {
		this.model = model;
	}

	@Override
	protected Packet create(byte type, byte[] data, Packet[] packets) {
		Packet p = null;
		switch (type) {
		case ANNOUNCEMENT_PACKET:
			p = new AnnouncementPacket(data,packets,model);
			break;
		}
		return p;
	}

}

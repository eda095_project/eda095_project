package shared.connection.factories;

import shared.connection.packets.MapPacket;
import connection.packets.Packet;
import connection.packets.factory.PacketFactory;

public abstract class TetrisFactory implements PacketFactory {

	public static final byte MAP_PACKET = 71;
	public static final byte SETTINGS_PACKET = 72;
	public static final byte KEY_PRESS_PACKET = 74;
	public static final byte BOARD_PACKET = 75;
	public static final byte STATUS_PACKET = 76;
	public static final byte HANDSHAKE_PACKET = 77;
	public static final byte JOIN_GAME_PACKET = 78;
	public static final byte GAME_INFO_PACKET = 79;
	public static final byte SESSION_PACKET = 80;
	public static final byte LEAVE_QUEUE_PACKET = 81;
	public static final byte CHAT_PACKET = 82;
	public static final byte ANNOUNCEMENT_PACKET = 83;

	@SuppressWarnings("rawtypes")
	@Override
	public Packet createPacket(byte type, byte[] data, Packet[] packets) {
		Packet p = null;
		switch (type) {
		case MAP_PACKET:
			p = new MapPacket(data, packets);
			break;
		default:
			p = create(type, data, packets);
		}
		return p;
	}

	protected abstract Packet create(byte type, byte[] data, Packet[] packets);

}

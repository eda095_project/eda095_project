package shared;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Functions {
	public static String getTime() {
		return new SimpleDateFormat("HH:mm:ss").format(new Date());
	}

	public static String getTime(long now) {
		return new SimpleDateFormat("HH:mm:ss").format(new Date(now));
	}

	public static String milliesToString(long time) {
		long millies = time;
		if (millies < 1000) {
			return time + "ms";
		}
		long sec = millies / 1000;
		millies %= 1000;
		if (millies > 500)
			sec++;
		if (sec < 60) {
			return sec + "s";
		}
		long min = sec / 60;
		sec %= 60;
		if (sec > 30)
			min++;

		return min + "m";
	}
}

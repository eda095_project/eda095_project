package shared.data;

import java.util.LinkedList;

public class BlockingQueue<E> {

	private LinkedList<E> list;

	public BlockingQueue() {
		list = new LinkedList<>();
	}

	public synchronized boolean offer(E e) {
		boolean b = list.add(e);
		notifyAll();
		return b;
	}

	public synchronized E poll() {
		while (list.isEmpty())
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return list.poll();
	}

	public synchronized E peek() {
		while (list.isEmpty())
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return list.peek();
	}

	@Override
	public String toString() {
		return list.toString();
	}
}

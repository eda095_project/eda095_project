package shared;

public class Constants {

	private Constants() {

	}

	/* Board size constants */
	public static final String ROWS = "rows";
	public static final String COLS = "cols";

	/* Export board constants */
	public static final String CURRENT = "current";
	public static final String NEXT = "next";
	public static final String GHOST = "ghost";
	public static final String BOARD = "board";
	public static final String OTHER = "other";
	public static final String YOUR_CURRENT = "you";

	/* Board versions */
	public static final String BOARD_VERSION = "version";
	public static final String BOARD_VERSION_ALL = "all";

	/* Block exports */
	public static final String X = "x";
	public static final String Y = "y";
	public static final String PART = "part";
	public static final String BLOCKS = "parts";
	public static final String TYPE = "type";
	public static final String DIR = "direction";

	/* Game types */
	public static final String OFFLINE = "offline";
	public static final String SIMPLE = "simple";
	public static final String SIMPLE2 = "simple2";
	public static final String TWO_PLAYER = "two_player";
	public static final String THREE_PLAYER = "three_player";
	public static final String FOUR_PLAYER = "four_player";

	public static final String INCR = "incr";

	/* Global state */
	public static final String CONNECTION_STATE = "state";
	public static final int CONNECTION_STATE_CONNECTED = 0;
	public static final int CONNECTION_STATE_LOBBY = 1;
	public static final int CONNECTION_STATE_GAME = 2;
	public static final int CONNECTION_STATE_OFFLINE = 3;

	/* Status constants */
	public static final String STATUS_DATA = "data";
	public static final String STATUS_DATA_GAMEMODES = "gamemodes";
	public static final String STATUS_DATA_CHAT = "chat";
	public static final String STATUS_DATA_CHATMESSAGES = "messages";

	/* Game states */
	public static final String GAME_STATE = "Game state";
	public static final String NOT_STARTED = "Not started";
	public static final String STARTED = "Started";
	public static final String PAUSED = "Paused";
	public static final String LOCKED = "Locked";
	public static final String ENDED = "Ended";
	public static final String SCORE = "Score";
	public static final String LEVEL = "Level";
	public static final String LINES = "Lines";
	public static final String PLAYER_NAMES = "names";

	/* Game commands */
	public static final int COMMAND_LEFT = 0;
	public static final int COMMAND_RIGHT = 1;
	public static final int COMMAND_ROTATE = 2;
	public static final int COMMAND_DOWN = 3;
	public static final int COMMAND_FORCE_DOWN = 4;
	public static final int COMMAND_PAUSE = 5;

	/* FILE NAMES */
	public static final String DATA_FOLDER = ".tetrisAnarchy";
	public static final String DATA_FILE = "data.json";
	public static final String SERVER_DATA_FOLDER = "data";
	public static final String SERVER_DATA_FILE = "data.json";

	/* Player info */
	public static final String NAME = "name";
	public static final String ADDRESS = "addr";
	public static final String VERSION = "version";

	/* JSON */
	public static final String SESSION = "session";
	public static final String GAMEMODE = "gamemode";
	public static final String GAMEMODES = "gamemodes";
	public static final String VALID = "valid";

	/* Connection */
	public static final int SERVER_PORT = 30000;

	/* Server sessions */
	public static final long SESSION_TIMEOUT_TIME = 5 * 60 * 1000;

	/* Game info constants */
	public static final String GAMEINFO_STATUS = "status";
	public static final int GAMEINFO_NO_QUEUE = 0;
	public static final int GAMEINFO_IN_QUEUE = 1;
	public static final int GAMEINFO_IN_GAME = 2;

	/* Chat constants */
	public static final String CHAT_FROM = "from";
	public static final String CHAT_MESSAGE = "message";
	public static final String CHAT_TIMESTAMP = "timestamp";

}

package shared.game;

public enum Direction {

	LEFT, RIGHT, UP, DOWN;

	public int getX() {
		switch (this) {
		case LEFT:
			return -1;
		case RIGHT:
			return 1;
		default:
			return 0;
		}
	}

	public int getY() {
		switch (this) {
		case UP:
			return -1;
		case DOWN:
			return 1;
		default:
			return 0;
		}
	}

	public static Direction getNextDirection(Direction dir) {
		switch (dir) {
		case UP:
			return RIGHT;
		case RIGHT:
			return DOWN;
		case DOWN:
			return LEFT;
		case LEFT:
			return UP;
		default:
			return UP;
		}
	}
	
	public static double getRadDirection(Direction dir) {
		switch (dir) {
		case UP:
			return Math.PI/2;
		case RIGHT:
			return 0;
		case DOWN:
			return -Math.PI/2;
		case LEFT:
			return Math.PI;
		default:
			return Math.PI;
		}
	}
	
	public int toInt() {
		switch (this) {
		case UP:
			return 0;
		case RIGHT:
			return 1;
		case DOWN:
			return 2;
		case LEFT:
			return 3;
		default:
			return 3;
		}
	}
	
	public static Direction generate(int val) {
		switch (val) {
		case 0:
			return UP;
		case 1:
			return RIGHT;
		case 2:
			return DOWN;
		case 3:
			return LEFT;
		default:
			return LEFT;
		}
	}
}

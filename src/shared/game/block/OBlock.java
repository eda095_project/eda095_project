package shared.game.block;

public class OBlock extends Block {

	public OBlock(int x, int y) {
		super(BlockType.O, x, y);

		add(x, y);
		add(x + 1, y);
		add(x, y + 1);
		add(x + 1, y + 1);
	}

	@Override
	public void rotate() {
	}
	
	public void setMetadata(Block b) {
	}

}

package shared.game.block.generator;

import java.util.ArrayList;
import java.util.List;

import shared.game.block.BlockType;

public class StandardGenerator extends BlockGenerator {

	private List<BlockType> blocks;
	private BlockType next;

	public StandardGenerator() {
		blocks = new ArrayList<>();
		blocks.add(BlockType.I);
		blocks.add(BlockType.J);
		blocks.add(BlockType.O);
		blocks.add(BlockType.S);
		blocks.add(BlockType.Z);
		blocks.add(BlockType.L);
		blocks.add(BlockType.T);
		next = generate();
	}

	@Override
	public BlockType poll() {
		BlockType b = next;
		next = generate();
		return b;
	}

	@Override
	public BlockType next() {
		return next;
	}

	private BlockType generate() {
		int i = r.nextInt(blocks.size());
		return blocks.get(i);
	}
}

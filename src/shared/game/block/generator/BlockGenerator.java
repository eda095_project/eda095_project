package shared.game.block.generator;

import java.util.Random;

import shared.game.block.BlockType;

public abstract class BlockGenerator {

	protected Random r = new Random();

	public abstract BlockType poll();

	public abstract BlockType next();
}

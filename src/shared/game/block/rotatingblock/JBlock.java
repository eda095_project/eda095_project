package shared.game.block.rotatingblock;

import shared.game.block.BlockType;

public class JBlock extends RotatingBlock {

	public JBlock(int x, int y) {
		super(BlockType.J, x, y);
		add(x, y - 1);
		add(x, y);
		add(x, y + 1);
		add(x - 1, y + 1);
	}

}

package shared.game.block.rotatingblock;

import shared.game.Direction;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.block.SimpleBlock;

public class RotatingBlock extends Block {
	protected int[][] R = { { 0, -1 }, { 1, 0 } };
	protected int modifier = 1;

	protected RotatingBlock(BlockType type, int coreX, int coreY) {
		super(type, coreX, coreY);
	}

	@Override
	public void rotate() {
		for (SimpleBlock sb : blocks) {
			int dx = sb.getX() - x;
			int dy = sb.getY() - y;
			int rx = modifier * (R[0][0] * dx + R[0][1] * dy);
			int ry = modifier * (R[1][0] * dx + R[1][1] * dy);
			int mx = rx - dx;
			int my = ry - dy;
			sb.move(mx, my);
		}
		updateDirection();
	}
	
	protected void updateDirection() {
		dir = Direction.getNextDirection(dir);
	}
}

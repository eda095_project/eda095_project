package shared.game.block.rotatingblock;

import shared.game.block.BlockType;

public class LBlock extends RotatingBlock {

	public LBlock(int x, int y) {
		super(BlockType.L, x, y);
		add(x, y - 1);
		add(x, y);
		add(x, y + 1);
		add(x + 1, y + 1);
	}

}

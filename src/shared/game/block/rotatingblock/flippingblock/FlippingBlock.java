package shared.game.block.rotatingblock.flippingblock;

import shared.game.Direction;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.block.rotatingblock.RotatingBlock;

public class FlippingBlock extends RotatingBlock {

	protected FlippingBlock(BlockType type, int coreX, int coreY) {
		super(type, coreX, coreY);
	}

	@Override
	public void setMetadata(Block b) {
		super.setMetadata(b);
		FlippingBlock fb = (FlippingBlock) b;
		modifier = fb.modifier;
	}

	@Override
	public void rotate() {
		super.rotate();
		modifier = -modifier;
	}
	
	protected void updateDirection() {
		if (modifier == 1) {
			dir = Direction.UP;
		} else {
			dir = Direction.RIGHT;
		}
	}
}

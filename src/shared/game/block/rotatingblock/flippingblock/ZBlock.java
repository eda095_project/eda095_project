package shared.game.block.rotatingblock.flippingblock;

import shared.game.block.BlockType;

public class ZBlock extends FlippingBlock {

	public ZBlock(int x, int y) {
		super(BlockType.Z, x, y);
		add(x - 1, y);
		add(x, y);
		add(x, y + 1);
		add(x + 1, y + 1);
	}

}

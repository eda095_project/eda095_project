package shared.game.block.rotatingblock.flippingblock;

import shared.game.block.BlockType;

public class SBlock extends FlippingBlock {

	public SBlock(int x, int y) {
		super(BlockType.S, x, y);
		add(x + 1, y);
		add(x, y);
		add(x, y + 1);
		add(x - 1, y + 1);
	}
}

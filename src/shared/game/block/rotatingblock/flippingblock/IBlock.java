package shared.game.block.rotatingblock.flippingblock;

import shared.game.block.BlockType;

public class IBlock extends FlippingBlock {

	public IBlock(int x, int y) {
		super(BlockType.I, x, y);
		add(x, y - 1);
		add(x, y);
		add(x, y + 1);
		add(x, y + 2);
	}
}

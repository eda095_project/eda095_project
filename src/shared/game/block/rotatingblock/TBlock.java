package shared.game.block.rotatingblock;

import shared.game.block.BlockType;

public class TBlock extends RotatingBlock {

	public TBlock(int x, int y) {
		super(BlockType.T, x, y);
		add(x - 1, y);
		add(x, y);
		add(x + 1, y);
		add(x, y + 1);
	}

}

package shared.game.block;

public class SimpleBlock {
	private int x;
	private int y;
	private int part;

	SimpleBlock(int x, int y, int part) {
		this.x = x;
		this.y = y;
		this.part = part;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getPart() {
		return part;
	}

	public void move(int dx, int dy) {
		x += dx;
		y += dy;
	}

	public String toString() {
		return x + "," + y;
	}
}
package shared.game.block;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;
import shared.game.Direction;

public abstract class Block implements Iterable<SimpleBlock> {

	protected BlockType type;
	protected ArrayList<SimpleBlock> blocks;
	protected int x, y;
	private String id;
	protected Direction dir;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	protected Block(BlockType type, int coreX, int coreY) {
		this.x = coreX;
		this.y = coreY;
		this.type = type;
		blocks = new ArrayList<>();
		dir = Direction.UP;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	/**
	 * Makes a copy of this block at the same location and containing the same
	 * SimpleBlocks as this Block currently has
	 * 
	 * @return - the copy
	 */
	public Block copy() {
		Block copy = generate(type, x, y);
		copy.blocks.clear();
		for (SimpleBlock sb : blocks) {
			copy.add(sb.getX(), sb.getY());
		}
		copy.setMetadata(this);
		copy.setId(id);
		return copy;
	}

	protected void setMetadata(Block b) {
		dir = b.dir;
	}

	/**
	 * Adds a SimpleBlock to the given coordinates
	 * 
	 * @param x
	 *            - the x coordinate
	 * @param y
	 *            - the y coordinate
	 * 
	 */
	protected void add(int x, int y) {
		blocks.add(new SimpleBlock(x, y, blocks.size()));
	}

	/**
	 * Returns the BlockType of this Block
	 * 
	 * @return the BlockType
	 */
	public BlockType getType() {
		return type;
	}

	@Override
	public Iterator<SimpleBlock> iterator() {
		return blocks.iterator();
	}

	/**
	 * Moves this block (dx, dy)
	 * 
	 * @param dx
	 *            - the distance to move x-wise
	 * @param dy
	 *            - the distance to move y-wise
	 */
	private void move(int dx, int dy) {
		x += dx;
		y += dy;
		for (SimpleBlock b : blocks) {
			b.move(dx, dy);
		}
	}

	public void move(Direction dir) {
		move(dir.getX(), dir.getY());
	}

	/**
	 * Place this block at coordinates (x, y)
	 * 
	 * @param x
	 * @param y
	 */
	public void place(int x, int y) {
		int dx = x - this.x;
		int dy = y - this.y;
		move(dx, dy);
	}

	/**
	 * Rotates this block if applicable
	 */
	public abstract void rotate();
	
	public Direction getDirection() {
		return dir;
	}

	/**
	 * Removes all SimpleBlocks in this Block that has any of the given y values
	 * Collapses the block according to tetris rules: SimpleBlocks above the y
	 * value are moved down. SimpleBlocks below the y value are not moved
	 * 
	 * @param rows
	 *            - the y values to use
	 */
	public void removeYs(List<Integer> rows) {
		ArrayList<SimpleBlock> toRemove = new ArrayList<>();
		for (int row : rows) {
			for (SimpleBlock b : blocks) {
				if (b.getY() == row)
					toRemove.add(b);
				else if (b.getY() < row)
					b.move(0, 1);
			}
		}
		blocks.removeAll(toRemove);
	}

	/**
	 * Checks if this Block contains any SimpleBlocks
	 * 
	 * @return true if there are no SimpleBlocks in this Block
	 */
	public boolean isEmpty() {
		return blocks.isEmpty();
	}

	/**
	 * Checks if this block overlaps another Block
	 * 
	 * @param other
	 *            - the Block to check against
	 * @return true if the blocks overlap
	 */
	public boolean overlapsBlock(Block other) {
		for (SimpleBlock otherSimpleBlock : other.blocks) {
			for (SimpleBlock mySimpleBlock : blocks) {
				if (otherSimpleBlock.getX() == mySimpleBlock.getX()
						&& otherSimpleBlock.getY() == mySimpleBlock.getY()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Checks if this Block overlaps a given x value
	 * 
	 * @param x
	 * @return true if there is overlap
	 */
	public boolean overlapsX(int x) {
		for (SimpleBlock sb : blocks) {
			if (sb.getX() == x)
				return true;
		}
		return false;
	}

	/**
	 * Checks if this Block overlaps a given y value
	 * 
	 * @param y
	 * @return true if there is overlap
	 */
	public boolean overlapsY(int y) {
		for (SimpleBlock sb : blocks) {
			if (sb.getY() == y)
				return true;
		}
		return false;
	}

	/**
	 * Returns a set of all the y values that exist in this Block
	 * 
	 * @return the set of y values
	 */
	public Set<Integer> getYVals() {
		Set<Integer> yVals = new HashSet<>();
		for (SimpleBlock sb : blocks) {
			yVals.add(sb.getY());
		}
		return yVals;
	}

	/**
	 * Returns a set of the x values that exist in this Block for a given y
	 * value
	 * 
	 * @param y
	 *            - the y value to check for
	 * @return a Set with x values
	 */
	public Set<Integer> getXValsForY(int y) {
		Set<Integer> xVals = new HashSet<>();
		for (SimpleBlock sb : blocks) {
			if (sb.getY() == y) {
				xVals.add(sb.getX());
			}
		}
		return xVals;
	}

	/**
	 * Checks if the given SimpleBlock has a SimpleBlock in the given direction
	 * in this block. Warning: uses equality by part. If used with a different
	 * Block than the one containing the SimpleBlock it will likely produce
	 * false results.
	 * 
	 * @param sb
	 * @param dir
	 * @return true if a SimpleBlock exists in the given direction
	 */
	public boolean hasPartInDirection(SimpleBlock sb, Direction dir) {
		for (SimpleBlock sbb : this) {
			if (sbb.getPart() != sb.getPart()) {
				switch (dir) {
				case LEFT:
					if (sbb.getX() < sb.getX() && sbb.getY() == sb.getY()) {
						return true;
					}
					break;
				case UP:
					if (sbb.getX() == sb.getX() && sbb.getY() < sb.getY()) {
						return true;
					}
					break;
				case RIGHT:
					if (sbb.getX() > sb.getX() && sbb.getY() == sb.getY()) {
						return true;
					}
					break;
				case DOWN:
					if (sbb.getX() == sb.getX() && sbb.getY() > sb.getY()) {
						return true;
					}
					break;
				default:
					break;
				}
			}
		}
		return false;
	}

	/**
	 * Equals uses the id of the block to compare to other blocks. If you want a
	 * block to be equal to another block, set the same id
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block other = (Block) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public synchronized JSONObject export() {
		JSONObject obj = new JSONObject();
		obj.put(Constants.TYPE, type.toInt());
		obj.put(Constants.DIR, dir.toInt());
		JSONArray jsonBlocks = new JSONArray();
		for (SimpleBlock sb : blocks) {
			JSONObject temp = new JSONObject();
			temp.put(Constants.X, sb.getX());
			temp.put(Constants.Y, sb.getY());
			temp.put(Constants.PART, sb.getPart());
			jsonBlocks.put(temp);
		}
		obj.put(Constants.BLOCKS, jsonBlocks);
		return obj;
	}

	public static Block generate(BlockType type, int x, int y) {
		return type.create(x, y);
	}

	public static Block generate(JSONObject json) {
		JSONArray jsonBlocks = json.getJSONArray(Constants.BLOCKS);
		BlockType type = BlockType.generate(json.getInt(Constants.TYPE));
		Direction dir = Direction.generate(json.getInt(Constants.DIR));

		Block b = type.create(0, 0);
		b.setDirection(dir);
		b.blocks.clear();
		for (Object o : jsonBlocks) {
			JSONObject jObj = (JSONObject) o;
			int x = jObj.getInt(Constants.X);
			int y = jObj.getInt(Constants.Y);
			int part = jObj.getInt(Constants.PART);
			b.blocks.add(new SimpleBlock(x, y, part));
		}
		return b;
	}

	private void setDirection(Direction dir) {
		this.dir = dir;
	}
}

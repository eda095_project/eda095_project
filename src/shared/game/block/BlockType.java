package shared.game.block;

import shared.game.block.rotatingblock.JBlock;
import shared.game.block.rotatingblock.LBlock;
import shared.game.block.rotatingblock.TBlock;
import shared.game.block.rotatingblock.flippingblock.IBlock;
import shared.game.block.rotatingblock.flippingblock.SBlock;
import shared.game.block.rotatingblock.flippingblock.ZBlock;

public enum BlockType {
	I, T, O, L, J, S, Z;

	public int toInt() {
		switch (this) {
		case I:
			return 0;
		case J:
			return 1;
		case L:
			return 2;
		case O:
			return 3;
		case S:
			return 4;
		case T:
			return 5;
		case Z:
			return 6;
		default:
			return 7;
		}
	}

	public static BlockType generate(int block) {
		switch (block) {
		case 0:
			return I;
		case 1:
			return J;
		case 2:
			return L;
		case 3:
			return O;
		case 4:
			return S;
		case 5:
			return T;
		case 6:
			return Z;

		}
		return null;
	}

	Block create(int x, int y) {
		Block b = null;
		switch (this) {
		case I:
			b = new IBlock(x, y);
			break;
		case J:
			b = new JBlock(x, y);
			break;
		case L:
			b = new LBlock(x, y);
			break;
		case O:
			b = new OBlock(x, y);
			break;
		case S:
			b = new SBlock(x, y);
			break;
		case T:
			b = new TBlock(x, y);
			break;
		case Z:
			b = new ZBlock(x, y);
			break;
		}
		return b;
	}
}

package shared.game.board;

import shared.game.block.Block;
import shared.game.block.generator.StandardGenerator;

public class StandardTetrisBoard extends TetrisBoard {

	public StandardTetrisBoard(int players) {
		this(DEFAULT_ROWS, DEFAULT_COLUMNS * Math.min(3, players), players);
	}

	public StandardTetrisBoard(int rows, int columns, int players) {
		super(rows, columns, new StandardGenerator(), players);
	}

	@Override
	public synchronized boolean setCurrent(Player player, Block b) {
		
		next.place(getCols() * (player.index + 1) / (players + 1), 0);
		return super.setCurrent(player, b);
	}

}

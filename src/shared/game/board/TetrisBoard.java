package shared.game.board;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;
import shared.game.Direction;
import shared.game.block.Block;
import shared.game.block.BlockType;
import shared.game.block.generator.BlockGenerator;

public abstract class TetrisBoard {
	public static final int DEFAULT_ROWS = 20;
	public static final int DEFAULT_COLUMNS = 10;

	protected List<Block> staticBlocks;
	protected Player[] playerList;

	protected Block next;
	private int rows;
	private int cols;
	protected BlockGenerator generator;
	protected int players;

	public TetrisBoard(BlockGenerator generator, int players) {
		this(DEFAULT_ROWS, DEFAULT_COLUMNS, generator, players);
	}

	public TetrisBoard(int rows, int columns, BlockGenerator generators,
			int players) {
		this.rows = rows;
		this.cols = columns;
		this.players = players;
		playerList = new Player[players];
		for (int i = 0; i < players; i++)
			playerList[i] = new Player(i);
		staticBlocks = new ArrayList<Block>();
		next = generateRandomBlock();
	}

	/**
	 * Adds the given block to the list
	 * 
	 * @param b
	 */
	public synchronized void addStaticBlock(Block b) {
		staticBlocks.add(b);
		setExportStaticBlocksToAllPlayers();
	}

	/**
	 * Force export of entire board to all players next tick
	 */
	private synchronized void setExportStaticBlocksToAllPlayers() {
		for (Player player : playerList) {
			player.setExport();
		}
	}

	/**
	 * Try to set current for player pos to next and next to a new block b. If
	 * it fails due to colliding to another current block, it sets current for
	 * that player to null instead, so as it can be set next tick. It returns
	 * true in both cases.
	 * 
	 * If it would collide to another block, it returns false.
	 * 
	 * @param player
	 *            - the player for which to update current
	 * @param b
	 *            - the block to set to next
	 * @return - true if set current worked, or failed because of collision with
	 *         other current block. False if collision with other blocks.
	 */
	public synchronized boolean setCurrent(Player player, Block b) {
		next.setId(Constants.CURRENT + player);
		player.current = next;
		if (crashWhenSummoned(player, next)) {
			return true; // uppdatera s� att den tickar och v�ntar p� att
							// det finns plats
		}
		if (!isBlockPositionLegal(player.current)) {
			return false;
		}

		next = b;
		updateGhost(player);
		return true;
	}

	/**
	 * Returns the current block
	 * 
	 * @return the current block, null if not set
	 */
	public synchronized Block getCurrent(Player player) {
		return player.current;
	}

	/**
	 * Returns the next block
	 * 
	 * @return the next block
	 */
	public synchronized Block getNext() {
		return next;
	}

	/**
	 * Returns the ghost block , that is the block at the position where the
	 * current block would be if it was moved down to the static blocks
	 * 
	 * @return the ghost Block
	 */
	public synchronized Block getGhost(Player player) {
		return player.ghost;
	}

	/**
	 * Updates the position of the ghost block, projecting it down upon the
	 * static blocks
	 */
	private synchronized void updateGhost(Player player) {
		if (player.current == null) {
			player.ghost = null;
			return;
		}
		Block ghost = player.ghost = player.current.copy();
		while (isBlockPositionLegal(ghost)) {
			ghost.move(Direction.DOWN);
		}
		ghost.move(Direction.UP);
	}

	/**
	 * Moves current block if not null and move would be legal
	 * 
	 * @param dx
	 * @param dy
	 */
	public synchronized boolean moveCurrentIfLegal(Player player, Direction dir) {
		if (player.current == null) {
			return false;
		}
		Block temp = player.current.copy();
		temp.move(dir);
		if (isBlockPositionLegal(temp)) {
			player.current.move(dir);
			updateGhost(player);
			return true;
		}
		return false;
	}

	/**
	 * Try to move all currents down simultaneously. Move blocks that have legal
	 * space to move to. Try to move the rest (blocks that were not moved last
	 * loop) again, and repeat until either all blocks are moved or nothing has
	 * changed since last loop. Return a boolean array representing the blocks
	 * that were moved as true, and the ones that were not moved as false
	 * 
	 * @return boolean[] - array of success/fail booleans, one for each current
	 *         block.
	 */
	public synchronized boolean[] moveCurrentsDownIfLegal() {
		boolean[] movedBlocks = new boolean[players];
		Block[] temps = new Block[players];
		for (int i = 0; i < players; i++) {
			temps[i] = playerList[i].current != null ? playerList[i].current
					.copy() : null;
		}
		Block[] interims = new Block[players];
		boolean changed = true;
		int nbrMoved = 0;
		while (changed && nbrMoved < players) {
			changed = false;
			for (int i = 0; i < players; i++) {
				Block b = temps[i];
				if (b != null && !movedBlocks[i]) {
					b.move(Direction.DOWN);
					if (isBlockPositionLegal(b)) {
						// Temp block was moved, update flags and move the
						// actual block
						changed = true;
						nbrMoved++;
						movedBlocks[i] = true;
						playerList[i].current.move(Direction.DOWN);
						updateGhost(playerList[i]);
					} else {
						// Temp block was not moved, reset temp block and try
						// again next loop
						b.move(Direction.UP);
						interims[i] = b;
					}
				}
			}
			temps = interims;
		}
		return movedBlocks;
	}

	/**
	 * Rotates current block if not null and action would be legal
	 */
	public synchronized void rotateCurrentIfLegal(Player player) {
		Block current = player.current;
		if (current == null) {
			return;
		}
		Block temp = current.copy();
		temp.rotate();
		if (isBlockPositionLegal(temp)) {
			current.rotate();
			updateGhost(player);
		}
	}

	/**
	 * Forces the current block down to the next solid obstacle if not null
	 * 
	 * @return the number of spaces moved
	 */
	public synchronized int forceDown(Player player) {
		int val = 0;
		while (moveCurrentIfLegal(player, Direction.DOWN)) {
			val++;
		}
		return val;
	}

	/**
	 * Does a check when a new current is summoned to see if it crashes into
	 * statics. If crashed into another current it should wait to summon.
	 */
	public synchronized boolean crashWhenSummoned(Player player, Block temp) {
		for (Player p : playerList) {
			Block c = p.current;
			if (c != null && !temp.equals(c) && temp.overlapsBlock(c)) {
				player.current = null;
				return true;
			}
		}
		return false;
	}

	/**
	 * Tests if a block's position is legal
	 * 
	 * @param temp
	 *            - the block to test
	 * @return true if legal
	 */
	public synchronized boolean isBlockPositionLegal(Block temp) {
		if (temp.overlapsX(cols))
			return false;
		if (temp.overlapsX(-1))
			return false;
		if (temp.overlapsY(rows))
			return false;
		for (Player player : playerList) {
			Block c = player.current;
			if (c != null && !temp.equals(c) && temp.overlapsBlock(c)) {
				return false;
			}
		}

		for (Block b : staticBlocks) {
			if (b.overlapsBlock(temp)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * We tell each block to remove its SimpleBlocks at the given rows, if they
	 * exist. We then remove any block from the list that no longer contains any
	 * SimpleBlocks.
	 * 
	 * @param row
	 *            - the row to remove
	 */
	public synchronized void removeRows(List<Integer> rows) {
		ListIterator<Block> itr = staticBlocks.listIterator();
		while (itr.hasNext()) {
			Block b = itr.next();
			b.removeYs(rows);
			if (b.isEmpty()) {
				itr.remove();
			}
		}
		setExportStaticBlocksToAllPlayers();
	}

	/**
	 * Checks if the row with the given index is complete
	 * 
	 * @param row
	 *            - the row to check
	 * @return
	 */
	public synchronized boolean hasRow(int row) {
		Set<Integer> containedXVals = new HashSet<Integer>();
		for (Block b : staticBlocks) {
			containedXVals.addAll(b.getXValsForY(row));
		}
		for (int i = 0; i < cols; i++) {
			if (!containedXVals.contains(i)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the number of rows of this board
	 * 
	 * @return the number of rows
	 */
	public synchronized int getRows() {
		return rows;
	}

	/**
	 * Returns the number of columns of this board
	 * 
	 * @return the number of columns
	 */
	public synchronized int getCols() {
		return cols;
	}

	/**
	 * Returns a map of lists of string represenation of SimpleBlocks for
	 * current, next, ghost and static blocks respectively. Only exports the
	 * static blocks if shouldExportStaticBlocks = true
	 * 
	 * @return a map of lists of simple blocks
	 */
	public synchronized JSONObject exportBoard(Player player) {
		JSONObject obj = new JSONObject();

		// Version
		obj.put(Constants.BOARD_VERSION, Constants.BOARD_VERSION_ALL);

		// Current blocks
		JSONArray jsonCurrent = new JSONArray();
		for (Player p : playerList) {
			Block current = p.current;
			if (current != null) {
				JSONObject c = current.export();
				c.put(Constants.YOUR_CURRENT, p == player);
				jsonCurrent.put(c);
			}
		}
		obj.put(Constants.CURRENT, jsonCurrent);

		// Next block
		if (next != null) {
			int nextID = next.getType().toInt();
			obj.put(Constants.NEXT, nextID);
		}

		// Ghost block
		Block ghost = player.ghost;
		if (ghost != null) {
			obj.put(Constants.GHOST, ghost.export());
		}

		// Static blocks
		if (player.export()) {
			JSONArray jsonStatic = new JSONArray();
			for (Block b : staticBlocks) {
				jsonStatic.put(b.export());
			}
			obj.put(Constants.BOARD, jsonStatic);
		}

		return obj;
	}

	/**
	 * Generates a random block and returns it
	 * 
	 * @return the block
	 */
	public Block generateRandomBlock() {
		Random r = new Random();
		int x = cols / 2;
		int y = 0;
		int val = r.nextInt(7);
		return Block.generate(BlockType.generate(val), x, y);
	}

	/**
	 * Resets the board, removing all blocks
	 */
	public synchronized void reset() {
		for (int i = 0; i < players; i++) {
			playerList[i].current = null;
			playerList[i].ghost = null;
		}
		next = generateRandomBlock();
		staticBlocks.clear();
	}

	public Player getPlayer(int player) {
		return playerList[player];
	}

}
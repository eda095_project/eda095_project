package shared.game.board;

import shared.game.block.Block;

public class Player {
	Block current;
	Block ghost;
	private boolean export;
	int index;

	public Player(int index) {
		this.index = index;
	}
	
	public boolean export() {
		if (export == true) {
			export = false;
			return true;
		}
		return false;
	}
	
	public void setExport() {
		export = true;
	}
	
	public Block getCurrent() {
		return current;
	}
	public Block getGhost() {
		return ghost;
	}
}

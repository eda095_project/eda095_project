package shared.game.board;

import java.util.List;

import shared.game.block.Block;

public interface ClientBoard {

	public int getWidth();

	public int getHeight();

	public Block getNext();

	public List<Block> getOther();

	public Block getGhost();

	public Block getCurrent();

	public List<Block> getStaticBlocks();

	public void addStaticBlock(Block b);

}

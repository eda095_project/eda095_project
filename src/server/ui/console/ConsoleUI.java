package server.ui.console;

import server.ui.ServerUI;
import shared.Functions;

public class ConsoleUI extends ServerUI {

	public ConsoleUI() {
		new InputReader(this);
	}

	@Override
	public void println(String s) {
		System.out.printf("[%s] %s%n", Functions.getTime(), s);
	}

	@Override
	public void printErr(String s) {
		System.err.printf("[%s] %s%n", Functions.getTime(), s);
	}

	public void perform(String s) {
		String[] message = s.split("\\s+", 2);
		if (message.length < 2) {
			printErr("Unknwon command, " + s);
		} else {
			switch (message[0]) {
			case "alert":
				model.systemMessage(message[1]);
				break;
			case "achtung":
				model.announce(message[1]);
				break;
			default:
				printErr("Unknwon command, " + s);
			}
		}
	}
}

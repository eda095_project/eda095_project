package server.ui.console;

import java.util.Scanner;

public class InputReader extends Thread {
	private ConsoleUI ui;
	private Scanner s;

	public InputReader(ConsoleUI ui) {
		this.ui = ui;
		s = new Scanner(System.in);
		start();
	}

	@Override
	public void run() {
		while (s.hasNext()) {
			ui.perform(s.nextLine());
		}
		s.close();
		ui.println("Bye!");
		System.exit(0);
	}
}

package server.ui;

import server.model.ServerModel;
import server.model.lobby.Lobby;

public abstract class ServerUI {

	protected ServerModel model;
	protected Lobby lobby;

	public void setModel(ServerModel model) {
		this.model = model;
		println("Model set");
	}

	public void setLobby(Lobby lobby) {
		this.lobby = lobby;
		println("Lobby set");
	}

	public abstract void println(String s);

	public void println() {
		println("");
	}

	public abstract void printErr(String s);

	public void printErr() {
		printErr("");
	}
}

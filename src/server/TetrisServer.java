package server;

import java.io.IOException;
import java.net.BindException;

import connection.Server;
import server.model.ServerModel;
import server.model.lobby.Lobby;
import server.ui.ServerUI;
import server.ui.console.ConsoleUI;
import shared.Constants;

public class TetrisServer {

	private static ServerUI ui;

	public static void main(String[] args) {
		try {
			ui = new ConsoleUI();

			Lobby lobby = new Lobby();
			ui.setLobby(lobby);
			ServerModel model = new ServerModel(lobby);
			ui.setModel(model);
			Server server = new Server(Constants.SERVER_PORT, model);
			server.start();
		} catch (BindException e) {
			printErr("Could not start server, port in use.");
			printErr("Make sure no other server is up and restart this aplication.");
			System.exit(2);
		} catch (IOException e1) {
			printErr("Could not read gamefiles");
			System.exit(1);
		}
	}

	public static void println(String s) {
		ui.println(s);
	}

	public static void println() {
		ui.println();
	}

	public static void printErr(String s) {
		ui.printErr(s);
	}

	public static void printErr() {
		ui.printErr();
	}

}

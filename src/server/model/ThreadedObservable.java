package server.model;

import java.util.Observable;

public class ThreadedObservable extends Observable {

	@Override
	public void notifyObservers(Object arg) {
		new Notifier(arg);
	}

	private class Notifier extends Thread {

		private Object o;

		public Notifier(Object o) {
			this.o = o;
			start();
		}

		@Override
		public void run() {
			ThreadedObservable.super.notifyObservers(o);
		}

	}
}

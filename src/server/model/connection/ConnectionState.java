package server.model.connection;

import shared.Constants;

public enum ConnectionState {
	connected, lobby, game,offline;

	public int toInt() {
		switch (this) {
		case connected:
			return Constants.CONNECTION_STATE_CONNECTED;
		case lobby:
			return Constants.CONNECTION_STATE_LOBBY;
		case game:
			return Constants.CONNECTION_STATE_GAME;
		case offline:
			return Constants.CONNECTION_STATE_OFFLINE;
		default:
			return -1;
		}
	}
}

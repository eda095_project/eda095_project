package server.model.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.swing.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

import server.TetrisServer;
import server.model.ServerModel;
import server.model.ThreadedObservable;
import server.model.chat.ChatMessage;
import server.model.games.GameController;
import server.model.lobby.Lobby;
import server.model.lobby.data.GameMode;
import shared.Constants;
import shared.connection.factories.ServerTetrisFactory;
import shared.connection.models.ServerTetrisModel;
import shared.connection.packets.AnnouncementPacket;
import connection.Client;

public class ConnectionModel extends ThreadedObservable implements
		ServerTetrisModel {

	private ServerModel superModel;
	private Client client;
	private ServerTetrisFactory factory;

	private Lobby lobby;
	private GameController game;
	private ConnectionState state;
	private HashMap<String, String> info;
	private String session;
	private Queue<ChatMessage> messages;

	public ConnectionModel(ServerModel superModel, Socket s, Lobby l) {
		this.superModel = superModel;
		this.lobby = l;
		session = null;
		info = new HashMap<>();
		messages = new LinkedList<>();
		info.put(Constants.ADDRESS, s.getRemoteSocketAddress().toString());

		factory = new ServerTetrisFactory(this);
		client = new Client(s, factory);
		client.startPinger(1000, 2000);

		state = ConnectionState.connected;
		print("New client connected, " + info.get(Constants.ADDRESS));

		new ConnectionStatusUpdater();
	}

	public synchronized ConnectionState getState() {
		return state;
	}

	public synchronized void setGame(GameController game) {
		this.game = game;
		factory.setGameModel(game);
		notifyAll();
	}

	private synchronized JSONArray getMessages() {
		JSONArray data = new JSONArray();
		while (!messages.isEmpty())
			data.put(messages.poll().toJSON());
		return data;
	}

	public boolean isAlive() {
		return client.isAlive();
	}

	public String getName() {
		return info.get(Constants.NAME);
	}

	public String getNameAndStatus() {
		String status = isAlive() ? "" : "Disconnected";
		return getName() + "," + status;
	}

	@Override
	public JSONObject getStatus() {
		JSONObject data = new JSONObject();
		data.put(Constants.CONNECTION_STATE, state.toInt());

		JSONObject extra = new JSONObject();
		switch (state) {
		case game:
			extra = game.getStatus();
			break;
		case connected:
			break;
		case lobby:
			extra.put(Constants.STATUS_DATA_GAMEMODES,
					lobby.getStatus(info.get(Constants.VERSION)));
			JSONArray users = new JSONArray();
			extra.put(Constants.STATUS_DATA_CHAT, users);
			for (String user : superModel.getLobbyUsers()) {
				users.put(user);
			}
			extra.put(Constants.STATUS_DATA_CHATMESSAGES, getMessages());
			break;
		default:
			break;
		}
		data.put(Constants.STATUS_DATA, extra);
		return data;
	}

	@Override
	public JSONObject handshake(Map<String, String> map) {
		String session = map.get(Constants.SESSION);

		JSONObject obj = new JSONObject();
		if (session == null) {
			this.session = superModel.addModel(this);
			obj.put(Constants.SESSION, this.session);
			state = ConnectionState.lobby;
			info.putAll(map);

		} else {
			ConnectionModel cm = superModel.updateModel(session, this);
			state = cm.state;
			info = cm.info;
			game = cm.game;
			game.setModel(this);
			factory.setGameModel(game);
			this.session = cm.session;
			obj.put(Constants.GAMEMODE, game.getType());
			obj.put(Constants.SESSION, session);
		}
		List<GameMode> gamemodes = lobby.getGameModes(map
				.get(Constants.VERSION));
		JSONArray ja = new JSONArray();
		for (GameMode gm : gamemodes) {
			StringBuilder sb = new StringBuilder();
			sb.append(gm.getName()).append(":");
			sb.append(gm.getTitle()).append(":");
			sb.append(gm.getPlayers()).append(":");
			sb.append(gm.getDescription());
			ja.put(sb.toString());
		}
		obj.put(Constants.GAMEMODES, ja);

		superModel.systemMessage(getName() + " joined the lobby");

		return obj;
	}

	@Override
	public String toString() {
		return info.get(Constants.NAME) + "@" + info.get(Constants.ADDRESS);
	}

	@Override
	public boolean joinGame(String type) {
		return lobby.joinGame(this, type);
	}

	@Override
	public JSONObject gameInfo() {
		int status = -1;
		JSONObject data = new JSONObject();
		if (!lobby.inQueue(this) && game == null) {
			status = Constants.GAMEINFO_NO_QUEUE;
		} else if (game == null) {
			status = Constants.GAMEINFO_IN_QUEUE;
		} else {
			status = Constants.GAMEINFO_IN_GAME;
			for (String s : game.getDimensions().keySet()) {
				data.put(s, game.getDimensions().get(s));
			}
			state = ConnectionState.game;
		}
		data.put(Constants.GAMEINFO_STATUS, status);
		return data;
	}

	private class ConnectionStatusUpdater implements ActionListener {

		private Timer t;

		public ConnectionStatusUpdater() {
			t = new Timer(1000, this);
			t.start();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!client.isAlive()) {
				lobby.remove(ConnectionModel.this);
				if (game != null) {
					game.lockGame();
				}
				t.stop();
				setChanged();
				notifyObservers();
				print("Lost connection to client: "
						+ info.get(Constants.ADDRESS));
			}
		}
	}

	@Override
	public JSONObject sessionData(String session) {
		ConnectionModel cm = superModel.getModel(session);
		JSONObject obj = new JSONObject();
		obj.put(Constants.VALID, cm != null);
		return obj;
	}

	public boolean inGame() {
		return state == ConnectionState.game;
	}

	@Override
	public boolean equals(Object obj) {
		if (session == null) {
			return super.equals(obj);
		}
		if (obj instanceof ConnectionModel) {
			ConnectionModel cm = (ConnectionModel) obj;
			return session.equals(cm.session);
		}
		return false;
	}

	public void print(String s) {
		TetrisServer.println(client + "\t" + s);
	}

	@Override
	public void leaveQueue() {
		lobby.leaveQueue(this);
	}

	@Override
	public void chat(String message) {
		ChatMessage cm = new ChatMessage(message, getName());
		superModel.chat(cm);
	}

	public synchronized void sendChat(ChatMessage message) {
		messages.offer(message);
	}

	public void announce(String message) {
		client.sendNoResponse(new AnnouncementPacket(message));
	}
}

package server.model.chat;

public class SystemMessage extends ChatMessage {

	public SystemMessage(String message) {
		super(message, "SYSTEM");
	}

}

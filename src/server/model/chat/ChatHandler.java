package server.model.chat;

import server.model.connection.ConnectionModel;
import server.model.connection.ConnectionState;
import server.model.session.SessionHandler;
import shared.data.BlockingQueue;

public class ChatHandler {

	private SessionHandler sessions;

	private BlockingQueue<ChatMessage> queue;

	public ChatHandler(SessionHandler sessions) {
		this.sessions = sessions;
		queue = new BlockingQueue<>();
		new ChatSender().start();
	}

	public void send(ChatMessage message) {
		queue.offer(message);
	}

	@Override
	public String toString() {
		return queue.toString();
	}

	private class ChatSender extends Thread {

		@Override
		public void run() {
			while (true) {
				ChatMessage message = queue.poll();
				if (message.publicMessage()) {
					for (ConnectionModel model : sessions
							.getModels(ConnectionState.lobby)) {
						model.sendChat(message);
					}
				} else {
				}
			}
		}
	}
}

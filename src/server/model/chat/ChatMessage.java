package server.model.chat;

import org.json.JSONObject;

import shared.Constants;
import shared.chat.Message;

public class ChatMessage extends Message {

	private String to;

	public ChatMessage(String message, String from, String to) {
		super(from, message, System.currentTimeMillis());
		this.to = to;
	}

	public ChatMessage(String message, String from) {
		this(message, from, null);
	}

	public boolean publicMessage() {
		return to == null;
	}

	@Override
	public String toString() {
		return "[" + timestamp + "]" + from + ":\t" + message;
	}

	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put(Constants.CHAT_FROM, from);
		obj.put(Constants.CHAT_MESSAGE, message);
		obj.put(Constants.CHAT_TIMESTAMP, timestamp);
		return obj;
	}
}

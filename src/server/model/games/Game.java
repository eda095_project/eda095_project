package server.model.games;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

import server.model.connection.ConnectionModel;
import server.model.games.score.Score;
import shared.Constants;
import shared.game.Direction;
import shared.game.block.Block;
import shared.game.board.Player;
import shared.game.board.TetrisBoard;

public abstract class Game implements ActionListener {
	public static final int DEFAULT_DELAY = 1000;

	protected Timer timer;
	protected TetrisBoard board;
	protected List<GameController> controllers;
	protected int delay;
	protected GameState gameState = GameState.NOT_STARTED;
	protected Score score;
	protected int players;
	private String type;

	public Game(String type, int players) {
		this.type = type;
		this.players = players;
		controllers = new ArrayList<>();
		delay = DEFAULT_DELAY;
		timer = new Timer(delay, this);
	}

	protected synchronized String getType() {
		return type;
	}

	public synchronized void startGame() {
		gameState = GameState.STARTED;
		timer.start();
	}

	protected synchronized void stopGame() {
		gameState = GameState.ENDED;
		timer.stop();
	}

	protected synchronized void togglePauseGame() {
		if (gameState == GameState.STARTED) {
			pauseGame();
		} else if (gameState == GameState.PAUSED) {
			resumeGame();
		}
	}

	protected synchronized void lockGame() {
		pauseGame();
		gameState = GameState.LOCKED;
	}

	public synchronized void unlockGame() {
		pauseGame();
	}

	private synchronized void pauseGame() {
		gameState = GameState.PAUSED;
		timer.stop();
	}

	private synchronized void resumeGame() {
		gameState = GameState.STARTED;
		timer.start();
	}

	public synchronized JSONObject exportBoard(Player player) {
		return board.exportBoard(player);
	}

	public synchronized JSONObject exportEntireBoard(Player player) {
		player.setExport();
		return board.exportBoard(player);
	}

	protected JSONObject getStatus(Player player) {
		JSONObject data = new JSONObject();
		data.put(Constants.GAME_STATE, gameState.translate());

		JSONArray players = new JSONArray();
		for (GameController cont : controllers) {
			players.put(cont.getModel().getNameAndStatus());
		}
		data.put(Constants.PLAYER_NAMES, players);
		return data;
	}

	protected void tryUnlock() {
		boolean shouldUnlock = true;
		for (GameController cont : controllers) {
			if (!cont.getModel().isAlive()) {
				shouldUnlock = false;
			}
		}
		if (shouldUnlock) {
			unlockGame();
		}
	}

	public synchronized void addClient(ConnectionModel cm) {
		if (players == controllers.size()) {
			throw new RuntimeException("Can't add more players than allowed");
		}
		int playerID = controllers.size();
		GameController cont = createController(playerID, cm);
		controllers.add(cont);
		cm.setGame(cont);
	}

	protected abstract GameController createController(int token,
			ConnectionModel model);

	protected synchronized boolean move(Player player, Direction dir) {
		if (gameState == GameState.STARTED) {
			return board.moveCurrentIfLegal(player, dir);
		}
		return false;
	}

	protected synchronized void rotate(Player player) {
		if (gameState == GameState.STARTED) {
			board.rotateCurrentIfLegal(player);
		}
	}

	public synchronized int forceDown(Player player) {
		if (gameState == GameState.STARTED) {
			return board.forceDown(player);
		}
		return 0;
	}

	public synchronized JSONObject getDimensions() {
		JSONObject obj = new JSONObject();
		obj.put(Constants.ROWS, board.getRows());
		obj.put(Constants.COLS, board.getCols());
		return obj;
	}

	public abstract void gameTick();

	@Override
	public void actionPerformed(ActionEvent e) {
		synchronized (this) {
			gameTick();
		}
	}

	protected synchronized int updateBoardWithNewCurrent(Player player) {
		int rowsRemoved = 0;
		Block current = board.getCurrent(player);
		if (current != null) {
			board.addStaticBlock(current);
			rowsRemoved = checkRemoveRows(current);
		}
		if (!board.setCurrent(player, board.generateRandomBlock())) {
			stopGame();
		}
		return rowsRemoved;
	}

	protected synchronized int checkRemoveRows(Block current) {
		int rowsRemoved = 0;
		Set<Integer> possibleRowsToRemove = current.getYVals();
		List<Integer> rowsToRemove = new ArrayList<>();
		synchronized (board) {
			for (int row : possibleRowsToRemove) {
				if (board.hasRow(row)) {
					rowsToRemove.add(row);
					rowsRemoved++;
				}
			}
			Collections.sort(rowsToRemove);
			board.removeRows(rowsToRemove);
		}
		return rowsRemoved;
	}

}
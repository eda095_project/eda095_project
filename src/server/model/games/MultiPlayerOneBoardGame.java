package server.model.games;

import org.json.JSONObject;

import server.model.connection.ConnectionModel;
import server.model.games.score.SharedScore;
import shared.Constants;
import shared.game.Direction;
import shared.game.board.Player;
import shared.game.board.StandardTetrisBoard;

public class MultiPlayerOneBoardGame extends Game {
	private int lines = 0;
	private int level = 1;
	private static final int[] pointModifiers = { 0, 40, 100, 300, 1200 };

	public MultiPlayerOneBoardGame(String type, int players) {
		super(type, players);
		board = new StandardTetrisBoard(players);
		score = new SharedScore(pointModifiers);
	}

	public MultiPlayerOneBoardGame(String type, int players, int rows,
			int columns) {
		super(type, players);
		board = new StandardTetrisBoard(rows, columns, players);
		score = new SharedScore(pointModifiers);
	}

	@Override
	public void gameTick() {
		boolean[] movedIndices = board.moveCurrentsDownIfLegal();
		for (int i = 0; i < movedIndices.length; i++) {
			if (!movedIndices[i]) {
				int tetris = updateBoardWithNewCurrent(board.getPlayer(i));
				lines += tetris;
				score.scoreTetris(tetris, level);
				updateLevel();
			} else {
				score.scoreTick();
			}
		}
	}

	private void updateLevel() {
		int tempLevel = 1 + lines / 10;
		if (tempLevel > level) {
			level = tempLevel;
			timer.setDelay((int) (1000 * 1.17 * Math.pow(Math.E, -0.4
					* (level - 1))));
		}
	}

	@Override
	public JSONObject getStatus(Player player) {
		JSONObject status = super.getStatus(player);
		status.put(Constants.SCORE, score.getPoints());
		status.put(Constants.LINES, lines);
		status.put(Constants.LEVEL, level);
		return status;
	}

	@Override
	protected GameController createController(int token, ConnectionModel model) {
		Player p = board.getPlayer(token);
		return new Controller(p, model);
	}

	private class Controller extends GameController {

		Controller(Player token, ConnectionModel model) {
			super(token, model);
		}

		@Override
		public JSONObject getBoard() {
			return exportBoard(token);
		}

		@Override
		public JSONObject getEntireBoard() {
			return exportEntireBoard(token);
		}

		@Override
		protected void left() {
			move(token, Direction.LEFT);
		}

		@Override
		protected void right() {
			move(token, Direction.RIGHT);
		}

		@Override
		protected void down() {
			if (move(token, Direction.DOWN)) {
				score.scoreMove();
			}
		}

		@Override
		protected void rotate() {
			MultiPlayerOneBoardGame.this.rotate(token);
		}

		@Override
		protected void forceDown() {
			int steps = MultiPlayerOneBoardGame.this.forceDown(token);
			score.scoreForce(steps);
		}

		@Override
		protected void pause() {
			togglePauseGame();
		}

		@Override
		public void lockGame() {
			MultiPlayerOneBoardGame.this.lockGame();
		}

		@Override
		public JSONObject getStatus() {
			return MultiPlayerOneBoardGame.this.getStatus(token);
		}

		@Override
		public String getType() {
			return MultiPlayerOneBoardGame.this.getType();
		}

		@Override
		public JSONObject getDimensions() {
			return MultiPlayerOneBoardGame.this.getDimensions();
		}

		@Override
		protected void unlock() {
			tryUnlock();
		}
	}
}
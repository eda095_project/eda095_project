package server.model.games;

import shared.Constants;

public enum GameState {
	NOT_STARTED, STARTED, PAUSED, LOCKED, ENDED;

	public String translate() {
		switch (this) {
		case NOT_STARTED:
			return Constants.NOT_STARTED;
		case STARTED:
			return Constants.STARTED;
		case PAUSED:
			return Constants.PAUSED;
		case ENDED:
			return Constants.ENDED;
		case LOCKED:
			return Constants.LOCKED;
		default:
			throw new IllegalArgumentException("Not supported type");
		}
	}

}

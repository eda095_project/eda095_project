package server.model.games.score;

public class SharedScore extends Score {

	private int[] pointModifiers;

	public SharedScore(int[] pointModifiers) {
		this.pointModifiers = pointModifiers;
	}

	@Override
	public void scoreTetris(int n, int level) {
		if (n >= pointModifiers.length) {
			n = pointModifiers.length - 1;
		}
		points += pointModifiers[n] * level;
	}

	@Override
	public void scoreTick() {
		scoreForce(1);
	}

	@Override
	public void scoreMove() {
		scoreForce(1);
	}

	@Override
	public void scoreForce(int steps) {
		points += steps;
	}
}

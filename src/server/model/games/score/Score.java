package server.model.games.score;

public abstract class Score {

	protected int points;

	public int getPoints() {
		return points;
	}

	public abstract void scoreTick();
	
	public abstract void scoreMove();

	public abstract void scoreForce(int steps);
	
	public abstract void scoreTetris(int n, int level);

}

package server.model.games;

import org.json.JSONObject;

import server.model.connection.ConnectionModel;
import shared.Constants;
import shared.connection.models.ServerGameModel;
import shared.game.board.Player;

public abstract class GameController implements ServerGameModel {

	protected Player token;
	private ConnectionModel model;

	GameController(Player token, ConnectionModel model) {
		this.token = token;
		this.model = model;
	}

	ConnectionModel getModel() {
		return model;
	}

	public void setModel(ConnectionModel model) {
		this.model = model;
		unlock();
	}

	@Override
	public void keyPress(int key) {
		switch (key) {
		case Constants.COMMAND_LEFT:
			left();
			break;
		case Constants.COMMAND_RIGHT:
			right();
			break;
		case Constants.COMMAND_DOWN:
			down();
			break;
		case Constants.COMMAND_ROTATE:
			rotate();
			break;
		case Constants.COMMAND_FORCE_DOWN:
			forceDown();
			break;
		case Constants.COMMAND_PAUSE:
			pause();
			break;
		default:
			// Do nothing
			break;
		}
	}

	protected abstract void left();

	protected abstract void right();

	protected abstract void down();

	protected abstract void rotate();

	protected abstract void forceDown();

	protected abstract void pause();

	public abstract void lockGame();

	public abstract JSONObject getStatus();

	public abstract String getType();

	public abstract JSONObject getDimensions();

	protected abstract void unlock();
}
package server.model.session;

import java.util.Observable;
import java.util.Observer;

import server.model.connection.ConnectionModel;
import shared.Constants;
import shared.Functions;

class Session implements Comparable<Session>, Observer {

	private ConnectionModel cm;
	private SessionHandler sh;
	private long timestamp;
	private String session;

	public Session(SessionHandler sh, ConnectionModel cm, String session) {
		this.cm = cm;
		this.session = session;
		this.sh = sh;
		timestamp = Long.MAX_VALUE;

		cm.addObserver(this);
	}

	@Override
	public int compareTo(Session s) {
		if (timestamp < s.timestamp) {
			return -1;
		}
		if (timestamp > s.timestamp) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Session) {
			return ((Session) obj).compareTo(this) == 0;
		}
		return false;
	}

	public boolean available(String session) {
		if (this.session.equals(session) && !cm.isAlive())
			return true;
		return false;

	}

	public ConnectionModel getModel() {
		return cm;
	}

	public void updateModel(ConnectionModel cm) {
		this.cm = cm;
		updateTimespamp();
	}

	@Override
	public synchronized String toString() {
		if (timestamp == Long.MAX_VALUE)
			return "Online";
		if (timestamp < System.currentTimeMillis())
			return "Should be purged";
		long time = timeLeft();
		return Functions.milliesToString(time);
	}

	private synchronized void updateTimespamp() {
		if (!cm.isAlive()) {
			if (cm.inGame()) {
				timestamp = System.currentTimeMillis() + Constants.SESSION_TIMEOUT_TIME;
			} else {
				timestamp = 0;
			}
		} else {
			timestamp = Long.MAX_VALUE;
		}
		sh.update();
	}

	@Override
	public synchronized void update(Observable arg0, Object arg1) {
		updateTimespamp();
	}

	public synchronized long timeLeft() {
		return timestamp - System.currentTimeMillis();
	}

	public synchronized boolean isOnline() {
		return timestamp == Long.MAX_VALUE;
	}

}

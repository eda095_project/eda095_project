package server.model.session;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import server.model.connection.ConnectionModel;
import server.model.connection.ConnectionState;

public class SessionHandler {

	private PriorityQueue<Session> sessions;

	public SessionHandler() {
		sessions = new PriorityQueue<>();
		new SessionRemover().start();
	}

	public synchronized String add(ConnectionModel cm) {
		SecureRandom r = new SecureRandom();
		String session = new BigInteger(130, r).toString();
		sessions.offer(new Session(this, cm, session));
		notifyAll();
		return session;
	}

	public synchronized ConnectionModel update(String id,
			ConnectionModel newModel) {
		Session s = getSession(id);
		ConnectionModel oldModel = s.getModel();
		s.updateModel(newModel);
		return oldModel;
	}

	public synchronized ConnectionModel get(String id) {
		Session s = getSession(id);
		if (s == null)
			return null;
		ConnectionModel cm = s.getModel();
		if (cm.inGame())
			return cm;
		return null;
	}

	private synchronized Session getSession(String id) {
		for (Session session : sessions) {
			if (session.available(id)) {
				return session;
			}
		}
		return null;
	}

	public synchronized String toString() {
		if (sessions.isEmpty()) {
			return "No sessions";
		}
		int live = 0;
		int dead = 0;

		for (Session s : sessions) {
			if (s.isOnline()) {
				live++;
			} else {
				dead++;
			}
		}
		return live + " online, " + dead + " dead";
	}

	private class SessionRemover extends Thread {
		@Override
		public void run() {
			try {
				while (true) {
					removeOutdatedSession();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized Session removeOutdatedSession()
			throws InterruptedException {
		PriorityQueue<Session> temp = new PriorityQueue<>();
		temp.addAll(sessions);
		sessions = temp;
		while (sessions.isEmpty()) {
			wait();
		}
		Session s = sessions.peek();

		if (s.timeLeft() > 0) {
			wait(s.timeLeft());
		}
		if (s.timeLeft() < 0) {
			sessions.remove(s);
			return s;
		}
		return null;
	}

	synchronized void update() {
		notifyAll();
	}

	public synchronized List<ConnectionModel> getModels(ConnectionState state) {
		ArrayList<ConnectionModel> models = new ArrayList<>();
		for (Session s : sessions) {
			ConnectionModel cm = s.getModel();
			if (cm.getState().equals(state))
				models.add(cm);
		}
		return models;
	}

	public synchronized List<ConnectionModel> getModels() {
		ArrayList<ConnectionModel> models = new ArrayList<>();
		for (Session s : sessions) {
			models.add(s.getModel());
		}
		return models;
	}
}
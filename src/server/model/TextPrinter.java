package server.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import server.TetrisServer;

public class TextPrinter implements ActionListener {

	private Object o;
	String type;
	private String last = "";

	public TextPrinter(Object o, String type) {
		this.o = o;
		this.type = type;
		new Timer(1000, this).start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String next = o.toString();
		if (!next.equals(last)) {
			last = next;
			TetrisServer.println(type + "\t" + next);
		}
	}

}

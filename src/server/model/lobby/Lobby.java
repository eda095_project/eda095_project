package server.model.lobby;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import server.model.TextPrinter;
import server.model.connection.ConnectionModel;
import server.model.games.Game;
import server.model.games.MultiPlayerOneBoardGame;
import server.model.lobby.data.Data;
import server.model.lobby.data.GameMode;
import shared.Constants;

public class Lobby {

	private Data data;

	private HashMap<ConnectionModel, String> users;
	private HashMap<String, LobbyQueue> queues;
	private HashMap<String, Integer> lobbyStats;

	public Lobby() throws IOException {
		users = new HashMap<>();
		queues = new HashMap<>();
		lobbyStats = new HashMap<>();

		data = new Data();
		data.load();

		List<GameMode> gamemodes = data.getAllGames();
		for (GameMode gamemode : gamemodes) {
			String name = gamemode.getName();
			int players = gamemode.getPlayers();
			queues.put(name, new LobbyQueue(this, name, players));
		}

		for (String s : queues.keySet())
			lobbyStats.put(s, 0);

		new TextPrinter(users, "Lobby");
	}

	public synchronized boolean joinGame(ConnectionModel cm, String type) {
		LobbyQueue lq = queues.get(type);
		if (lq == null)
			return false;
		lq.add(cm);
		return true;
	}

	public synchronized void leaveQueue(ConnectionModel cm) {
		for (String type : queues.keySet()) {
			LobbyQueue lq = queues.get(type);
			lq.remove(cm);
		}
	}

	void startGame(String type, List<ConnectionModel> cms) {
		Game g;
		switch (type) {
		case Constants.SIMPLE2:
			g = new MultiPlayerOneBoardGame(type, 1, 25, 30);
			break;
		case Constants.TWO_PLAYER:
			g = new MultiPlayerOneBoardGame(type, 2);
			break;
		case Constants.THREE_PLAYER:
			g = new MultiPlayerOneBoardGame(type, 3);
			break;
		case Constants.FOUR_PLAYER:
			g = new MultiPlayerOneBoardGame(type, 4);
			break;
		case Constants.SIMPLE:
			// Fall-through
		default:
			type = Constants.SIMPLE;
			g = new MultiPlayerOneBoardGame(type, 1);
		}
		for (ConnectionModel cm : cms) {
			users.put(cm, type);
			g.addClient(cm);

			Integer i = lobbyStats.get(type);
			if (i == null)
				i = 0;
			i++;
			lobbyStats.put(type, i);
		}
		g.startGame();
	}

	public List<GameMode> getGameModes(String version) {
		return data.getGames(version);
	}

	public void remove(ConnectionModel cm) {
		String type = users.remove(cm);
		if (type != null) {
			Integer i = lobbyStats.get(type);
			lobbyStats.put(type, --i);
		}
		leaveQueue(cm);
	}

	public JSONObject getStatus(String version) {
		JSONObject data = new JSONObject();
		List<GameMode> gamemodes = getGameModes(version);
		for (GameMode gm : gamemodes) {
			JSONObject mode = new JSONObject();
			String s = gm.getName();
			mode.put("playing", lobbyStats.get(s));
			mode.put("queue", queues.get(s).size());
			data.put(s, mode);
		}
		return data;
	}

	public boolean inQueue(ConnectionModel cm) {
		for (String type : queues.keySet()) {
			if (queues.get(type).has(cm))
				return true;
		}
		return false;
	}
}
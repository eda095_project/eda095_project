package server.model.lobby.data;

import static server.model.lobby.data.DataConstants.GAMEMODES;
import static server.model.lobby.data.DataConstants.JSON_GAMEMODES;
import static server.model.lobby.data.DataConstants.JSON_GAMEMODES_ACCESS;
import static server.model.lobby.data.DataConstants.JSON_GAMEMODES_DESCRIPTION;
import static server.model.lobby.data.DataConstants.JSON_GAMEMODES_PLAYERS;
import static server.model.lobby.data.DataConstants.JSON_GAMEMODES_TITLE;
import static server.model.lobby.data.DataConstants.JSON_HIGHSCORE;
import static server.model.lobby.data.DataConstants.getAccess;
import static server.model.lobby.data.DataConstants.getDescription;
import static server.model.lobby.data.DataConstants.getPlayers;
import static server.model.lobby.data.DataConstants.getTitle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;

public class Data {
	private File file;
	private JSONObject root;

	public Data() {
		File folder = new File(Constants.SERVER_DATA_FOLDER);
		if (!folder.exists())
			folder.mkdir();

		file = new File(folder, Constants.SERVER_DATA_FILE);
	}

	public void load() throws IOException {
		if (file.exists()) {
			StringBuilder sb = new StringBuilder();
			Scanner s = new Scanner(file);
			while (s.hasNext()) {
				sb.append(s.nextLine());
			}
			s.close();
			root = new JSONObject(sb.toString());
		} else {
			System.out.println("No data file found, creating default.");
			setDefaultValues();
			save();
		}
	}

	private void setDefaultValues() {
		root = new JSONObject();
		JSONObject highscore = new JSONObject();
		for (String gamemode : GAMEMODES) {
			highscore.put(gamemode, new JSONArray());
		}
		JSONObject gamemodes = new JSONObject();
		for (String gamemode : GAMEMODES) {
			JSONObject game = new JSONObject();
			game.put(JSON_GAMEMODES_DESCRIPTION, getDescription(gamemode));
			game.put(JSON_GAMEMODES_PLAYERS, getPlayers(gamemode));
			game.put(JSON_GAMEMODES_TITLE, getTitle(gamemode));
			game.put(JSON_GAMEMODES_ACCESS, getAccess(gamemode));
			gamemodes.put(gamemode, game);
		}

		root.put(JSON_HIGHSCORE, highscore);
		root.put(JSON_GAMEMODES, gamemodes);
	}

	public void save() throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(file);
		pw.write(root.toString(4));
		pw.flush();
		pw.close();
	}

	public List<GameMode> getAllGames() {
		ArrayList<GameMode> games = new ArrayList<>();
		JSONObject modes = root.getJSONObject(JSON_GAMEMODES);
		for (String name : modes.keySet()) {
			JSONObject gamemode = modes.getJSONObject(name);
			String title = gamemode.getString(JSON_GAMEMODES_TITLE);
			int players = gamemode.getInt(JSON_GAMEMODES_PLAYERS);
			String description = gamemode.getString(JSON_GAMEMODES_DESCRIPTION);
			games.add(new GameMode(name, title, players, description));
		}
		return games;
	}

	public List<GameMode> getGames(String version) {

		String[] temp = version.split(":", 2);
		String type = temp[0];
		String nr = temp[1];
		ArrayList<GameMode> games = new ArrayList<>();
		JSONObject modes = root.getJSONObject(JSON_GAMEMODES);

		outer: for (String name : modes.keySet()) {
			JSONObject gamemode = modes.getJSONObject(name);
			JSONObject access = gamemode.getJSONObject(JSON_GAMEMODES_ACCESS);
			if (access.has(type)) {
				JSONArray array = access.getJSONArray(type);
				boolean allowed = false;
				inner: for (Object has : array) {
					if (has.equals(nr)) {
						allowed = true;
						break inner;
					}
				}
				if (!allowed)
					continue outer;
			} else {
				continue outer;
			}
			String title = gamemode.getString(JSON_GAMEMODES_TITLE);
			int players = gamemode.getInt(JSON_GAMEMODES_PLAYERS);
			String description = gamemode.getString(JSON_GAMEMODES_DESCRIPTION);
			games.add(new GameMode(name, title, players, description));
		}
		return games;
	}
}

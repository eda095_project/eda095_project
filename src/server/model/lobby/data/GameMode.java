package server.model.lobby.data;

public class GameMode {
	private String name;
	private String title;
	private int players;
	private String description;

	public GameMode(String name, String title, int players, String description) {
		this.name = name;
		this.title = title;
		this.players = players;
		this.description = description;
	}

	@Override
	public String toString() {
		return "GameMode: " + title + " (" + players + ")";
	}

	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public int getPlayers() {
		return players;
	}

	public String getDescription() {
		return description;
	}
}

package server.model.lobby.data;

import org.json.JSONArray;
import org.json.JSONObject;

import shared.Constants;

public class DataConstants {
	public static final String SIMPLE = "simple";
	public static final String SIMPLE2 = "simple2";
	public static final String TWO_PLAYER = "two_player";
	public static final String THREE_PLAYER = "three_player";
	public static final String FOUR_PLAYER = "four_player";

	public static final String[] GAMEMODES = new String[] { SIMPLE, SIMPLE2,
			TWO_PLAYER, THREE_PLAYER, FOUR_PLAYER };

	public static final String JSON_HIGHSCORE = "highscore";
	public static final String JSON_GAMEMODES = "gamemodes";
	public static final String JSON_GAMEMODES_DESCRIPTION = "description";
	public static final String JSON_GAMEMODES_PLAYERS = "players";
	public static final String JSON_GAMEMODES_TITLE = "title";
	public static final String JSON_GAMEMODES_ACCESS = "access";
	public static final String JSON_GAMEMODES_ACCESS_JAVA = "JAVA";

	public static String getDescription(String gamemode) {
		String val = "unknwon";
		switch (gamemode) {
		case Constants.SIMPLE2:
			val = "Just as standard but with a wider game field";
			break;
		case Constants.TWO_PLAYER:
			val = "Our world famous standard game, now with a friend.";
			break;
		case Constants.THREE_PLAYER:
			val = "You thought it could never be this good. But it can, get your two best friends and play this awesome three player mode. Made just for you!";
			break;
		case Constants.FOUR_PLAYER:
			val = "Who are you kidding. You don't have three friends. But don't worry, you can always open 4 windows and play against yourself.";
			break;
		case Constants.SIMPLE:
			val = "A simple tetris game where you play solo";
			break;
		default:
			val = "unknwon game mode";
		}
		return val;
	}

	public static int getPlayers(String gamemode) {
		int val = 1;
		switch (gamemode) {
		case TWO_PLAYER:
			val = 2;
			break;
		case THREE_PLAYER:
			val = 3;
			break;
		case Constants.FOUR_PLAYER:
			val = 4;
			break;
		}
		return val;
	}

	public static String getTitle(String gamemode) {
		String val = "???";
		switch (gamemode) {
		case Constants.SIMPLE2:
			val = "Large";
			break;
		case Constants.TWO_PLAYER:
			val = "2 player";
			break;
		case Constants.THREE_PLAYER:
			val = "3 player";
			break;
		case Constants.FOUR_PLAYER:
			val = "4 player";
			break;
		case Constants.SIMPLE:
			val = "Standard";
			break;
		}
		return val;
	}

	public static JSONObject getAccess(String gamemode) {
		JSONObject array = new JSONObject();
		JSONArray java = new JSONArray();
		switch (gamemode) {
		case Constants.SIMPLE2:
			java.put("1.0");
			break;
		case Constants.TWO_PLAYER:
			java.put("1.0");
			java.put("1.1");
			break;
		case Constants.THREE_PLAYER:
			java.put("1.0");
			java.put("1.1");
			break;
		case Constants.FOUR_PLAYER:
			java.put("1.0");
			java.put("1.1");
			break;
		case Constants.SIMPLE:
			java.put("1.0");
			java.put("1.1");
			break;
		}
		array.put(JSON_GAMEMODES_ACCESS_JAVA, java);
		return array;
	}

}

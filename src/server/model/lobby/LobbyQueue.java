package server.model.lobby;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import server.model.connection.ConnectionModel;

public class LobbyQueue {

	private LinkedList<ConnectionModel> queue;
	private int limit;
	private String type;
	private Lobby l;

	public LobbyQueue(Lobby l, String type, int limit) {
		queue = new LinkedList<>();
		this.limit = limit;
		this.type = type;
		this.l = l;
		new GameCreator().start();
	}

	public synchronized void add(ConnectionModel cm) {
		queue.offer(cm);
		notifyAll();
	}

	private synchronized List<ConnectionModel> get()
			throws InterruptedException {
		while (queue.size() < limit) {
			wait();
		}
		ArrayList<ConnectionModel> list = new ArrayList<>();
		for (int i = 0; i < limit; i++) {
			list.add(queue.poll());
		}

		return list;
	}

	public synchronized int size() {
		return queue.size();
	}

	public synchronized void remove(ConnectionModel cm) {
		queue.remove(cm);
	}

	public synchronized boolean has(ConnectionModel cm) {
		return queue.contains(cm);
	}

	private class GameCreator extends Thread {

		@Override
		public void run() {
			try {
				while (true) {
					List<ConnectionModel> list;
					list = get();
					l.startGame(type, list);
				}
			} catch (InterruptedException e) {
			}
		}

	}
}

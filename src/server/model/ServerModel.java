package server.model;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import server.model.chat.ChatHandler;
import server.model.chat.ChatMessage;
import server.model.chat.SystemMessage;
import server.model.connection.ConnectionModel;
import server.model.connection.ConnectionState;
import server.model.lobby.Lobby;
import server.model.session.SessionHandler;
import connection.ConnectionHandler;

public class ServerModel implements ConnectionHandler {

	private Lobby lobby;
	public static final long STANDARD_TIMEOUT_MS = 1000 * 60 * 5;

	private SessionHandler sh;
	private ChatHandler ch;

	public ServerModel(Lobby lobby) {
		this.lobby = lobby;
		sh = new SessionHandler();
		ch = new ChatHandler(sh);
		new TextPrinter(sh, "Serials");
		new TextPrinter(ch, "Chat");
	}

	@Override
	public void addConnection(Socket s) {
		new ConnectionModel(this, s, lobby);
	}

	public ConnectionModel updateModel(String session, ConnectionModel newModel) {
		return sh.update(session, newModel);
	}

	public ConnectionModel getModel(String session) {
		return sh.get(session);
	}

	public String addModel(ConnectionModel cm) {
		return sh.add(cm);
	}

	public List<String> getLobbyUsers() {
		ArrayList<String> users = new ArrayList<>();

		for (ConnectionModel cm : sh.getModels(ConnectionState.lobby)) {
			String user = cm.getName();
			users.add(user);
		}
		return users;
	}

	public void chat(ChatMessage cm) {
		ch.send(cm);
	}

	public void systemMessage(String message) {
		ch.send(new SystemMessage(message));
	}

	public void announce(String message) {
		systemMessage(message);
		for (ConnectionModel cm : sh.getModels()) {
			cm.announce(message);
		}
	}

}

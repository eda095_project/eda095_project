package connection;

import java.io.IOException;
import java.util.LinkedList;

public class ThreadController {

	private Client c;

	private LinkedList<Runnable> threads;

	public ThreadController(Client c) {
		this.c = c;
		threads = new LinkedList<>();
	}

	public synchronized void add(Runnable r) {
		threads.offer(r);
	}

	public synchronized void remove(Runnable r) {
		threads.remove(r);
		if (threads.isEmpty())
			try {
				c.close();
			} catch (IOException e) {
			}
	}

	@Override
	public synchronized String toString() {
		return threads.size() + " threads awake " + threads.toString();
	}

}

package connection;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;

import shared.Functions;
import connection.data.RandomByteSet;
import connection.packets.SuperPacket;
import connection.packets.factory.PacketFactory;
import connection.packets.factory.SuperFactory;
import connection.packets.operands.NullPacket;
import connection.packets.operands.OperatorPacket;

public class Client {

	private static int clients;
	private final int clientNum;
	private Connection conn;
	private RandomByteSet ids;
	private HashMap<Byte, OperatorPacket> inbox;
	private SuperFactory factory;
	private boolean verbose = false;
	private ThreadController tc;

	public Client(Socket s, Boolean verbose, PacketFactory... factories) {
		this.verbose = verbose;
		clientNum = ++clients;
		factory = new SuperFactory();
		for (PacketFactory f : factories) {
			factory.addFactory(f);
		}

		tc = new ThreadController(this);
		conn = new Connection(s, tc);
		ids = new RandomByteSet();
		inbox = new HashMap<>();
		new PacketHandler().start();
	}

	public Client(Socket s, PacketFactory... factories) {
		this(s, false, factories);
	}

	public Client(String addr, int port, PacketFactory... factories)
			throws UnknownHostException, IOException {
		this(new Socket(addr, port), factories);
	}

	public Client(String addr, int port, boolean verbose,
			PacketFactory... factories) throws UnknownHostException,
			IOException {
		this(new Socket(addr, port), verbose, factories);
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public OperatorPacket sendRecieve(OperatorPacket send) {
		byte id = send(send);
		return recieve(id);
	}

	public synchronized byte send(OperatorPacket p) {
		byte id = ids.getRandomByte();
		SuperPacket sp = new SuperPacket(p, id);
		conn.send(sp.getData());
		return id;
	}

	public synchronized void sendNoResponse(OperatorPacket p) {
		byte id = send(p);
		new ResponseWaiter(id);
	}

	public synchronized OperatorPacket recieve(byte id) {
		OperatorPacket op = null;
		while ((op = inbox.get(id)) == null)
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace(System.err);
			}
		inbox.remove(id);
		ids.returnByte(id);
		return op;
	}

	private synchronized OperatorPacket recieve(byte id, int timeout) {
		OperatorPacket op = null;
		long called = System.currentTimeMillis();
		while ((op = inbox.get(id)) == null
				&& (called + timeout > System.currentTimeMillis()))
			try {
				wait(500);
			} catch (InterruptedException e) {
				e.printStackTrace(System.err);
			}
		inbox.remove(id);
		ids.returnByte(id);
		return op;
	}

	private synchronized void addResponse(byte id, OperatorPacket op) {
		inbox.put(id, op);
		notifyAll();
	}

	public void close() throws IOException {
		conn.close();
	}

	public boolean isAlive() {
		return conn.isAlive();
	}

	private class PacketHandler extends Thread {

		@Override
		public void run() {
			tc.add(this);
			while (conn.isAlive()) {
				byte[] b = conn.recieve();
				print("Recieved package!");
				if (b == null)
					continue;
				try {
					SuperPacket sp = (SuperPacket) factory.createPacket(b);

					if (sp.isAck()) {
						print("ack:\t" + sp);
						byte id = sp.getId();
						OperatorPacket op = sp.getPacket();
						addResponse(id, op);
					} else {
						SuperPacket response = sp.getAck();
						print("incoming:\t" + sp);
						print("response:\t" + response);
						conn.send(response.getData());
					}
				} catch (ClassCastException e) {
					System.err.println("Error casting data to superpacket");
					System.err.println(Arrays.toString(b));
				}
			}
			tc.remove(this);
		}

		@Override
		public String toString() {
			return "PacketHandler";
		}
	}

	private class ResponseWaiter extends Thread {
		private byte id;

		public ResponseWaiter(byte id) {
			this.id = id;
			start();
		}

		@Override
		public void run() {
			tc.add(this);
			print(id + ":yay i got a job!");
			recieve(id);
			print(id + ": Done!");
			tc.remove(this);
		}

		@Override
		public String toString() {
			return "Response waiter";
		}
	}

	public void startPinger(int period, int timeout) {
		new Pinger(period, timeout).start();
	}

	private class Pinger extends Thread {
		private int timeout;
		private int period;

		public Pinger(int period, int timeout) {
			this.timeout = timeout;
			this.period = period;
		}

		@Override
		public void run() {
			tc.add(this);
			boolean alive = true;

			while (alive) {
				try {
					Thread.sleep(period);
				} catch (InterruptedException e1) {
				}
				byte id = send(new NullPacket());
				OperatorPacket op = recieve(id, timeout);
				if (op == null) {
					print("Connection timeout");
					try {
						alive = false;
						close();
					} catch (IOException e) {
					}
				}
			}
			tc.remove(this);
		}

		@Override
		public String toString() {
			return "Pinger";
		}
	}

	@Override
	public String toString() {
		return "Con #" + clientNum;
	}

	private void print(String s) {
		if (verbose)
			System.out.println("[" + Functions.getTime() + "]\tCon #"
					+ clientNum + "\t" + s);
	}

	public void printActiveThreads() {
		System.out.println(tc);
	}
}
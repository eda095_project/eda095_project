package connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

import connection.data.ConnectionQueue;

class Connection {

	private Socket socket;
	private ConnectionQueue inbox;
	private ConnectionQueue outbox;
	private ThreadController tc;

	public Connection(Socket socket, ThreadController tc) {
		this.socket = socket;
		this.tc = tc;
		inbox = new ConnectionQueue();
		outbox = new ConnectionQueue();
		new Reciever().start();
		new Sender().start();
	}

	public synchronized void send(byte[] b) {
		outbox.offer(b);
		notifyAll();
	}

	private synchronized byte[] getData() {
		while (outbox.isEmpty() && !socket.isClosed())
			try {
				wait(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return outbox.poll();
	}

	public synchronized byte[] recieve() {
		while (inbox.isEmpty() && !socket.isClosed())
			try {
				wait(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return inbox.poll();
	}

	private synchronized void newData(byte[] b) {
		inbox.offer(b);
		notifyAll();
	}

	private class Reciever extends Thread {

		@Override
		public void run() {
			tc.add(this);
			try {
				InputStream is = socket.getInputStream();
				while (!socket.isClosed()) {
					byte[] l = new byte[4];
					for (int i = 0; i < 4; i++)
						l[i] = readByte(is);
					int length = byteToInt(l);
					byte[] data = new byte[length];
					for (int i = 0; i < data.length; i++)
						data[i] = readByte(is);
					newData(data);
				}
			} catch (IOException e) {
				try {
					close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			tc.remove(this);
		}

		@Override
		public String toString() {
			return "Reciever";
		}

		private byte readByte(InputStream is) throws IOException {
			int input = is.read();
			if (input == -1)
				throw new IOException("EOF");
			return (byte) input;
		}
	}

	private class Sender extends Thread {

		@Override
		public void run() {
			tc.add(this);
			try {
				OutputStream os = socket.getOutputStream();
				while (!socket.isClosed()) {
					byte[] data = getData();
					if (data == null)
						continue;
					byte[] size = intToByte(data.length);

					os.write(size);
					os.write(data);
					os.flush();
				}
			} catch (IOException e) {
				// System.err.println("Error in connection! (Sending) (" +
				// e.getMessage() + ")");
				try {
					close();
				} catch (IOException e1) {
					// e1.printStackTrace();
				}
			}
			tc.remove(this);
		}

		@Override
		public String toString() {
			return "Sender";
		}
	}

	public void close() throws IOException {
		if (!socket.isClosed())
			socket.close();
	}

	public boolean isAlive() {
		return !socket.isClosed();
	}

	private byte[] intToByte(int i) {
		return ByteBuffer.allocate(4).putInt(i).array();
	}

	private int byteToInt(byte[] b) {
		ByteBuffer bb = ByteBuffer.wrap(b);
		return bb.getInt();
	}

}

package connection.packets.data;

import java.nio.ByteBuffer;

import connection.packets.Packet;
import connection.packets.factory.SuperFactory;
public class LongPacket extends DataPacket {

	private long l;

	public LongPacket(long l) {
		this.l = l;
	}

	public LongPacket(byte[] data, Packet[] packets) {
		ByteBuffer bb = ByteBuffer.wrap(data);
		l = bb.getLong();
	}

	@Override
	protected byte[] getLoad() {
		return ByteBuffer.allocate(8).putLong(l).array();
	}

	@Override
	protected byte getType() {
		return SuperFactory.LONG_PACKET;
	}
	
	@Override
	public String toString() {
		return "" + l;
	}

	public long toLong() {
		return l;
	}

}

package connection.packets.operands;

import connection.packets.Packet;

public abstract class OperatorPacket extends Packet {

	public abstract OperatorPacket perform();

	@Override
	protected byte[] getLoad() {
		return new byte[0];
	}
	

}

package connection.packets.operands;
import connection.packets.Packet;
import connection.packets.factory.SuperFactory;
public class ResponsePacket extends OperatorPacket {

	private Packet p;

	public ResponsePacket(Packet p) {
		this.p = p;
	}

	public ResponsePacket(byte[] data, Packet[] packets) {
		p = packets[0];
	}

	@Override
	public OperatorPacket perform() {
		return new NullPacket();
	}

	public Packet getPacket() {
		return p;
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { p };
	}

	@Override
	protected byte getType() {
		return SuperFactory.RESPONSE_PACKET;
	}
	
	@Override
	public String toString() {
		return "Response: " + p;
	}

}

package connection.packets.operands;

import org.json.JSONArray;
import org.json.JSONObject;

import connection.packets.Packet;
import connection.packets.data.StringPacket;
import connection.packets.factory.SuperFactory;

public class JSONPacket extends OperatorPacket {

	private StringPacket sp;

	public JSONPacket(JSONObject json) {
		sp = new StringPacket(json.toString(4));
	}

	public JSONPacket(byte[] data, Packet[] packets) {
		sp = (StringPacket) packets[0];
	}

	@Override
	public OperatorPacket perform() {
		return new NullPacket();
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { sp };
	}

	public JSONObject getJSON() {
		return new JSONObject(sp.toString());
	}

	public JSONObject getJSONObject(String key) {
		return getJSON().getJSONObject(key);
	}

	public JSONArray getJSONArray(String key) {
		return getJSON().getJSONArray(key);
	}

	public String getString(String key) {
		return getJSON().getString(key);
	}
	
	public int getInt(String key) {
		return getJSON().getInt(key);
	}

	@Override
	protected byte getType() {
		return SuperFactory.JSON_PACKET;
	}

	@Override
	public String toString() {
		return "JSON" + sp;
	}

}

package connection.packets.operands;

import connection.packets.Packet;
import connection.packets.factory.SuperFactory;

public class NullPacket extends OperatorPacket {

	public NullPacket() {

	}

	@Override
	public boolean isNull() {
		return true;
	}

	public NullPacket(byte[] data, Packet[] packets) {
	}

	@Override
	public OperatorPacket perform() {
		return new NullPacket();
	}

	@Override
	protected byte getType() {
		return SuperFactory.NULL_PACKET;
	}

	@Override
	public String toString() {
		return "NULL";
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[0];
	}

}

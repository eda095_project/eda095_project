package connection.packets;

import java.nio.ByteBuffer;

public abstract class Packet {

	protected abstract byte[] getLoad();

	protected abstract Packet[] getPackages();

	protected abstract byte getType();

	public boolean isNull() {
		return false;
	}

	public final byte[] getData() {
		int size = 9;
		byte[] load = getLoad();
		size += load.length;
		Packet[] packets = getPackages();
		for (Packet p : packets)
			size += p.getData().length;

		byte[] data = new byte[size];
		int i = 0;
		data[i++] = getType();
		for (byte b : intToByte(load.length)) {
			data[i++] = b;
		}
		for (byte b : load)
			data[i++] = b;
		for (byte b : intToByte(packets.length)) {
			data[i++] = b;
		}
		for (Packet p : packets) {
			byte[] temp = p.getData();
			for (byte b : temp)
				data[i++] = b;
		}

		return data;
	}

	private byte[] intToByte(int i) {
		return ByteBuffer.allocate(4).putInt(i).array();
	}
}
